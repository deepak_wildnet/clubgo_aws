<?php

include('config.php');

@$venue_id=$_REQUEST['venue'];

@$userid=$_REQUEST['userid'];

@$photo=$_REQUEST['photo'];

@$food=$_REQUEST['food'];

@$slide=$_REQUEST['slide'];

@$open=$_REQUEST['open'];

@$known=$_REQUEST['known'];

@$cuisine=$_REQUEST['cuisine'];

@$high=$_REQUEST['high'];

@$cost=$_REQUEST['cost'];

@$busy=$_REQUEST['busy'];

// main page array starts here ........

if($venue_id){

@$query=mysqli_query($conn," select i.item_id as id,i.item_title as title,(

SELECT COUNT( * ) 
FROM review
WHERE  item_id=i.item_id and status=1) as reviews,(

SELECT COUNT( * ) 
FROM user
WHERE  user_id='$userid' and user_wishlist_venue LIKE CONCAT(  '%', $venue_id,  '%' )
) AS userwish, i.item_rating as rating, i.item_lat as lat, i.item_lng as lng,i.item_address as address, l.loc_title from items i,location l where i.item_location = l.loc_id and i.item_id='$venue_id' ");

$fetch=mysqli_fetch_assoc($query);

$arr['detail'][]=$fetch;

$review=mysqli_query($conn," select u.user_name,u.user_photo,r.r_desc,r.time,r.rating from user u,review r where u.user_id=r.user_id and r.item_id='$venue_id' and status=1");

//...........


function get_time_difference_php($created_time)
 {
        date_default_timezone_set('Asia/kolkata'); //Change as per your default time
        $str = strtotime($created_time);
        $today = strtotime(date('Y-m-d H:i:s'));

        // It returns the time difference in Seconds...
        $time_differnce = $today-$str;

        // To Calculate the time difference in Years...
        $years = 60*60*24*365;

        // To Calculate the time difference in Months...
        $months = 60*60*24*30;

        // To Calculate the time difference in Days...
        $days = 60*60*24;

        // To Calculate the time difference in Hours...
        $hours = 60*60;

        // To Calculate the time difference in Minutes...
        $minutes = 60;

        if(intval($time_differnce/$years) > 1)
        {
            return intval($time_differnce/$years)." years ago";
        }else if(intval($time_differnce/$years) > 0)
        {
            return intval($time_differnce/$years)." year ago";
        }else if(intval($time_differnce/$months) > 1)
        {
            return intval($time_differnce/$months)." months ago";
        }else if(intval(($time_differnce/$months)) > 0)
        {
            return intval(($time_differnce/$months))." month ago";
        }else if(intval(($time_differnce/$days)) > 1)
        {
            return intval(($time_differnce/$days))." days ago";
        }else if (intval(($time_differnce/$days)) > 0) 
        {
            return intval(($time_differnce/$days))." day ago";
        }else if (intval(($time_differnce/$hours)) > 1) 
        {
            return intval(($time_differnce/$hours))." hours ago";
        }else if (intval(($time_differnce/$hours)) > 0) 
        {
            return intval(($time_differnce/$hours))." hour ago";
        }else if (intval(($time_differnce/$minutes)) > 1) 
        {
            return intval(($time_differnce/$minutes))." minutes ago";
        }else if (intval(($time_differnce/$minutes)) > 0) 
        {
            return intval(($time_differnce/$minutes))." minute ago";
        }else if (intval(($time_differnce)) > 1) 
        {
            return intval(($time_differnce))." seconds ago";
        }else
        {
            return "few seconds ago";
        }
  }

//..........

while($reviewfetch=mysqli_fetch_assoc($review)){

array_Push($reviewfetch,get_time_difference_php($reviewfetch['time']));

$arr['review'][]=$reviewfetch;

}

if(mysqli_num_rows($review)==0){

$arr['review']=array();

}

if(mysqli_num_rows($query)==0){

$arr['detail']=array();

}

$slidesel=mysqli_query($conn," select item_img1,item_img2,item_img3,item_img4 from items where item_id='$venue_id'");

$slidefetch=mysqli_fetch_assoc($slidesel);


if(mysqli_num_rows($slidesel)==0){

$arr['slide']=array();

}

else{ 

foreach($slidefetch as $key=>$value){

if(strlen($value)>0){
$arr['slide'][]=$value;
}

}
 

}


$count_rating = count($arr['review']);
if($count_rating != 0){
    foreach ($arr['review'] as $key => $value) {
        $total_rating += $value['rating'];
    }
    $calculate_rating = ceil($total_rating/$count_rating);
    $arr['detail'][0]['rating'] = $calculate_rating;    
}else{
    $arr['detail'][0]['rating'] = 0;    
}



echo json_encode($arr,JSON_UNESCAPED_SLASHES);

}

// main page array ends here ........


// slider code starts here.....


if($slide){



}


// slider code ends here.....



// food code starts here......


if($food){

$foodsel=mysqli_query($conn," select menu_image,bar_image from items where item_id='$food' ");

$foodfetch=mysqli_fetch_array($foodsel);

if($foodfetch[0]){

if(strlen($foodfetch[0])==0){

$arr['food'][]="No Records found";

}


else{

$imgarr=explode(',',$foodfetch[0]);

for($i=0;$i<count($imgarr);$i++){

if(strlen($imgarr[$i])>0){

$arr['food'][]=$imgarr[$i];
}
}

}
}

//.............................

if($foodfetch[1]){

if(strlen($foodfetch[1])==0){

$arr['bar'][]="No Records found";

}



else{

$bararr=explode(',',$foodfetch[1]);

for($j=0;$j<count($bararr);$j++){

if(strlen($bararr[$j])>0){

$arr['bar'][]=$bararr[$j];
}
}

}
}
if(strlen($foodfetch[0])==0 and strlen($foodfetch[1])==0){

$arr['food']=array();
$arr['bar']=array();

}
echo json_encode($arr,JSON_UNESCAPED_SLASHES);

}


// food code ends here......



// photo code starts here......


if($photo){

$photosel=mysqli_query($conn," select venue_image from items where item_id='$photo'");

$photofetch=mysqli_fetch_array($photosel);

if(strlen($photofetch[0])==0){

$arr['photo'][]=null;

}

else{

$photoarr=explode(',',$photofetch[0]);

for($i=0;$i<count($photoarr);$i++){

if(strlen($photoarr[$i])>0){

$arr['photo'][]=$photoarr[$i];
}
}

}

echo json_encode($arr,JSON_UNESCAPED_SLASHES);
}


// photo code ends here...... 


// open code starts here.....


if($open){

$opensel=mysqli_query($conn," select happy_hours from items where item_id='$open'");

$openfetch=mysqli_fetch_array($opensel);

if(strlen($openfetch[0])==0){

$arr['open'][]="No Records found";

}

else{

$a=preg_replace("/-/"," to ",$openfetch[0]);

$ex=explode('@',$a);


for($i=0;$i<count($ex);$i++){

$day=explode("*",$ex[$i]);




$time=explode("#",$day[1]);

if(strlen($time[1])==1){
  $arr['open'][$day[0]]= $time[0];
}
else{
 $arr['open'][$day[0]]=$time[1];
}
    
    }

echo json_encode($arr,JSON_UNESCAPED_SLASHES);

}


}


// open code ends here.....


//known code starts here....


if($known){

$knownsel=mysqli_query($conn," select known_for from items where item_id='$known'");

$knownfetch=mysqli_fetch_array($knownsel);

if(strlen($knownfetch[0])==0){

$arr['known'][]="No Records found";

}

else{

$ex=explode(',',$knownfetch[0]);

for($i=0;$i<count($ex);$i++){

$arr['known'][]=$ex[$i];

}



}
echo json_encode($arr);

}


//known code ends here....


// cuisine code starts here....

if($cuisine){

$cuisinesel=mysqli_query($conn," select cuisines from items where item_id='$cuisine'");

$cuisinefetch=mysqli_fetch_array($cuisinesel);

if(strlen($cuisinefetch[0])==0){

$arr['cuisine'][]="No Records found";

}

else{

$ex=explode(',',$cuisinefetch[0]);

for($i=0;$i<count($ex);$i++){

$arr['cuisine'][]=$ex[$i];

}



}

echo json_encode($arr);



}


// cuisine code ends here...


// highlight code starts here........


if($high){

$highsel=mysqli_query($conn," select highlights from items where item_id='$high'");

$highfetch=mysqli_fetch_array($highsel);

if(strlen($highfetch[0])==0){

$arr['high'][]="No Records found";

}

else{

$ex=explode(',',$highfetch[0]);

for($i=0;$i<count($ex);$i++){

$arr['high'][]=$ex[$i];

}


}

echo json_encode($arr);

}

// highlight code ends here......


// cost code starts here .........


if($cost){

$costsel=mysqli_query($conn," select cost from items where item_id='$cost'");

$costfetch=mysqli_fetch_array($costsel);

if(strlen($costfetch[0])==0){

$arr['cost'][]="No Records found";

}

else{

$ex=explode(',',$costfetch[0]);

for($i=0;$i<count($ex);$i++){

$arr['cost'][]=$ex[$i];

}



}

echo json_encode($arr);

}


// cost code ends here .........


// busy code starts here....


if($busy){

$busysel=mysqli_query($conn," select busy_nights from items where item_id='$busy'");

$busyfetch=mysqli_fetch_array($busysel);

if(strlen($busyfetch[0])==0){

$arr['busy'][]=null;

}

else{


$ex=explode(',',$busyfetch[0]);

for($i=0;$i<count($ex);$i++){

$arr['busy'][]=$ex[$i];

}



}

echo json_encode($arr);

}

//  busy code ends here ......
?>