<?php

date_default_timezone_set('Asia/kolkata');
@$date=date('Y-m-d',time());
$ta=strtotime(date('H:i'));

include('config.php');

$booktimegroup=$_REQUEST['group'];
$userid=$_REQUEST['userid'];//user login id


// live starts here .......


if($_REQUEST['group']=='live'){

$livequery=mysqli_query($conn,"SELECT i.item_id, e.e_id AS event,TIME_FORMAT(e.e_start,' %h:%i %p') AS tiime, e.e_name AS event_name,b.book_id as booking,TIME_FORMAT( (
IF( b.book_time =1, tt.af_time, tt.bf_time )
),' %h:%i %p') AS ticket_tiime, tt.type_title AS tic_name, i.item_title AS title, l.loc_title AS location,  'normal' AS 
type , e.feature_image as feat_image, DATE_FORMAT( b.book_date,  '%W , %d %M' ) AS DATE, b.user_booking_id AS dynamic_id,b.last_entry AS lastentry, b.male_ticket, b.female_ticket, b.couple_ticket, b.booked_price
FROM items i, location l, booking b, event e, ticket t, ticket_type tt
WHERE b.event_id = e.e_id
AND b.ticket_id = t.ticket_id
AND t.ticket_type = tt.type_id
AND e.item_id = i.item_id
AND i.item_location = l.loc_id
AND   ( b.book_date >= '".$date."' or (e.e_enddate = '".$date."' and e.e_end <='".$ta."') ) and b.userid='".$userid."' order by b.book_posted desc limit 5");

while($livefetch=mysqli_fetch_assoc($livequery)){

   $arr['live'][]=$livefetch;

   }

   //if(mysqli_num_rows($livequery)==0){  $arr['live']['normal'][]=null; }

   $livevip=mysqli_query($conn,"SELECT i.item_id,i.item_title AS title,e.e_id as event,e.e_name AS event_name, l.loc_title AS location,'vip' as type, i.feat_image, DATE_FORMAT( vb.for_date,  '%W %d %M' ) AS DATE, v.vip_name, v.vip_price, v.vip_desc,vb.dynamic_vbookid as dynamic
FROM items i, location l, vip_book vb, vip v, event e
WHERE vb.vip_id = v.vip_id
AND v.e_id = e.e_id
AND e.item_id = i.item_id
AND i.item_location = l.loc_id
AND  vb.for_date >= '".$date."' and vb.user_id='".$userid."' order by vb.vbook_time desc");

while($vipfetch=mysqli_fetch_assoc($livevip)){

  $arr['live'][]=$vipfetch;

}

if(mysqli_num_rows($livevip)==0 && mysqli_num_rows($livequery)==0){  $arr['live']=array(); }

echo json_encode($arr,JSON_UNESCAPED_SLASHES);

}

// live ends here........


// past starts here..........

if($_REQUEST['group']=='past'){

$pastquery=mysqli_query($conn,"SELECT i.item_id, e.e_id AS event,TIME_FORMAT(e.e_start,' %h:%i %p') AS tiime, e.e_name AS event_name,b.book_id as booking,TIME_FORMAT( (
IF( b.book_time =1, tt.af_time, tt.bf_time )
),' %h:%i %p') AS ticket_time, tt.type_title AS tic_name, i.item_title AS title, l.loc_title AS location,  'normal' AS 
type , e.feature_image as feat_image, DATE_FORMAT( b.book_date,  '%W , %d %M' ) AS DATE, b.user_booking_id AS dynamic_id,b.last_entry AS lastentry, b.male_ticket, b.female_ticket, b.couple_ticket, b.booked_price
FROM items i, location l, booking b, event e, ticket t, ticket_type tt
WHERE b.event_id = e.e_id
AND b.ticket_id = t.ticket_id
AND t.ticket_type = tt.type_id
AND e.item_id = i.item_id
AND i.item_location = l.loc_id
AND   (b.book_date < '".$date."' or (e.e_enddate = '".$date."' and e.e_end >='".$ta."')) and b.userid='".$userid."' order by b.book_posted desc limit 5");

while($pastfetch=mysqli_fetch_assoc($pastquery)){

 $arr['past'][]=$pastfetch;

}

//if(mysqli_num_rows($pastquery)==0){  $arr['past']['normal']=array(); }


$pastvip=mysqli_query($conn,"SELECT i.item_id,i.item_title AS title,e.e_id as event,e.e_name AS event_name, l.loc_title AS location,'vip' as type, i.feat_image, DATE_FORMAT( vb.for_date,  '%W %d %M' ) AS DATE, v.vip_name, v.vip_price, v.vip_desc,vb.dynamic_vbookid as dynamic
FROM items i, location l, vip_book vb, vip v, event e
WHERE vb.vip_id = v.vip_id
AND v.e_id = e.e_id
AND e.item_id = i.item_id
AND i.item_location = l.loc_id
AND  vb.for_date < '".$date."' and vb.user_id='".$userid."' order by vb.vbook_time desc");

while($pastvipf=mysqli_fetch_assoc($pastvip)){

 $arr['past'][]=$pastvipf;

}

if(mysqli_num_rows($pastvip)==0 && mysqli_num_rows($pastquery)==0){  $arr['past']=array(); }

echo json_encode($arr,JSON_UNESCAPED_SLASHES);

}

// past ends here.............

?>