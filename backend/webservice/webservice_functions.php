<?php

/**
* To Obtain crud operations
*/
class Webservices
{
	
	private $conn;
	public function __construct($conn) {
        global $conn;
        $this->conn = $conn;
    }

	/**
	 * Get Single Row
	 * @First Argument for table name
	 * @Second Argument for id (table id)
	 */
	function get_row($tablename,$id){

		if($id){
			$query = mysqli_query($this->conn,"SELECT * FROM $tablename where id =".$id);
			$row = mysqli_fetch_object($query);
			return (object) $row;
		}

	}
	/**
	 * Get Single Row
	 * @First Argument for table name
	 * @Second Argument for key
	 * @Third argument for value
	 */
	function get_row_using_key_and_value($tablename,$key,$value){

		if($key && $value){
			$query = mysqli_query($this->conn,"SELECT * FROM $tablename where $key =".$value);
			$row = mysqli_fetch_object($query);
			return (object) $row;
		}

	}

	/**
	 * Get Results (Multiple rows)
	 * @First Argument for table name
	 * @Second Argument array
	 */
	function get_results($tablename,$where){

		$query = mysqli_query($this->conn,"SELECT * FROM $tablename where ".$where);
			
		$bookings = array();

		while($row = mysqli_fetch_object($query)){
			array_push($bookings,$row);
		}

		return (object) $bookings;
	}
	
	/**
     * Insert Row
	*/

	function insert_row($tablename,$data=array()){
		
		$sql = $this->insertToSqlQuery($data);

		$insert = mysqli_query($this->conn,"INSERT into $tablename (".$sql['keys'].") values(".$sql['values'].")");
		
		if(!$insert){
			die("Error :".mysqli_errno());
		}
	}
	/**
	 * Update Row
	*/
	function update_row($tablename,$data=array(),$where){
		if($where && $data){

			$sql = $this->updateToSqlQuery($data);
			$update = mysqli_query($this->conn,"UPDATE $tablename set $sql where".$where);
			
			if($this->conn->affected_rows > 0){
				return $this->conn->affected_rows;
			}else{
				return 0;
			}
		}
		
	}
	/**
	 * Delete Uncomplete Booking
	*/
	function delete_uncomplete_booking($tablename){

		if($tablename){
			$delete = mysqli_query($this->conn,"DELETE FROM $tablename WHERE status = 0 and created_at < (NOW() - INTERVAL 5 MINUTE) ");

			if($this->conn->affected_rows > 0){
				return $this->conn->affected_rows;
			}else{
				return 0;
			}
		}
	}
	/**
	  * This function is used for convert array into keys and values
	  * @return Array
	  */
	public function insertToSqlQuery($data){

		$keys = "";
		$values = "";
		foreach ($data as $key => $value) {
			$keys .= $key.',';
			$values .= "'$value'".",";
		}
		$keys = rtrim($keys,',');
		$values = rtrim($values,',');

		$sql = array('keys'=>$keys,'values'=>$values);
		return $sql;

	}

	/**
	 * This function is used for convert array into like update query
	 */
	public function updateToSqlQuery($data){
		
		$fields = "";
		foreach ($data as $key => $value) {
			$fields .= "`$key`='$value'".',';
		}
		$fields = rtrim($fields,',');
		return $fields;
		
	}
}