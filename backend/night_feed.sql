-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 29, 2015 at 05:07 PM
-- Server version: 5.6.27-0ubuntu1
-- PHP Version: 5.6.11-1ubuntu3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `like_a_fish`
--

-- --------------------------------------------------------

--
-- Table structure for table `night_feed`
--

CREATE TABLE IF NOT EXISTS `night_feed` (
  `feed_id` int(11) NOT NULL,
  `item_id` int(30) DEFAULT NULL,
  `event_date` varchar(100) DEFAULT NULL,
  `event_desc` varchar(200) DEFAULT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `night_feed`
--

INSERT INTO `night_feed` (`feed_id`, `item_id`, `event_date`, `event_desc`, `posted`) VALUES
(1, 8, '27th dec', 'Badshah live', '2015-10-29 09:23:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `night_feed`
--
ALTER TABLE `night_feed`
  ADD PRIMARY KEY (`feed_id`),
  ADD KEY `item_id` (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `night_feed`
--
ALTER TABLE `night_feed`
  MODIFY `feed_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `night_feed`
--
ALTER TABLE `night_feed`
  ADD CONSTRAINT `night_feed_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
