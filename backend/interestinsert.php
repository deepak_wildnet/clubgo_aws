<?php
error_reporting(0);
ob_start();
session_start();
$a = $_SESSION['name'];
//echo $a;
if (!$a) {

    header("location:login.php");
}
$expireAfter = 30;
if (isset($_SESSION['last_action'])) {

    $secondsInactive = time() - $_SESSION['last_action'];
    $expireAfterSeconds = $expireAfter * 60;


    if ($secondsInactive >= $expireAfterSeconds) {

        session_unset();
        session_destroy();
    }
}

$_SESSION['last_action'] = time();
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>ClubGo - Responsive Admin Template</title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="vendor/themify-icons/themify-icons.min.css">
        <link href="vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/plugins.css">        
        <link rel="stylesheet" href="assets/datepicker/css/datepicker.css">     


        <link rel="stylesheet" href="assets/css/themes/theme-1.css" id="skin_color" />
        <!-- end: CLIP-TWO CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

        
        <script type="text/javascript">

            function formvalidation() {

                var c, d;

                var a = document.getElementById("t1");
                a = a.removeAttribute("required");
            }

        </script>
    </head>
    <!-- end: HEAD -->
    <style>
        .col-lg-6 {
            width:100%!important;
        }
    </style>

    <?php
    include('config.php');

    @$ftitle = addslashes($_POST['title']);
    @$fdate = addslashes($_POST['datepicker']);

    @$act = $_GET['action'];
    $id = $_GET['id'];
    
    if (@$_POST['can']) {
        header('location: interest.php');
    }


    if (@$_POST['send']) {

        if (isset($act) and $act == 'add') {
                @$insert = mysqli_query($conn, "INSERT into interest(title,interest_date) values('$ftitle','$fdate')");
                header('location:interest.php');
                // move_uploaded_file($filetmp,$ufilename);
        }

        if (isset($act) and $act == 'edit') {
            $update = mysqli_query($conn, "UPDATE interest set title='$ftitle',interest_date='$fdate' where id='$id'");
            echo 'Updated';            
        }
    }
    ?>
    <body>


        <div id="app">
            <!-- sidebar -->
            <div class="sidebar app-aside" id="sidebar">
                <div class="sidebar-container perfect-scrollbar">
                    <nav>                        
                        <!-- start: MAIN NAVIGATION MENU -->
                        <div class="navbar-title">
                            <span>Main Navigation</span>
                        </div>
                        <ul class="main-navigation-menu">

<?php include("nav_index.php"); ?>

                            <!-- end: DOCUMENTATION BUTTON -->
                            <div class="wrapper">
                                <a href="documentation.html" class="button-o">
                                    <i class="ti-help"></i>
                                    <span>Documentation</span>
                                </a>
                            </div>
                    </nav>
                </div>
            </div>
            <!-- / sidebar -->
            <div class="app-content">
                <!-- start: TOP NAVBAR -->
                <header class="navbar navbar-default navbar-static-top">
                    <!-- start: NAVBAR HEADER -->
                    <div class="navbar-header">
                        <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="navbar-brand" href="#">
                        <!--	<img src="assets/images/logo.png" alt="Clip-Two"/> --><h1>ClubGo</h1>
                        </a>
                        <a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ti-view-grid"></i>
                        </a>
                    </div>
                    <!-- end: NAVBAR HEADER -->
                    <!-- start: NAVBAR COLLAPSE -->
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-right">                            
                            <!-- start: LANGUAGE SWITCHER -->
                            <!-- start: USER OPTIONS DROPDOWN -->
                            <li class="dropdown current-user">
                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="assets/images/unnamed.jpg" alt="Peter"> <span class="username"><?php echo $a ?> <i class="ti-angle-down"></i></i></span>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">                                   
                                    <li>
                                        <a href="logout.php">
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER OPTIONS DROPDOWN -->
                        </ul>
                        <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
                        <div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <div class="arrow-left"></div>
                            <div class="arrow-right"></div>
                        </div>
                        <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
                    </div>
                    <a class="dropdown-off-sidebar" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
                        &nbsp;
                    </a>
                    <!-- end: NAVBAR COLLAPSE -->
                </header>
                <!-- end: TOP NAVBAR -->
                <div class="main-content" >
                    <div class="wrap-content container" id="container">
                        <!-- start: DASHBOARD TITLE -->
                        <section id="page-title" class="padding-top-15 padding-bottom-15">
                            <div class="row">
                                <div class="col-sm-7">
                                    <h1 class="mainTitle">Interest</h1>
                                </div>
                                <div class="col-sm-5">
                                    <!-- start: MINI STATS WITH SPARKLINE -->
                                    <ul class="mini-stats pull-right">									
                                    </ul>
                                    <!-- end: MINI STATS WITH SPARKLINE -->
                                </div>
                            </div>
                        </section>
                        <!-- end: DASHBOARD TITLE -->
                        <!-- start: FEATURED BOX LINKS -->
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">

                                <div class="panel-body">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="panel panel-white">
                                            <div class="panel-body">                                                   
                                                <form role="form" enctype="multipart/form-data" action="<?php $_PHP_SELF ?>" class="form-horizontal" method="post">
                                                    <!--edit code-->

                                                    <?php
                                                    if ($act and $act == 'edit') {
                                                        @$query3 = mysqli_query($conn, "select * FROM interest where id='$id' ");
                                                        $fetch3 = mysqli_fetch_array($query3);
                                                        ?>
                                                    
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="title">
                                                                Title
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  id="t1" name="title" class="form-control"  value="<?php echo $fetch3['title'] ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group margin-bottom-0">
                                                            <div class="col-sm-offset-2 col-sm-10">

                                                                <input type="submit" name="send" class="btn btn-o btn-primary"  value="Save">

                                                                <input type="submit" name="can" class="btn btn-o btn-primary"  onclick="formvalidation()" value="Cancel">
                                                             <!-- <span style="color:green"><?php
                                                                if (@$message) {
                                                                    echo $message;
                                                                }
                                                                ?></span> -->
                                                            </div>
                                                                <?php
                                                                if (isset($message) and strlen($message) > 0) {
                                                                    echo $message;
                                                                }
                                                                ?>
                                                            <div>
                                                            </div>
                                                        </div>



                                                            <?php
                                                        } else {
                                                            ?> 

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="title">
                                                                Title
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  id="t1" name="title" class="form-control" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group margin-bottom-0">
                                                            <div class="col-sm-offset-2 col-sm-10">

                                                                <input type="submit" name="send" class="btn btn-o btn-primary"  value="Save">

                                                                <input type="submit" name="can" class="btn btn-o btn-primary"  onclick="formvalidation()" value="Cancel">

                                                            </div>
                                                            <?php
                                                            if (isset($message) and strlen($message) > 0) {
                                                                echo $message;
                                                            }
                                                            ?>
                                                                <div>

                                                            </div>
                                                        </div>

                                                    <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- end: FOURTH SECTION -->
    </div>
</div>
</div>
<!-- start: FOOTER -->
<footer>
    <div class="footer-inner">
        <div class="pull-left">
            &copy; <span class="current-year"></span><span class="text-bold text-uppercase">ClubGo</span>. <span>All rights reserved</span>
        </div>
        <div class="pull-right">
            <span class="go-top"><i class="ti-angle-up"></i></span>
        </div>
    </div>
</footer>
<!-- end: FOOTER -->
</div>
<!-- start: MAIN JAVASCRIPTS -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/modernizr/modernizr.js"></script>
<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="vendor/switchery/switchery.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="vendor/Chart.js/Chart.min.js"></script>
<script src="vendor/jquery.sparkline/jquery.sparkline.min.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CLIP-TWO JAVASCRIPTS -->
<script src="assets/js/main.js"></script>

<script src="assets/datepicker/js/bootstrap-datepicker.js"></script>


<!-- start: JavaScript Event Handlers for this page -->
<script src="assets/js/index.js"></script>
<script>
            jQuery(document).ready(function() {
                Main.init();
                Index.init();
            });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
<?php ob_end_flush();
?>