<?php
error_reporting(0);
ob_start();
session_start();
$a = $_SESSION['name'];
//echo $a;
if (!$a) {

    header("location:login.php");
}
$expireAfter = 30;
if (isset($_SESSION['last_action'])) {

    $secondsInactive = time() - $_SESSION['last_action'];
    $expireAfterSeconds = $expireAfter * 60;


    if ($secondsInactive >= $expireAfterSeconds) {

        session_unset();
        session_destroy();
    }
}

$_SESSION['last_action'] = time();
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>ClubGo - Responsive Admin Template</title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="vendor/themify-icons/themify-icons.min.css">
        <link href="vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/plugins.css">        
        <link rel="stylesheet" href="assets/datepicker/css/datepicker.css">     


        <link rel="stylesheet" href="assets/css/themes/theme-1.css" id="skin_color" />
        <!-- end: CLIP-TWO CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

        
        <script type="text/javascript">

            function formvalidation() {

                var c, d;

                var e = document.getElementById("sc");
                e = e.removeAttribute("required");



                var d = document.getElementById("sb");
                d = d.removeAttribute("required");

                var a = document.getElementById("t1");
                a = a.removeAttribute("required");

                var b = document.getElementById("t2");
                b = b.removeAttribute("required");

                var c = document.getElementById("si");
                c = c.removeAttribute("required");


                self.location.href = "http://localhost/funify/coupon.php";
                //self.location.href="http://test.webgarage.in/funify/nightview.php"
            }

        </script>
    </head>
    <!-- end: HEAD -->
    <style>
        .col-lg-6 {
            width:100%!important;
        }
    </style>

    <?php
    include('config.php');

    @$ftitle = addslashes($_POST['title']);
    @$fdesc = addslashes($_POST['desc']);
    @$fdate = addslashes($_POST['datepicker']);

    @$act = $_GET['action'];
    $id = $_GET['id'];
    $neimage1 = $_POST['image11'];

    /* ----------- ImageUpload ------- */
    $name = $_FILES['image']['name'];
    $tmp = $_FILES['image']['tmp_name'];
    $fs = $_FILES['image']['size'];
    $ft = $_FILES['image']['type'];
    $fe = $_FILES['image']['error'];
    $target = "images/news/" . $name;
    /* ---------------------------- */



    if (@$_POST['can']) {

        header('location: news.php');
    }


    if (@$_POST['send']) {

        if (isset($act) and $act == 'add') {

            if ($name) {

                if ($fs > 2097152) {

                    $message = "<font color='red'>File size must be less than 2 MB.</font>";
                } elseif ($ft != "image/jpg" && $ft != "image/png" && $ft != "image/jpeg") {
                    $message = "<font color='red'>extension not allowed, please choose a JPG, JPEG & PNG  file.</font>";
                } else {

                    @$insert = mysqli_query($conn, "INSERT into news(title,news_desc,news_date,image) values('$ftitle','$fdesc','$fdate','$target')");                    

                    if (!$insert) {

                        die("Server Error, Please Try Again Later" . mysqli_error());
                    }

                    $sql1 = "select max(id) from news";
                    $data1 = mysqli_query($conn, $sql1);
                    $res1 = mysqli_fetch_array($data1);

                    $cid = $res1[0];

                    $sql2 = "select image from news where id='$cid'";
                    $data2 = mysqli_query($conn, $sql2);
                    $res2 = mysqli_fetch_array($data2);

                    $iname = $res2['0'];

                    $iname1 = explode('/', $iname);
                    $iname2 = explode('.', $iname1[2]);


                    $iname3 = "images/news/" . mt_rand() . "." . $iname2[1];

                    $sql3 = "update news set image='$iname3' where id='$cid'";
                    $dis = mysqli_query($conn, $sql3);

                    if ($dis) {

                        $message = "Data saved";
                        move_uploaded_file($tmp, $iname3);
                        header('location:news.php');
                    } else {
                        $message = "Data not saved";
                    }
                }
            } else {
                @$insert = mysqli_query($conn, "INSERT into news(title,news_desc,news_date) values('$ftitle','$fdesc','$fdate')");
                header('location:news.php');
                // move_uploaded_file($filetmp,$ufilename);
            }
        }



        if (isset($act) and $act == 'edit') {
            if ($name) {
                unlink($neimage1);

                if ($fs > 2097152) {
                    $message = "<font color='red'>File size must be less than 2 MB.</font>";
                } elseif ($ft != "image/jpg" && $ft != "image/png" && $ft != "image/jpeg") {
                    $message = "<font color='red'>extension not allowed, please choose a JPG, JPEG & PNG  file.</font>";
                } else {
                    $update = mysqli_query($conn, "UPDATE news set title='$ftitle',news_desc='$fdesc',news_date='$fdate',image='$target' where id='$id'");

                    if ($update) {
                        $sql2 = "select image from news where id='$id'";
                        $data2 = mysqli_query($conn, $sql2);
                        $res2 = mysqli_fetch_array($data2);

                        $iname = $res2['0'];
                        $iname1 = explode('/', $iname);
                        $iname2 = explode('.', $iname1[2]);


                        $iname3 = "images/news/" . mt_rand() . "." . $iname2[1];

                        $sql3 = "update news set image='$iname3' where id='$id'";
                        $change = mysqli_query($conn, $sql3);

                        if ($change) {
                            $message = "Data updated";
                        }
                        move_uploaded_file($tmp, $iname3);
                        //header('location:category.php');
                        echo 'Updated';
                    }
                }
            } else {
                $update = mysqli_query($conn, "UPDATE news set title='$ftitle',news_desc='$fdesc',news_date='$fdate' where id='$id'");

                echo 'Updated';
            }
        }
    }
    ?>
    <body>


        <div id="app">
            <!-- sidebar -->
            <div class="sidebar app-aside" id="sidebar">
                <div class="sidebar-container perfect-scrollbar">
                    <nav>                        
                        <!-- start: MAIN NAVIGATION MENU -->
                        <div class="navbar-title">
                            <span>Main Navigation</span>
                        </div>
                        <ul class="main-navigation-menu">

<?php include("nav_index.php"); ?>

                            <!-- end: DOCUMENTATION BUTTON -->
                            <div class="wrapper">
                                <a href="documentation.html" class="button-o">
                                    <i class="ti-help"></i>
                                    <span>Documentation</span>
                                </a>
                            </div>
                    </nav>
                </div>
            </div>
            <!-- / sidebar -->
            <div class="app-content">
                <!-- start: TOP NAVBAR -->
                <header class="navbar navbar-default navbar-static-top">
                    <!-- start: NAVBAR HEADER -->
                    <div class="navbar-header">
                        <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="navbar-brand" href="#">
                        <!--	<img src="assets/images/logo.png" alt="Clip-Two"/> --><h1>ClubGo</h1>
                        </a>
                        <a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ti-view-grid"></i>
                        </a>
                    </div>
                    <!-- end: NAVBAR HEADER -->
                    <!-- start: NAVBAR COLLAPSE -->
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-right">                            
                            <!-- start: LANGUAGE SWITCHER -->
                            <!-- start: USER OPTIONS DROPDOWN -->
                            <li class="dropdown current-user">
                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="assets/images/unnamed.jpg" alt="Peter"> <span class="username"><?php echo $a ?> <i class="ti-angle-down"></i></i></span>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">                                   
                                    <li>
                                        <a href="logout.php">
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER OPTIONS DROPDOWN -->
                        </ul>
                        <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
                        <div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <div class="arrow-left"></div>
                            <div class="arrow-right"></div>
                        </div>
                        <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
                    </div>
                    <a class="dropdown-off-sidebar" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
                        &nbsp;
                    </a>
                    <!-- end: NAVBAR COLLAPSE -->
                </header>
                <!-- end: TOP NAVBAR -->
                <div class="main-content" >
                    <div class="wrap-content container" id="container">
                        <!-- start: DASHBOARD TITLE -->
                        <section id="page-title" class="padding-top-15 padding-bottom-15">
                            <div class="row">
                                <div class="col-sm-7">
                                    <h1 class="mainTitle">News</h1>
                                    <span class="mainDescription">overview &amp; stats </span>
                                </div>
                                <div class="col-sm-5">
                                    <!-- start: MINI STATS WITH SPARKLINE -->
                                    <ul class="mini-stats pull-right">									
                                    </ul>
                                    <!-- end: MINI STATS WITH SPARKLINE -->
                                </div>
                            </div>
                        </section>
                        <!-- end: DASHBOARD TITLE -->
                        <!-- start: FEATURED BOX LINKS -->
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">

                                <div class="panel-body">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="panel panel-white">
                                            <div class="panel-heading">
                                                <h5 class="panel-title">News Form</h5>
                                            </div>
                                            <div class="panel-body">                                                   
                                                <form role="form" enctype="multipart/form-data" action="<?php $_PHP_SELF ?>" class="form-horizontal" method="post">
                                                    <!--edit code-->

                                                    <?php
                                                    if ($act and $act == 'edit') {
                                                        @$query3 = mysqli_query($conn, "select * FROM news where id='$id' ");
                                                        $fetch3 = mysqli_fetch_array($query3);
                                                        ?>
                                                    
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="title">
                                                                Title
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  id="t1" name="title" class="form-control"  value="<?php echo $fetch3['title'] ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="description">
                                                                Description
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea id="t2" cols="25" rows="10" name="desc" class="form-control" ><?php echo $fetch3['news_desc'] ?></textarea>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="datepicker">
                                                                Date
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  id="datepicker" name="datepicker" class="form-control" value="<?php echo $fetch3['news_date'] ?>" >
                                                            </div>

                                                            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                                            <script>
                                                            $(function() {
                                                                $("#datepicker").datepicker();
                                                            });
                                                            </script>
                                                        </div>
                                                    

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="inputEmail3">
                                                                Image
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <?php if($fetch3['image'] !=''){ ?>                                                                
                                                                <image id="t4"  width="40px" height="40px" src="<?php echo $fetch3['image'] ?>">
                                                                <?php } ?>
                                                                <input type="file" name="image"  id="t4" class="form-control" >
                                                                <input type="hidden" name="image11" value="<?php echo $fetch3['image'] ?>">
                                                                <strong style="color:red"><?php
                                                    if (isset($errors) and strlen($errors) > 0) {
                                                        echo $errors;
                                                    }
                                                    ?></strong>
                                                            </div>
                                                        </div>

                                                        <div class="form-group margin-bottom-0">
                                                            <div class="col-sm-offset-2 col-sm-10">

                                                                <input type="submit" name="send" class="btn btn-o btn-primary"  value="Save">

                                                                <input type="submit" name="can" class="btn btn-o btn-primary"  onclick="formvalidation()" value="Cancel">
                                                             <!-- <span style="color:green"><?php
                                                                if (@$message) {
                                                                    echo $message;
                                                                }
                                                                ?></span> -->
                                                            </div>
                                                                <?php
                                                                if (isset($message) and strlen($message) > 0) {
                                                                    echo $message;
                                                                }
                                                                ?>
                                                            <div>
                                                            </div>
                                                        </div>



                                                            <?php
                                                        } else {
                                                            ?> 

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="title">
                                                                Title
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  id="t1" name="title" class="form-control" >
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="description">
                                                                Description
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea id="t2" cols="25" rows="10" name="desc" class="form-control" ></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label" for="datepicker">
                                                                Date
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  id="datepicker" name="datepicker" class="form-control" >
                                                            </div>

                                                            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                                            <script>
                                                            $(function() {
                                                                $("#datepicker").datepicker();
                                                            });
                                                            </script>
                                                        </div>

                                                        <div class="form-group">

                                                            <label class="col-sm-2 control-label" for="inputEmail3">
                                                                Image
                                                            </label>
                                                            <div class="col-sm-10">                                                        
                                                                <input type="file" name="image"  id="t4" class="form-control" >

                                                                <strong style="color:red"><?php
                                                    if (isset($errors) and strlen($errors) > 0) {
                                                        echo $errors;
                                                    }
                                                    ?></strong>
                                                            </div>

                                                        </div>


                                                        <div class="form-group margin-bottom-0">
                                                            <div class="col-sm-offset-2 col-sm-10">

                                                                <input type="submit" name="send" class="btn btn-o btn-primary"  value="Save">

                                                                <input type="submit" name="can" class="btn btn-o btn-primary"  onclick="formvalidation()" value="Cancel">

                                                            </div>
                                                            <?php
                                                            if (isset($message) and strlen($message) > 0) {
                                                                echo $message;
                                                            }
                                                            ?>
                                                                <div>

                                                            </div>
                                                        </div>

                                                    <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- end: FOURTH SECTION -->
    </div>
</div>
</div>
<!-- start: FOOTER -->
<footer>
    <div class="footer-inner">
        <div class="pull-left">
            &copy; <span class="current-year"></span><span class="text-bold text-uppercase">ClubGo</span>. <span>All rights reserved</span>
        </div>
        <div class="pull-right">
            <span class="go-top"><i class="ti-angle-up"></i></span>
        </div>
    </div>
</footer>
<!-- end: FOOTER -->
<!-- start: OFF-SIDEBAR -->
<div id="off-sidebar" class="sidebar">
    <div class="sidebar-wrapper">
        <ul class="nav nav-tabs nav-justified">
            <li class="active">
                <a href="#off-users" aria-controls="off-users" role="tab" data-toggle="tab">
                    <i class="ti-comments"></i>
                </a>
            </li>
            <li>
                <a href="#off-favorites" aria-controls="off-favorites" role="tab" data-toggle="tab">
                    <i class="ti-heart"></i>
                </a>
            </li>
            <li>
                <a href="#off-settings" aria-controls="off-settings" role="tab" data-toggle="tab">
                    <i class="ti-settings"></i>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="off-users">
                <div id="users" toggleable active-class="chat-open">
                    <div class="users-list">
                        <div class="sidebar-content perfect-scrollbar">
                            <h5 class="sidebar-title">On-line</h5>
                            <ul class="media-list">
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <i class="fa fa-circle status-online"></i>
                                        <img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Nicole Bell</h4>
                                            <span> Content Designer </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <div class="user-label">
                                            <span class="label label-success">3</span>
                                        </div>
                                        <i class="fa fa-circle status-online"></i>
                                        <img alt="..." src="assets/images/avatar-3.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Steven Thompson</h4>
                                            <span> Visual Designer </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <i class="fa fa-circle status-online"></i>
                                        <img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Ella Patterson</h4>
                                            <span> Web Editor </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <i class="fa fa-circle status-online"></i>
                                        <img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Kenneth Ross</h4>
                                            <span> Senior Designer </span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <h5 class="sidebar-title">Off-line</h5>
                            <ul class="media-list">
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Nicole Bell</h4>
                                            <span> Content Designer </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <div class="user-label">
                                            <span class="label label-success">3</span>
                                        </div>
                                        <img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Steven Thompson</h4>
                                            <span> Visual Designer </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <img alt="..." src="assets/images/avatar-8.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Ella Patterson</h4>
                                            <span> Web Editor </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <img alt="..." src="assets/images/avatar-9.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Kenneth Ross</h4>
                                            <span> Senior Designer </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Ella Patterson</h4>
                                            <span> Web Editor </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="media">
                                    <a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
                                        <img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
                                        <div class="media-body">
                                            <h4 class="media-heading">Kenneth Ross</h4>
                                            <span> Senior Designer </span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="user-chat">
                        <div class="chat-content">
                            <div class="sidebar-content perfect-scrollbar">
                                <a class="sidebar-back pull-left" href="#" data-toggle-class="chat-open" data-toggle-target="#users"><i class="ti-angle-left"></i> <span>Back</span></a>
                                <ol class="discussion">
                                    <li class="messages-date">
                                        Sunday, Feb 9, 12:58
                                    </li>
                                    <li class="self">
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                Hi, Nicole
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                How are you?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="other">
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                Hi, i am good
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="self">
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                Glad to see you ;)
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="messages-date">
                                        Sunday, Feb 9, 13:10
                                    </li>
                                    <li class="other">
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                What do you think about my new Dashboard?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="messages-date">
                                        Sunday, Feb 9, 15:28
                                    </li>
                                    <li class="other">
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                Alo...
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                Are you there?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="self">
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                Hi, i am here
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                Your Dashboard is great
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="messages-date">
                                        Friday, Feb 7, 23:39
                                    </li>
                                    <li class="other">
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                How does the binding and digesting work in AngularJS?, Peter?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="self">
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                oh that's your question?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                little reduntant, no?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                literally we get the question daily
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="other">
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                I know. I, however, am not a nerd, and want to know
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="self">
                                        <div class="message">
                                            <div class="message-name">
                                                Peter Clark
                                            </div>
                                            <div class="message-text">
                                                for this type of question, wouldn't it be better to try Google?
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-1.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="other">
                                        <div class="message">
                                            <div class="message-name">
                                                Nicole Bell
                                            </div>
                                            <div class="message-text">
                                                Lucky for us :)
                                            </div>
                                            <div class="message-avatar">
                                                <img src="assets/images/avatar-2.jpg" alt="">
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="message-bar">
                            <div class="message-inner">
                                <a class="link icon-only" href="#"><i class="fa fa-camera"></i></a>
                                <div class="message-area">
                                    <textarea placeholder="Message"></textarea>
                                </div>
                                <a class="link" href="#">
                                    Send
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="off-favorites">
                <div class="users-list">
                    <div class="sidebar-content perfect-scrollbar">
                        <h5 class="sidebar-title">Favorites</h5>
                        <ul class="media-list">
                            <li class="media">
                                <a href="#">
                                    <img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
                                    <div class="media-body">
                                        <h4 class="media-heading">Nicole Bell</h4>
                                        <span> Content Designer </span>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="#">
                                    <div class="user-label">
                                        <span class="label label-success">3</span>
                                    </div>
                                    <img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
                                    <div class="media-body">
                                        <h4 class="media-heading">Steven Thompson</h4>
                                        <span> Visual Designer </span>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="#">
                                    <img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
                                    <div class="media-body">
                                        <h4 class="media-heading">Ella Patterson</h4>
                                        <span> Web Editor </span>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="#">
                                    <img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
                                    <div class="media-body">
                                        <h4 class="media-heading">Kenneth Ross</h4>
                                        <span> Senior Designer </span>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="#">
                                    <img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
                                    <div class="media-body">
                                        <h4 class="media-heading">Ella Patterson</h4>
                                        <span> Web Editor </span>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="#">
                                    <img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
                                    <div class="media-body">
                                        <h4 class="media-heading">Kenneth Ross</h4>
                                        <span> Senior Designer </span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="off-settings">
                <div class="sidebar-content perfect-scrollbar">
                    <h5 class="sidebar-title">General Settings</h5>
                    <ul class="media-list">
                        <li class="media">
                            <div class="padding-10">
                                <div class="display-table-cell">
                                    <input type="checkbox" class="js-switch" checked />
                                </div>
                                <span class="display-table-cell vertical-align-middle padding-left-10">Enable Notifications</span>
                            </div>
                        </li>
                        <li class="media">
                            <div class="padding-10">
                                <div class="display-table-cell">
                                    <input type="checkbox" class="js-switch" />
                                </div>
                                <span class="display-table-cell vertical-align-middle padding-left-10">Show your E-mail</span>
                            </div>
                        </li>
                        <li class="media">
                            <div class="padding-10">
                                <div class="display-table-cell">
                                    <input type="checkbox" class="js-switch" checked />
                                </div>
                                <span class="display-table-cell vertical-align-middle padding-left-10">Show Offline Users</span>
                            </div>
                        </li>
                        <li class="media">
                            <div class="padding-10">
                                <div class="display-table-cell">
                                    <input type="checkbox" class="js-switch" checked />
                                </div>
                                <span class="display-table-cell vertical-align-middle padding-left-10">E-mail Alerts</span>
                            </div>
                        </li>
                        <li class="media">
                            <div class="padding-10">
                                <div class="display-table-cell">
                                    <input type="checkbox" class="js-switch" />
                                </div>
                                <span class="display-table-cell vertical-align-middle padding-left-10">SMS Alerts</span>
                            </div>
                        </li>
                    </ul>
                    <div class="save-options">
                        <button class="btn btn-success">
                            <i class="icon-settings"></i><span>Save Changes</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end: OFF-SIDEBAR -->
<!-- start: SETTINGS -->
<div class="settings panel panel-default hidden-xs hidden-sm" id="settings">
    <!-- <button ct-toggle="toggle" data-toggle-class="active" data-toggle-target="#settings" class="btn btn-default">
            <i class="fa fa-spin fa-gear"></i>
    </button> -->
    <div class="panel-heading">
        Style Selector
    </div>
    <div class="panel-body">
        <!-- start: FIXED HEADER -->
        <div class="setting-box clearfix">
            <span class="setting-title pull-left"> Fixed header</span>
            <span class="setting-switch pull-right">
                <input type="checkbox" class="js-switch" id="fixed-header" />
            </span>
        </div>
        <!-- end: FIXED HEADER -->
        <!-- start: FIXED SIDEBAR -->
        <div class="setting-box clearfix">
            <span class="setting-title pull-left">Fixed sidebar</span>
            <span class="setting-switch pull-right">
                <input type="checkbox" class="js-switch" id="fixed-sidebar" />
            </span>
        </div>
        <!-- end: FIXED SIDEBAR -->
        <!-- start: CLOSED SIDEBAR -->
        <div class="setting-box clearfix">
            <span class="setting-title pull-left">Closed sidebar</span>
            <span class="setting-switch pull-right">
                <input type="checkbox" class="js-switch" id="closed-sidebar" />
            </span>
        </div>
        <!-- end: CLOSED SIDEBAR -->
        <!-- start: FIXED FOOTER -->
        <div class="setting-box clearfix">
            <span class="setting-title pull-left">Fixed footer</span>
            <span class="setting-switch pull-right">
                <input type="checkbox" class="js-switch" id="fixed-footer" />
            </span>
        </div>
        <!-- end: FIXED FOOTER -->
        <!-- start: THEME SWITCHER -->
        <div class="colors-row setting-box">
            <div class="color-theme theme-1">
                <div class="color-layout">
                    <label>
                        <input type="radio" name="setting-theme" value="theme-1">
                        <span class="ti-check"></span>
                        <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                        <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                    </label>
                </div>
            </div>
            <div class="color-theme theme-2">
                <div class="color-layout">
                    <label>
                        <input type="radio" name="setting-theme" value="theme-2">
                        <span class="ti-check"></span>
                        <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                        <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="colors-row setting-box">
            <div class="color-theme theme-3">
                <div class="color-layout">
                    <label>
                        <input type="radio" name="setting-theme" value="theme-3">
                        <span class="ti-check"></span>
                        <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                        <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                    </label>
                </div>
            </div>
            <div class="color-theme theme-4">
                <div class="color-layout">
                    <label>
                        <input type="radio" name="setting-theme" value="theme-4">
                        <span class="ti-check"></span>
                        <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                        <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="colors-row setting-box">
            <div class="color-theme theme-5">
                <div class="color-layout">
                    <label>
                        <input type="radio" name="setting-theme" value="theme-5">
                        <span class="ti-check"></span>
                        <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                        <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                    </label>
                </div>
            </div>
            <div class="color-theme theme-6">
                <div class="color-layout">
                    <label>
                        <input type="radio" name="setting-theme" value="theme-6">
                        <span class="ti-check"></span>
                        <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                        <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                    </label>
                </div>
            </div>
        </div>
        <!-- end: THEME SWITCHER -->
    </div>
</div>
<!-- end: SETTINGS -->
</div>
<!-- start: MAIN JAVASCRIPTS -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/modernizr/modernizr.js"></script>
<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="vendor/switchery/switchery.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="vendor/Chart.js/Chart.min.js"></script>
<script src="vendor/jquery.sparkline/jquery.sparkline.min.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CLIP-TWO JAVASCRIPTS -->
<script src="assets/js/main.js"></script>

<script src="assets/datepicker/js/bootstrap-datepicker.js"></script>


<!-- start: JavaScript Event Handlers for this page -->
<script src="assets/js/index.js"></script>
<script>
            jQuery(document).ready(function() {
                Main.init();
                Index.init();
            });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
<?php ob_end_flush();
?>