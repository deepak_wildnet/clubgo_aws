<?php
  error_reporting(0);
  ob_start();
  session_start();
  $snam=$_SESSION['name'];
  //echo $a;
  if(!$snam)
  {

    header("location:login.php");
  }
?>

<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>ClubGo - Responsive Admin Template</title>
		<!-- start: META -->
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: GOOGLE FONTS -->
		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		<!-- end: GOOGLE FONTS -->
		<!-- start: MAIN CSS -->

		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="vendor/themify-icons/themify-icons.min.css">
		<link href="vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
		<link href="vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
		<link href="vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
		<!-- end: MAIN CSS -->
		<!-- start: CLIP-TWO CSS -->
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="assets/css/plugins.css">
		<link rel="stylesheet" href="assets/css/themes/theme-1.css" id="skin_color" />
<!--<link rel="stylesheet" href="assets/css/style1.css" />-->
<!--<link rel="stylesheet" href="assets/css/jquery.Jcrop.min1.css" type="text/css" />-->
<link rel="stylesheet" type="text/css" href="assets/css/imgareaselect-default.css" />

		<!-- end: CLIP-TWO CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- <script src="assets/js/jquery.imgareaselect.mi.js"></script> -->


<script type="text/javascript">

		
		$(document).ready(function(){

		$('#ep').keypress(function (e) {
   var regex = new RegExp("^[0-9-]+$");
   var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
   if (regex.test(str)) {
       return true;
   }

   e.preventDefault();
   return false;
	});

      $('#hep').keypress(function (e) {
   var regex = new RegExp("^[0-9-]+$");
   var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
   if (regex.test(str)) {
       return true;
   }

   e.preventDefault();
   return false;
  });

		$('#uph').keypress(function (e) {
   var regex = new RegExp("^[0-9-]+$");
   var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
   if (regex.test(str)) {
       return true;
   }

   e.preventDefault();
   return false;
	});

	});


		</script>

    <script type="text/javascript">

$(document).ready(function(){


    var maxField = 10; //Input fields increment limitation
    

    var addButtonmenu = $('.add_button_menu');
    var removeButtonmenu= $('.remove_button_menu');

    
    var addButtonfull = $('.add_button_full');
    var removeButtonfull = $('.remove_button_full');

    var addButtonvenue = $('.add_button_venue');
    var removeButtonvenue = $('.remove_button_venue');

    var addButtonbar = $('.add_button_bar');
    var removeButtonbar = $('.remove_button_bar');

    var addButtoncuis = $('.add_button_cuis');
    var removeButtoncuis = $('.remove_button_cuis');

    var addButtonhigh = $('.add_button_high');
    var removeButtonhigh = $('.remove_button_high');

    var addButtoncost = $('.add_button_cost');
    var removeButtoncost = $('.remove_button_cost');

     var addButtonoff = $('.add_button_off');
    var removeButtonoff = $('.remove_button_off');

///////////////////////////////////////////////////////////////

      var wrappermenu = $('#frmmenu');

      var wrapperfull = $('#frmfull');

      var wrappervenue = $('#frmvenue');

      var wrapperbar = $('#frmbar');

      var wrappercuis = $('#frmcuis');

      var wrapperhigh = $('#frmhigh');

      var wrappercost = $('#frmcost');

      var wrapperoff = $('#frmoff');
  
   
///////////////////////////////////////////////////////////////

  var fieldHTMLmenu = '<div><a href="javascript:void(0);" class="remove_button_menu" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLmenu1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Food Image (Image Size must be less than 600*600)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="menuimage[]" style="display:inline-block;"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_menu1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';
    

  var fieldHTMLfull = '<div><a href="javascript:void(0);" class="remove_button_full" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLfull1 = '<div width="500px"><div><label> Full Image</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="fullimage[]"/></div></br><a href="javascript:void(0);" class="remove_button_full1" title="Remove field"><img src="images/remove-icon.png"/></a></div>';
    


  var fieldHTMLvenue = '<div><a href="javascript:void(0);" class="remove_button_venue" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLvenue1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Venue Photo (Image Size must be less than 600*600)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="venueimage[]" style="display:inline-block;"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_venue1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';
  

  var fieldHTMLbar = '<div><a href="javascript:void(0);" class="remove_button_bar" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLbar1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Bar Photo (Image Size must be less than 600*600)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="barimage[]" style="display:inline-block;"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_bar1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';
      

  var fieldHTMLcuis = '<div><a href="javascript:void(0);" class="remove_button_cuis" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLcuis1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Cuisines</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="cuisines[]"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_cuis1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';


  var fieldHTMLhigh = '<div><a href="javascript:void(0);" class="remove_button_high" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLhigh1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Facilities</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="highlights[]"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_high1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';
    

  var fieldHTMLcost = '<div><a href="javascript:void(0);" class="remove_button_cost" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLcost1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Cost</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="cost[]"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_cost1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';
    

  var fieldHTMLoff = '<div><a href="javascript:void(0);" class="remove_button_off" title="Remove field"><img src="images/remove-icon.png"/></a></div>';

  var fieldHTMLoff1 = '<div width="500px" style="padding:20px 0px 0px 20px;"><div><label> Offers</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="ioffer[]"/>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button_off1" title="Remove field"><img src="images/remove-icon.png"/></a></div></div>';
    

    


////////////////////////////////////////////////////////////////////////////

    var fieldHTML4 = '<div>hello</div>';
    var fieldHTML5 = '';
    var x = 1; //Initial field counter is 1

   



    $(addButtonmenu).click(function(){ 

       $(wrappermenu).append(fieldHTMLmenu1);
       


    });

 $('.remove_button_menu').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrappermenu).on('click', '.remove_button_menu1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });



     $(addButtonfull).click(function(){ 

       $(wrapperfull).append(fieldHTMLfull1);
       


    });

 $('.remove_button_full').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrapperfull).on('click', '.remove_button_full1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });



    $(addButtonvenue).click(function(){ 

       $(wrappervenue).append(fieldHTMLvenue1);
       


    });

 $('.remove_button_venue').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrappervenue).on('click', '.remove_button_venue1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


      $(addButtonbar).click(function(){ 

       $(wrapperbar).append(fieldHTMLbar1);
       


    });

 $('.remove_button_bar').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrapperbar).on('click', '.remove_button_bar1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


    $(addButtoncuis).click(function(){ 

       $(wrappercuis).append(fieldHTMLcuis1);
       


    });

 $('.remove_button_cuis').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrappercuis).on('click', '.remove_button_cuis1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


    $(addButtonhigh).click(function(){ 

       $(wrapperhigh).append(fieldHTMLhigh1);
       


    });

 $('.remove_button_high').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrapperhigh).on('click', '.remove_button_high1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });

    $(addButtoncost).click(function(){ 

       $(wrappercost).append(fieldHTMLcost1);
       


    });

 $('.remove_button_cost').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrappercost).on('click', '.remove_button_cost1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


    $(addButtonoff).click(function(){ 

       $(wrapperoff).append(fieldHTMLoff1);
       


    });

 $('.remove_button_off').click(function(e){ //Once remove button is clicked
        e.preventDefault();
       
      var p= $(this).parent('div').attr('id');
     
      $("#"+p+"").remove();
      
    });


    $(wrapperoff).on('click', '.remove_button_off1', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });





      
});

</script>



		<script type="text/javascript">

   function formvalidation()
{
	
var c,d;
  var a=document.getElementById("t1");
  a=a.removeAttribute("required");
 
   var b=document.getElementById("t2");
  b=b.removeAttribute("required");
 
  var c=document.getElementById("t3");
  c=c.removeAttribute("required");
 
   var d=document.getElementById("t4");
  d=d.removeAttribute("required");
 
 var x=document.getElementById("t5");
  x=x.removeAttribute("required");

   var y=document.getElementById("uph");
  y=y.removeAttribute("required");
 
 var e=document.getElementById("t6");
  e=e.removeAttribute("required");
 
 var g=document.getElementById("t7");
  g=g.removeAttribute("required");
 
 var h=document.getElementById("t8");
  h=h.removeAttribute("required");
 
 var i=document.getElementById("t9");
  i=i.removeAttribute("required");

  var j=document.getElementById("t10");
  j=j.removeAttribute("required");

  var k=document.getElementById("t11");
  k=k.removeAttribute("required");

  var l=document.getElementById("t12");
  l=l.removeAttribute("required");

  var m=document.getElementById("t13");
  m=m.removeAttribute("required");

  var n=document.getElementById("t14");
  n=n.removeAttribute("required");

  var o=document.getElementById("t15");
  o=o.removeAttribute("required");

  var p=document.getElementById("t16");
  p=p.removeAttribute("required");

  var q=document.getElementById("t17");
  q=q.removeAttribute("required");

   var z=document.getElementById("t18");
  z=z.removeAttribute("required");

var r=document.getElementById("t19");
  r=r.removeAttribute("required");

var s=document.getElementById("t20");
  s=s.removeAttribute("required");

var mi=document.getElementById("mi");
  mi=mi.removeAttribute("required");

var fi=document.getElementById("fi");
  fi=fi.removeAttribute("required");

var vi=document.getElementById("vi");
  vi=vi.removeAttribute("required");

var t=document.getElementById("t21");
  t=t.removeAttribute("required");


    var ep=document.getElementById("ep");
  ep=ep.removeAttribute("required");

  var u=document.getElementById("t22");
  u=u.removeAttribute("required");

  var v=document.getElementById("t23");
  v=v.removeAttribute("required");

  var w=document.getElementById("t24");
  w=w.removeAttribute("required");


 
}	



		</script>

<script type="text/javascript">
	


</script>		
		
</head>
	<!-- end: HEAD -->

<style>

#imcu{
  float:right; margin-right:500px; margin-top:-40px; 
}


.col-lg-6 {
width:100%!important;
}

#intro {

 background-color:#F2F2F2;
}

.space1{
  padding-left:40px; 
}

.space2{
  padding:20px 0px 0px 20px;
}

</style>


	
 	<?php
	include('config.php');
	@$action=$_GET['action'];
	
   if (@$_POST['can'])
   {

     header('location:items.php');

   }
@$featured=$_POST['featured'];
   @$a=$_POST['monday'];
@$b=$_POST['tuesday'];
@$c=$_POST['wednesday'];
@$d=$_POST['thursday'];
@$e=$_POST['friday'];
@$f=$_POST['saturday'];
@$g=$_POST['sunday'];
 	@$title=addslashes($_POST['ititle']);
    @$desc=addslashes($_POST['idesc']);
    @$phone=$_POST['iphone'];
    @$location=addslashes($_POST['iloc']);
    @$offer=addslashes(implode(",",$_POST['ioffer']));
    @$review=addslashes($_POST['ireviews']);
    @$rating=$_POST['irating'];
    @$idd=addslashes($_POST['mobid']);
    @$happi=$_POST['happ'];
    @$featured=$_POST['featured'];
 @$pricing=$_POST['ipricing'];
@$open=$_POST['open'];
//echo "$open";
@$close=$_POST['close'];
@$coordi=$_POST['coordi'];

@$prior=$_POST['prior'];
@$hprior=$_POST['hprior'];

if($hprior=='')
{
   $hprior = mt_rand(1000,9999);
} 

@$cuisines=addslashes(implode(",",$_POST['cuisines']));

@$highlights=addslashes(implode(",",$_POST['highlights']));

@$cost=addslashes(implode(",",$_POST['cost']));

@$busynights=implode(",",$_POST['busynights']);

//echo $busynights;

@$desctitle=$_POST['desctitle'];

@$known=addslashes($_POST['known']);

@$address=addslashes($_POST['address']);

@$status=$_POST['status'];
@$metro=$_POST['metro'];

$coord=explode(",", $coordi);
$coord[0]=substr($coord[0],-9);
$coord[1]=substr($coord[1],-9);

if(isset($_POST['sen'])){



if(isset($_FILES['image1']) || isset($_FILES['image2']) || isset($_FILES['image3']) || isset($_FILES['image4']) || isset($_FILES['image5']) || isset($_FILES['image6'])  || isset($_FILES['image9']))
{




@$filepath="images/";
@$filepath1="video/";
//@$format=".png";

 @$imgname1=$_FILES['image1']['name'];
  @$imgsize1=$_FILES['image1']['size'];
  @$file_ext1=strtolower(end(explode('.',$_FILES['image1']['name'])));
  @$expansions1=array("png","jpg","jpeg");  
  $list1 = getimagesize($_FILES['image1']['tmp_name']); 

@$imgname2=$_FILES['image2']['name'];
  @$imgsize2=$_FILES['image2']['size'];
  @$file_ext2=strtolower(end(explode('.',$_FILES['image2']['name'])));
  @$expansions2=array("png","jpg","jpeg");
  $list2 = getimagesize($_FILES['image2']['tmp_name']); 

@$imgname3=$_FILES['image3']['name'];
  @$imgsize3=$_FILES['image3']['size'];
  @$file_ext3=strtolower(end(explode('.',$_FILES['image3']['name'])));
  @$expansions3=array("png","jpg","jpeg");
  $list3 = getimagesize($_FILES['image3']['tmp_name']); 


@$imgname4=$_FILES['image4']['name'];
  @$imgsize4=$_FILES['image4']['size'];
  @$file_ext4=strtolower(end(explode('.',$_FILES['image4']['name'])));
  @$expansions4=array("png","jpg","jpeg");
  $list4 = getimagesize($_FILES['image4']['tmp_name']); 

  @$imgname5=$_FILES['image5']['name'];
  @$imgsize5=$_FILES['image5']['size'];
  @$file_ext5=strtolower(end(explode('.',$_FILES['image5']['name'])));
  @$expansions5=array("png","jpg","jpeg");
  $list5 = getimagesize($_FILES['image5']['tmp_name']); 




  @$imgname6=$_FILES['image6']['name'];
  @$imgsize6=$_FILES['image6']['size'];
  @$file_ext6=strtolower(end(explode('.',$_FILES['image6']['name'])));
  @$expansions6=array("png","jpg","jpeg");
  $list6 = getimagesize($_FILES['image6']['tmp_name']); 



  @$imgname9=$_FILES['image9']['name'];
  @$imgsize9=$_FILES['image9']['size'];
  @$file_ext9=strtolower(end(explode('.',$_FILES['image9']['name'])));
  @$expansions9=array("mp4");
  
  
  

  @$imgname1= preg_replace('/( *)/', '', $imgname1);
  @$imgname2= preg_replace('/( *)/', '', $imgname2);
  @$imgname3= preg_replace('/( *)/', '', $imgname3);
  @$imgname4= preg_replace('/( *)/', '', $imgname4);

  @$imgname5= preg_replace('/( *)/', '', $imgname5);

  @$imgname6= preg_replace('/( *)/', '', $imgname6);
  
  @$imgname9= preg_replace('/( *)/', '', $imgname9);

  @$imgid='';
  @$message='';

   @$newimage1='';
  @$newimage2='';
  @$newimage3='';
  @$newimage4='';
@$newimage5='';
  @$newimage6='';

  @$newimage9='';

  @$update='';


@$chk=true;
if($imgname1!=='')
{
  if(in_array($file_ext1, $expansions1)==false)
     { $chk=false;
       $errors1="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize1 > 2097152)
    {$chk=false;
    $errors1='File size must be less than 2 MB';
    } 
    if($list1[0] == 1024)
    {
        $chk=true;
    }
    else{
    $chk=false;
    $errors1='File width must be 1024 pixels';
    }
     if($list1[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors1='File height must be 768 pixels';
    }
}
if($imgname2!=='')
{
  if(in_array($file_ext2, $expansions2)==false)
    { $chk=false;
       $errors2="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize2 > 2097152)
    {$chk=false;
    $errors2='File size must be less than 2 MB';
    } 
    if($list2[0] == 1024)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors2='File width must be 1024 pixels';
    }
     if($list2[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors2='File height must be 768 pixels';
    }
}
if($imgname3!=='')
{

if(in_array($file_ext3, $expansions3)==false)
    { $chk=false;
       $errors3="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize3 > 2097152)
    {$chk=false;
    $errors3='File size must be less than 2 MB';
    } 
    if($list3[0] == 1024)
    {$chk=true;
   
    }
    else{
    $chk=false;
    $errors3='File width must be 1024 pixels';
    }
     if($list3[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors3='File height must be 768 pixels';
    }
}
if($imgname4!=='')
{
  if(in_array($file_ext4, $expansions4)==false)
    { $chk=false;
       $errors4="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize4 > 2097152)
    {$chk=false;
    $errors4='File size must be less than 2 MB';
    } 
    if($list4[0] == 600)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors4='File width must be 600 pixels';
    }
     if($list4[1] == 400)
    {$chk=true;
    
    } 
    else{
    $chk=false;
     $errors4='File height must be 400 pixels';
    }
}
if($imgname5!=='')
{
  if(in_array($file_ext5, $expansions5)==false)
    { $chk=false;
       $errors5="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize5 > 2097152)
    {$chk=false;
    $errors5='File size must be less than 2 MB';
    }
    if($list5[0] == 320)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors5='File width must be 320 pixels';
    }

     if($list5[1] == 320)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors5='File height must be 320 pixels';
    }
}
if($imgname6!=='')
{
  if(in_array($file_ext6, $expansions6)==false)
    { $chk=false;
       $errors6="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize6 > 2097152)
    {$chk=false;
    $errors6='File size must be less than 2 MB';
    } 
    if($list6[0] == 1024)
    {$chk=true;
    }
    else{
    $chk=false;
    $errors6='File width must be 1024 pixels';
    }
     if($list6[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors6='File height must be 768 pixels';
    }
}
if($imgname9!=='')
{
  if(in_array($file_ext9, $expansions9)==false)
    { $chk=false;
       $errors9="extension not allowed, please choose a mp4  file.";
    }
    if($imgsize9 > 20971520)
    {$chk=false;
    $errors9='File size must be less than 20 MB';
    }
}
/*if($imgname1!=='' || $imgname2!=='' || $imgname3!=='' || $imgname4!=='' || $imgname6!=='' || $imgname9!=='' || $imgname5!=='')
{

   if(in_array($file_ext1, $expansions1)==false)
     { $chk=false;
       $errors1="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize1 > 2097152)
    {$chk=false;
    $errors1='File size must be less than 2 MB';
    } 
    if($list1[0] = 1024)
    {$chk=false;
    $errors1='File width must be 1024 pixels';
    }
     if($list1[1] = 768)
    {$chk=false;
    $errors1='File height must be 768 pixels';
    }


if(in_array($file_ext2, $expansions2)==false)
    { $chk=false;
       $errors2="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize2 > 2097152)
    {$chk=false;
    $errors2='File size must be less than 2 MB';
    } 
    if($list2[0] = 1024)
    {$chk=false;
    $errors2='File width must be 1024 pixels';
    }
     if($list2[1] = 768)
    {$chk=false;
    $errors2='File height must be 768 pixels';
    }

       


if(in_array($file_ext3, $expansions3)==false)
    { $chk=false;
       $errors3="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize3 > 2097152)
    {$chk=false;
    $errors3='File size must be less than 2 MB';
    } 
    if($list3[0] = 1024)
    {$chk=false;
    $errors3='File width must be 1024 pixels';
    }
     if($list3[1] = 768)
    {$chk=false;
    $errors3='File height must be 768 pixels';
    }



if(in_array($file_ext4, $expansions4)==false)
    { $chk=false;
       $errors4="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize4 > 2097152)
    {$chk=false;
    $errors4='File size must be less than 2 MB';
    } 
    if($list4[0] = 600)
    {$chk=false;
    $errors4='File width must be 600 pixels';
    }
     if($list4[1] = 400)
    {$chk=false;
    $errors4='File height must be 400 pixels';
    }

    if(in_array($file_ext5, $expansions5)==false)
    { $chk=false;
       $errors5="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize5 > 2097152)
    {$chk=false;
    $errors5='File size must be less than 2 MB';
    }
    if($list5[0] = 320)
    {$chk=false;
    $errors5='File width must be 320 pixels';
    }
     if($list5[1] = 320)
    {$chk=false;
    $errors5='File height must be 320 pixels';
    }




    if(in_array($file_ext6, $expansions6)==false)
    { $chk=false;
       $errors6="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize6 > 2097152)
    {$chk=false;
    $errors6='File size must be less than 2 MB';
    } 
    if($list6[0] = 1024)
    {$chk=false;
    $errors6='File width must be 1024 pixels';
    }
     if($list6[1] = 768)
    {$chk=false;
    $errors6='File height must be 768 pixels';
    }

     

    if(in_array($file_ext9, $expansions9)==false)
    { $chk=false;
       $errors9="extension not allowed, please choose a mp4  file.";
    }
    if($imgsize9 > 20971520)
    {$chk=false;
    $errors9='File size must be less than 20 MB';
    }


}*/


}

   


    

if(isset($action) and $action=='add'){
	


 
if($chk==true){



 @$sql=mysqli_query($conn,"insert into items(cate_id, item_title,item_location,item_offer,item_rating,item_img1,item_img2,item_img3,item_img4,cover_img,happy_hours,item_lat,item_lng,item_feat,item_video,item_prior,cuisines,highlights,cost,known_for,busy_nights,home_prior,item_address,item_published,feat_image,metro_station) values('$idd','$title','$location','$offer','$rating','$imgname1','$imgname2','$imgname3','$imgname6','$imgname4','$a[0]*$a[1]$a[2]-$a[3]$a[4]#$a[5]@$b[0]*$b[1]$b[2]-$b[3]$b[4]#$b[5]@$c[0]*$c[1]$c[2]-$c[3]$c[4]#$c[5]@$d[0]*$d[1]$d[2]-$d[3]$d[4]#$d[5]@$e[0]*$e[1]$e[2]-$e[3]$e[4]#$e[5]@$f[0]*$f[1]$f[2]-$f[3]$f[4]#$f[5]@$g[0]*$g[1]$g[2]-$g[3]$g[4]#$g[5]','$coord[0]','$coord[1]','$featured','$imgname9','$prior','$cuisines','$highlights','$cost','$known','$busynights','$hprior','$address','$status','$imgname5','$metro')");
 
       if(!$sql)
       {

        die("Error".mysqli_error());
       }

else{


          @$Getid=mysqli_query($conn,"SELECT max(item_id) from items");
  
           while($row=mysqli_fetch_array($Getid))
               {
              $imgid1=$row[0];
              $imgid2=$row[0];
              $imgid3=$row[0];
              $imgid4=$row[0]; 
              $imgid5=$row[0];  
              $imgid6=$row[0];
             
              $imgid9=$row[0];
               }



              @$no=mysqli_num_rows($Getid);


              if($no==1)
              {
                  if($imgname1!=='')
                  {
                    $newimage1=$filepath.$imgid1.'.1.'.$file_ext1;
                  }
                  else
                  {

                  }
                  if($imgname2!=='')
                  {
                    $newimage2=$filepath.$imgid2.'.2.'.$file_ext2;
                  }
                  else
                  {

                  }
                   if($imgname3!=='')
                  {
                    $newimage3=$filepath.$imgid3.'.3.'.$file_ext3;
                  }
                  else
                  {

                  }
                   if($imgname4!=='')
                  {
                    $newimage4=$filepath.$imgid4.'.4.'.$file_ext4;
                  }
                  else
                  {

                  }
                  if($imgname5!=='')
                  {
                    $newimage5=$filepath.$imgid5.'.5.'.$file_ext5;
                  }
                  else
                  {

                  }
                  if($imgname6!=='')
                  {
                    $newimage6=$filepath.$imgid6.'.6.'.$file_ext6;
                  }
                  else
                  {

                  }
                   if($imgname9!=='')
                  {
                    $newimage9=$filepath1.$imgid9.'1.'.$file_ext9;
                  }
                  else
                  {

                  }


       
                 
              }



            $update=mysqli_query($conn,"UPDATE  items set item_img1='$newimage1',item_img2='$newimage2',item_img3='$newimage3',item_img4='$newimage6',cover_img='$newimage4',item_video='$newimage9',feat_image='$newimage5' where item_id='$imgid1' and '$imgid2' and '$imgid3' and '$imgid4' and '$imgid5' and '$imgid6'  and '$imgid9' ");   
     
     
                 if(!$update)
                {

                 die("Error".mysqli_error());
                }

                else
                {   
                 move_uploaded_file($_FILES['image1']['tmp_name'],$newimage1);

                 move_uploaded_file($_FILES['image2']['tmp_name'],$newimage2);

                 move_uploaded_file($_FILES['image3']['tmp_name'],$newimage3);

                 move_uploaded_file($_FILES['image4']['tmp_name'],$newimage4);

                 move_uploaded_file($_FILES['image5']['tmp_name'],$newimage5);

                 move_uploaded_file($_FILES['image6']['tmp_name'],$newimage6);

                 

                  move_uploaded_file($_FILES['image9']['tmp_name'],$newimage9);


}
}
}

                  $menuimg = array();

                for($im=0;$im<count($_FILES['menuimage']['name']);$im++)
              {

                if(isset($_FILES['menuimage'])) 
                {

              
                @$filepath2="images/items/menu/";
              

                 @$imgname15=$_FILES['menuimage']['name'][$im];
                  @$imgsize15=$_FILES['menuimage']['size'][$im];
                  @$file_ext15=strtolower(end(explode('.',$_FILES['menuimage']['name'][$im])));
                  @$expansions15=array("png","jpg","jpeg"); 
                  $list15 = getimagesize($_FILES['menuimage']['tmp_name']);  

                

                  @$imgname15= preg_replace('/( *)/', '', $imgname15);
                  
                  @$imgid='';
                  @$message='';

                   @$newimage15='';
                  
                  @$update='';


              @$chk=true;
              if($imgname15!=='')
              {
                  //print_r($expansions15);
                  if(!in_array($file_ext15,$expansions15))
                  {
                    
                    $chk=false;
                     $errors15="extension not allowed, please choose a png, jpg or jpeg file.";
                  }
                 
                  if($imgsize15 > 2097152)
                  {
                    $chk=false;
                  $errors15='File size must be less than 2 MB';
                  }
                  if($list15[0] > 600)
                  {$chk=false;
                  $errors15='File width must be less than 600 pixels';
                  }
                   if($list15[1] > 600)
                  {$chk=false;
                  $errors15='File height must be less than 600 pixels';
                  } 


              }


              }

                       if($imgname15!=='')
                       {
                      $newimage15=$filepath2.$imgid1.".".$im.".".$file_ext15;
                       }
                       else
                      {
                      }
            
                   if($chk==true){  

                      array_push($menuimg,$newimage15);

                     move_uploaded_file($_FILES['menuimage']['tmp_name'][$im],$newimage15); 
                   }
                   else
                   {
                      @$delete=mysqli_query($conn,"DELETE from items where item_id='$imgid1'");
    
                   }


              }


            $menuimg1=implode(",",$menuimg);

            $updatemenu=mysqli_query($conn,"UPDATE  items set menu_image='$menuimg1' where item_id='$imgid1'");   
          
       
 /*$fullimg = array();

  for($j=0;$j<count($_FILES['fullimage']['name']);$j++)
{

  if(isset($_FILES['fullimage'])) 
  {


  @$filepath2="images/items/full/";
 

   @$imgname16=$_FILES['fullimage']['name'][$j];
    @$imgsize16=$_FILES['fullimage']['size'][$j];
    @$file_ext16=strtolower(end(explode('.',$_FILES['fullimage']['name'][$j])));
    @$expansions16=array("png","jpg","jpeg");  
    

   

    @$imgname16= preg_replace('/( *)/', '', $imgname16);
    
    @$imgid='';
    @$message='';

     @$newimage16='';
    
    @$update='';


@$chk=true;
if($imgname16!=='')
{

   if(in_array($file_ext16, $expansions16==false))
     { 
      $chk=false;
       $errors="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize16 > 2097152)
    {
      $chk=false;
    $errors='File size must be less than 2 MB';
    } 


}


}
         if($imgname16!=='')
           {
        $newimage16=$filepath2.$imgid1.".".$j.".".$file_ext16;
           }
           else
           {

           }

       

        array_push($fullimg,$newimage16);

       move_uploaded_file($_FILES['fullimage']['tmp_name'][$j],$newimage16); 

}


$fullimg1=implode(",",$fullimg);

$updatefull=mysqli_query($conn,"UPDATE  items set full_image='$fullimg1' where item_id='$imgid1'");   
          */
       
                     $venueimg = array();

                      for($k=0;$k<count($_FILES['venueimage']['name']);$k++)
                    {

                      if(isset($_FILES['venueimage'])) 
                      {

                
                      @$filepath2="images/items/venue/";
                  

                       @$imgname17=$_FILES['venueimage']['name'][$k];
                        @$imgsize17=$_FILES['venueimage']['size'][$k];
                        @$file_ext17=strtolower(end(explode('.',$_FILES['venueimage']['name'][$k])));
                        @$expansions17=array("png","jpg","jpeg");  
                        $list17 = getimagesize($_FILES['menuimage']['tmp_name']); 

                      

                        @$imgname17= preg_replace('/( *)/', '', $imgname17);
                        
                        @$imgid='';
                        @$message='';

                         @$newimage17='';
                        
                        @$update='';


                        @$chk1=true;
                    if($imgname17!=='')
                    {

                       if(!in_array($file_ext17, $expansions17))
                         { 
                          $chk1=false;
                           $errors17="extension not allowed, please choose a png, jpg or jpeg file.";
                        }
                        if($imgsize17 > 2097152)
                        {
                          $chk1=false;
                        $errors17='File size must be less than 2 MB';
                        }
                        if($list17[0] > 600)
                        {$chk1=false;
                        $errors17='File width must be less than 600 pixels';
                        }
                         if($list17[1] > 600)
                        {$chk1=false;
                        $errors17='File height must be less than 600 pixels';
                        }  


                    }


                    }
                             if($imgname17!=='')
                                    {
                            $newimage17=$filepath2.$imgid1.".".$k.".".$file_ext17;
                                   }
                                 else
                                {

                                }
                    
                           if($chk1==true){

                            array_push($venueimg,$newimage17);

                           move_uploaded_file($_FILES['venueimage']['tmp_name'][$k],$newimage17); 
                         }
                           else
                         {
                            @$delete=mysqli_query($conn,"DELETE from items where item_id='$imgid1'");
          
                         }

                    }


                    $venueimg=implode(",",$venueimg);

                    $updatevenue=mysqli_query($conn,"UPDATE  items set venue_image='$venueimg' where item_id='$imgid1'");   
                              
   
    ////////////////////////////////////////////////////////////////////////////////////////                

                     $barimg = array();

                      for($bp=0;$bp<count($_FILES['barimage']['name']);$bp++)
                    {

                      if(isset($_FILES['barimage'])) 
                      {

                
                      @$filepath3="images/items/bar/";
                  

                       @$imgname18=$_FILES['barimage']['name'][$bp];
                        @$imgsize18=$_FILES['barimage']['size'][$bp];
                        @$file_ext18=strtolower(end(explode('.',$_FILES['barimage']['name'][$bp])));
                        @$expansions18=array("png","jpg","jpeg");  
                        $list18 = getimagesize($_FILES['barimage']['tmp_name']); 

                      

                        @$imgname18= preg_replace('/( *)/', '', $imgname18);
                        
                        @$imgid='';
                        @$message='';

                         @$newimage18='';
                        
                        @$update='';


                        @$chk1=true;
                    if($imgname18!=='')
                    {

                       if(!in_array($file_ext18, $expansions18))
                         { 
                          $chk1=false;
                           $errors18="extension not allowed, please choose a png, jpg or jpeg file.";
                        }
                        if($imgsize18 > 2097152)
                        {
                          $chk1=false;
                        $errors18='File size must be less than 2 MB';
                        }
                        if($list18[0] > 600)
                        {$chk1=false;
                        $errors18='File width must be less than 600 pixels';
                        }
                         if($list18[1] > 600)
                        {$chk1=false;
                        $errors18='File height must be less than 600 pixels';
                        }  


                    }


                    }
                             if($imgname18!=='')
                                    {
                            $newimage18=$filepath3.$imgid1.".".$bp.".".$file_ext18;
                                   }
                                 else
                                {

                                }
                    
                           if($chk1==true){

                            array_push($barimg,$newimage18);

                           move_uploaded_file($_FILES['barimage']['tmp_name'][$bp],$newimage18); 
                         }
                           else
                         {
                            @$delete=mysqli_query($conn,"DELETE from items where item_id='$imgid1'");
          
                         }

                    }


                    $barimg=implode(",",$barimg);

                    $updatebar=mysqli_query($conn,"UPDATE  items set bar_image='$barimg' where item_id='$imgid1'");   
                              
 


                    //header('location:items.php');







                  /*$message="Save record";
                  header('location:items.php');
*/
}


}

/*}
}
}*/




@$i=$_GET['id'];

    if(@$_POST['send']){
           if(isset($action) and $action=='edit'){

   

    if(isset($_FILES['image1']) || isset($_FILES['image2']) || isset($_FILES['image3']) || isset($_FILES['image4'])  || isset($_FILES['image6'])  || isset($_FILES['image9']) || isset($_FILES['image5']))
    {




@$filepath="images/";
@$filepath1="video/";


 @$imgname1=$_FILES['image1']['name'];
  @$imgsize1=$_FILES['image1']['size'];
  @$file_ext1=strtolower(end(explode('.',$_FILES['image1']['name'])));
  @$expansions1=array("png","jpg","jpeg");  
  $list1 = getimagesize($_FILES['image1']['tmp_name']);

@$imgname2=$_FILES['image2']['name'];
  @$imgsize2=$_FILES['image2']['size'];
  @$file_ext2=strtolower(end(explode('.',$_FILES['image2']['name'])));
  @$expansions2=array("png","jpg","jpeg");
  $list2 = getimagesize($_FILES['image2']['tmp_name']);

@$imgname3=$_FILES['image3']['name'];
  @$imgsize3=$_FILES['image3']['size'];
  @$file_ext3=strtolower(end(explode('.',$_FILES['image3']['name'])));
  @$expansions3=array("png","jpg","jpeg");
  $list3 = getimagesize($_FILES['image3']['tmp_name']);


@$imgname4=$_FILES['image4']['name'];
  @$imgsize4=$_FILES['image4']['size'];
  @$file_ext4=strtolower(end(explode('.',$_FILES['image4']['name'])));
  @$expansions4=array("png","jpg","jpeg");
  $list4 = getimagesize($_FILES['image4']['tmp_name']);

  @$imgname5=$_FILES['image5']['name'];
  @$imgsize5=$_FILES['image5']['size'];
  @$file_ext5=strtolower(end(explode('.',$_FILES['image5']['name'])));
  @$expansions5=array("png","jpg","jpeg");
  $list5 = getimagesize($_FILES['image5']['tmp_name']);


 
  @$imgname6=$_FILES['image6']['name'];
  @$imgsize6=$_FILES['image6']['size'];
  @$file_ext6=strtolower(end(explode('.',$_FILES['image6']['name'])));
  @$expansions6=array("png","jpg","jpeg");
  $list6 = getimagesize($_FILES['image6']['tmp_name']);



  @$imgname9=$_FILES['image9']['name'];
  @$imgsize9=$_FILES['image9']['size'];
  @$file_ext9=strtolower(end(explode('.',$_FILES['image9']['name'])));
  @$expansions9=array("mp4");
  

  @$imgname1= preg_replace('/( *)/', '', $imgname1);
  @$imgname2= preg_replace('/( *)/', '', $imgname2);
  @$imgname3= preg_replace('/( *)/', '', $imgname3);
  @$imgname4= preg_replace('/( *)/', '', $imgname4);
  
  @$imgname5= preg_replace('/( *)/', '', $imgname5);
  @$imgname6= preg_replace('/( *)/', '', $imgname6);

  @$imgname9= preg_replace('/( *)/', '', $imgname9);

  @$imgid='';
  @$message='';

   @$newimage1='';
  @$newimage2='';
  @$newimage3='';
  @$newimage4='';

  @$newimage5='';
  
  @$newimage6='';

  @$newimage9=''; 


  @$update='';


@$chk=true;
if($imgname1!=='')
{
  if(in_array($file_ext1, $expansions1)==false)
     { $chk=false;
       $errors1="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize1 > 2097152)
    {$chk=false;
    $errors1='File size must be less than 2 MB';
    } 
    if($list1[0] == 1024)
    {
        $chk=true;
    }
    else{
    $chk=false;
    $errors1='File width must be 1024 pixels';
    }
     if($list1[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors1='File height must be 768 pixels';
    }
}
if($imgname2!=='')
{
  if(in_array($file_ext2, $expansions2)==false)
    { $chk=false;
       $errors2="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize2 > 2097152)
    {$chk=false;
    $errors2='File size must be less than 2 MB';
    } 
    if($list2[0] == 1024)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors2='File width must be 1024 pixels';
    }
     if($list2[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors2='File height must be 768 pixels';
    }
}
if($imgname3!=='')
{

if(in_array($file_ext3, $expansions3)==false)
    { $chk=false;
       $errors3="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize3 > 2097152)
    {$chk=false;
    $errors3='File size must be less than 2 MB';
    } 
    if($list3[0] == 1024)
    {$chk=true;
   
    }
    else{
    $chk=false;
    $errors3='File width must be 1024 pixels';
    }
     if($list3[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors3='File height must be 768 pixels';
    }
}
if($imgname4!=='')
{
  if(in_array($file_ext4, $expansions4)==false)
    { $chk=false;
       $errors4="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize4 > 2097152)
    {$chk=false;
    $errors4='File size must be less than 2 MB';
    } 
    if($list4[0] == 600)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors4='File width must be 600 pixels';
    }
     if($list4[1] == 400)
    {$chk=true;
    
    } 
    else{
    $chk=false;
     $errors4='File height must be 400 pixels';
    }
}
if($imgname5!=='')
{
  if(in_array($file_ext5, $expansions5)==false)
    { $chk=false;
       $errors5="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize5 > 2097152)
    {$chk=false;
    $errors5='File size must be less than 2 MB';
    }
    if($list5[0] == 320)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors5='File width must be 320 pixels';
    }

     if($list5[1] == 320)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors5='File height must be 320 pixels';
    }
}
if($imgname6!=='')
{
  if(in_array($file_ext6, $expansions6)==false)
    { $chk=false;
       $errors6="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize6 > 2097152)
    {$chk=false;
    $errors6='File size must be less than 2 MB';
    } 
    if($list6[0] == 1024)
    {$chk=true;
    }
    else{
    $chk=false;
    $errors6='File width must be 1024 pixels';
    }
     if($list6[1] == 768)
    {$chk=true;
    
    }
    else{
    $chk=false;
    $errors6='File height must be 768 pixels';
    }
}
if($imgname9!=='')
{
  if(in_array($file_ext9, $expansions9)==false)
    { $chk=false;
       $errors9="extension not allowed, please choose a mp4  file.";
    }
    if($imgsize9 > 20971520)
    {$chk=false;
    $errors9='File size must be less than 20 MB';
    }
}
/*if($imgname1!=='' and $imgname2!=='' and $imgname3!=='' and $imgname4!==''  and $imgname6!==''  and $imgname9!=='' and $imgname5!=='')
{

   if(in_array($file_ext1, $expansions1)==false)
     { $chk=false;
       $errors1="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize1 > 2097152)
    {$chk=false;
    $errors1='File size must be less than 2 MB';
    } 
    if($list1[0] = 1024)
    {$chk=false;
    $errors1='File width must be 1024 pixels';
    }
     if($list1[1] = 768)
    {$chk=false;
    $errors1='File height must be 768 pixels';
    }


if(in_array($file_ext2, $expansions2)==false)
    { $chk=false;
       $errors2="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize2 > 2097152)
    {$chk=false;
    $errors2='File size must be less than 2 MB';
    } 
    if($list2[0] = 1024)
    {$chk=false;
    $errors2='File width must be 1024 pixels';
    }
     if($list2[1] = 768)
    {$chk=false;
    $errors2='File height must be 768 pixels';
    }

       


if(in_array($file_ext3, $expansions3)==false)
    { $chk=false;
       $errors3="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize3 > 2097152)
    {$chk=false;
    $errors3='File size must be less than 2 MB';
    } 
    if($list3[0] = 1024)
    {$chk=false;
    $errors3='File width must be 1024 pixels';
    }
     if($list3[1] = 768)
    {$chk=false;
    $errors3='File height must be 768 pixels';
    }



if(in_array($file_ext4, $expansions4)==false)
    { $chk=false;
       $errors4="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize4 > 2097152)
    {$chk=false;
    $errors4='File size must be less than 2 MB';
    } 
    if($list4[0] = 600)
    {$chk=false;
    $errors4='File width must be 600 pixels';
    }
     if($list4[1] = 400)
    {$chk=false;
    $errors4='File height must be 400 pixels';
    }

    if(in_array($file_ext5, $expansions5)==false)
    { $chk=false;
       $errors5="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize5 > 2097152)
    {$chk=false;
    $errors5='File size must be less than 2 MB';
    } 
    if($list5[0] = 320)
    {$chk=false;
    $errors5='File width must be 320 pixels';
    }
     if($list5[1] = 320)
    {$chk=false;
    $errors5='File height must be 320 pixels';
    }


    

    if(in_array($file_ext6, $expansions6)==false)
    { $chk=false;
       $errors6="extension not allowed, please choose a png, jpg or jpeg  file.";
    }
    if($imgsize6 > 2097152)
    {$chk=false;
    $errors6='File size must be less than 2 MB';
    }
    if($list6[0] = 1024)
    {$chk=false;
    $errors6='File width must be 1024 pixels';
    }
     if($list6[1] = 768)
    {$chk=false;
    $errors6='File height must be 768 pixels';
    }

     

     if(in_array($file_ext9, $expansions9)==false)
    { $chk=false;
       $errors9="extension not allowed, please choose a mp4  file.";
    }
    if($imgsize9 > 20971520)
    {$chk=false;
    $errors9='File size must be less than 20 MB';
    }


}*/


}




if($chk==true){




        if($imgname1!=='')
        {
             $newimage1=$filepath.$i.'.1.'.$file_ext1;

        }
        else
        {

            $newimage1=$_POST['image11'];
        }

        if($imgname2!=='')
        {
             $newimage2=$filepath.$i.'.2.'.$file_ext2;
        }
        else
        {

            $newimage2=$_POST['image22'];
        }

        if($imgname3!=='')
        {
               $newimage3=$filepath.$i.'.3.'.$file_ext3;

        }
        else
        {

            $newimage3=$_POST['image33'];
        }

        if($imgname4!=='')
        {
             $newimage4=$filepath.$i.'.4.'.$file_ext4;

        }
        else
        {

            $newimage4=$_POST['image44'];
        }

          if($imgname5!=='')
        {
             $newimage5=$filepath.$i.'.5.'.$file_ext5;

        }
        else
        {

            $newimage5=$_POST['image55'];
        }


        if($imgname6!=='')
        {
             $newimage6=$filepath.$i.'.6.'.$file_ext6;

        }
        else
        {

            $newimage6=$_POST['image66'];
        }

        

        if($imgname9!=='')
        {
              $newimage9=$filepath1.$i.'1.'.$file_ext9;

        }
        else
        {

            $newimage9=$_POST['image99'];
        }






                  /*$newimage1=$filepath.$i.'.1.'.$file_ext1;
                  $newimage2=$filepath.$i.'.2.'.$file_ext2;
                  $newimage3=$filepath.$i.'.3.'.$file_ext3;
                  $newimage4=$filepath.$i.'.4.'.$file_ext4;
                  $newimage5=$filepath.$i.'.5.'.$file_ext5;
                  $newimage6=$filepath.$i.'.6.'.$file_ext6;*/
	

    //$happi=$_POST['happ'];
  @$update1=mysqli_query($conn,"UPDATE items set cate_id='$idd',item_title='$title',item_location='$location', item_offer='$offer',item_rating='$rating',item_img1='$newimage1',item_img2='$newimage2',item_img3='$newimage3',item_img4='$newimage6',cover_img='$newimage4',happy_hours='$a[0]*$a[1]$a[2]-$a[3]$a[4]#$a[5]@$b[0]*$b[1]$b[2]-$b[3]$b[4]#$b[5]@$c[0]*$c[1]$c[2]-$c[3]$c[4]#$c[5]@$d[0]*$d[1]$d[2]-$d[3]$d[4]#$d[5]@$e[0]*$e[1]$e[2]-$e[3]$e[4]#$e[5]@$f[0]*$f[1]$f[2]-$f[3]$f[4]#$f[5]@$g[0]*$g[1]$g[2]-$g[3]$g[4]#$g[5]' ,item_lat='$coord[0]',item_lng='$coord[1]',item_feat='$featured',item_video='$newimage9',item_prior='$prior',cuisines='$cuisines',highlights='$highlights',cost='$cost',known_for='$known',cuisines='$cuisines',highlights='$highlights',cost='$cost',busy_nights='$busynights',item_address='$address',item_published='$status',feat_image='$newimage5',metro_station='$metro',home_prior='$hprior' where item_id='$i'");

    
    if(!$update1)
    {

     /* echo $happi;
      echo $idd;*/
     die("Error".mysqli_error());
     

    }

    else
    {

  
                 move_uploaded_file($_FILES['image1']['tmp_name'],$newimage1);

                 move_uploaded_file($_FILES['image2']['tmp_name'],$newimage2);

                 move_uploaded_file($_FILES['image3']['tmp_name'],$newimage3);

                 move_uploaded_file($_FILES['image4']['tmp_name'],$newimage4);

                  move_uploaded_file($_FILES['image5']['tmp_name'],$newimage5);

                  move_uploaded_file($_FILES['image6']['tmp_name'],$newimage6);



                  move_uploaded_file($_FILES['image9']['tmp_name'],$newimage9);

}
}


 $menuimg = array();

  for($im=0;$im<count($_FILES['menuimage']['name']);$im++)
{

  if(isset($_FILES['menuimage'])) 
  {

//echo $im;
  @$filepath2="images/items/menu/";
  //@$format=".png";

   @$imgname15=$_FILES['menuimage']['name'][$im];
    @$imgsize15=$_FILES['menuimage']['size'][$im];
    @$file_ext15=strtolower(end(explode('.',$_FILES['menuimage']['name'][$im])));
    @$expansions15=array("png","jpg","jpeg");
    $list15 = getimagesize($_FILES['menuimage']['tmp_name']);  

   //echo $imgname15;

    @$imgname15= preg_replace('/( *)/', '', $imgname15);
    
    @$imgid='';
    @$message='';

     @$newimage15='';
    
    @$update='';


@$chk=true;
if($imgname15!=='')
{

   if(!in_array($file_ext15, $expansions15))
     { 
      $chk=false;
       $errors15="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize15 > 2097152)
    {
      $chk=false;
    $errors15='File size must be less than 2 MB';
    } 
    if($list15[0] > 600)
    {$chk=false;
    $errors15='File width must be less than 600 pixels';
    }
     if($list15[1] > 600)
    {$chk=false;
    $errors15='File height must be less than 600 pixels';
    }  


}


}
      
      if($imgname15!=='')
        {
               $newimage15=$filepath2.$i.".".$im.".".$file_ext15;

        }
        else
        {

            $newimage15=$_POST['menuimage1'][$im];
        }

        if($chk==true){
      

        array_push($menuimg,$newimage15);

       move_uploaded_file($_FILES['menuimage']['tmp_name'][$im],$newimage15); 
     }

}


$menuimg1=implode(",",$menuimg);

$updatemenu=mysqli_query($conn,"UPDATE  items set menu_image='$menuimg1' where item_id='$i'");   
 


/*$fullimg = array();

  for($j=0;$j<count($_FILES['fullimage']['name']);$j++)
{

  if(isset($_FILES['fullimage'])) 
  {


  @$filepath2="images/items/full/";
 

   @$imgname16=$_FILES['fullimage']['name'][$j];
    @$imgsize16=$_FILES['fullimage']['size'][$j];
    @$file_ext16=strtolower(end(explode('.',$_FILES['fullimage']['name'][$j])));
    @$expansions16=array("png","jpg","jpeg");  



    @$imgname16= preg_replace('/( *)/', '', $imgname16);
    
    @$imgid='';
    @$message='';

     @$newimage16='';
    
    @$update='';


@$chk=true;
if($imgname16!=='')
{

   if(in_array($file_ext16, $expansions16==false))
     { 
      $chk=false;
       $errors="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize16 > 2097152)
    {
      $chk=false;
    $errors='File size must be less than 2 MB';
    } 


}


}

        if($imgname16!=='')
        {
               $newimage16=$filepath2.$i.".".$j.".".$file_ext16;

        }
        else
        {

            $newimage16=$_POST['fullimage1'][$j];
        }


     

       

        array_push($fullimg,$newimage16);

       move_uploaded_file($_FILES['fullimage']['tmp_name'][$j],$newimage16); 

}


$fullimg1=implode(",",$fullimg);

$updatefull=mysqli_query($conn,"UPDATE  items set full_image='$fullimg1' where item_id='$i'");   
 

*/

$venueimg = array();

  for($k=0;$k<count($_FILES['venueimage']['name']);$k++)
{

  if(isset($_FILES['venueimage'])) 
  {

//echo $im;
  @$filepath2="images/items/venue/";
  //@$format=".png";

   @$imgname17=$_FILES['venueimage']['name'][$k];
    @$imgsize17=$_FILES['venueimage']['size'][$k];
    @$file_ext17=strtolower(end(explode('.',$_FILES['venueimage']['name'][$k])));
    @$expansions17=array("png","jpg","jpeg");  
    $list17 = getimagesize($_FILES['venueimage']['tmp_name']); 

   //echo $imgname15;

    @$imgname17= preg_replace('/( *)/', '', $imgname17);
    
    @$imgid='';
    @$message='';

     @$newimage17='';
    
    @$update='';


@$chk=true;
if($imgname17!=='')
{

   if(!in_array($file_ext17, $expansions17))
     { 
      $chk=false;
       $errors17="extension not allowed, please choose a png, jpg or jpeg file.";
    }
    if($imgsize17 > 2097152)
    {
      $chk=false;
    $errors17='File size must be less than 2 MB';
    }
    if($list17[0] > 600)
    {$chk=false;
    $errors17='File width must be less than 600 pixels';
    }
     if($list17[1] > 600)
    {$chk=false;
    $errors17='File height must be less than 600 pixels';
    }   


}


}

           if($imgname17!=='')
        {
               $newimage17=$filepath2.$i.".".$k.".".$file_ext17;

        }
        else
        {

            $newimage17=$_POST['venueimage1'][$k];
        }


       // $newimage17=$filepath2.$imgid1.".".$k.".".$file_ext17;
//echo "</br>".$newimage15;
       
        if($chk==true){

        array_push($venueimg,$newimage17);

       move_uploaded_file($_FILES['venueimage']['tmp_name'][$k],$newimage17); 
     }

}


$venueimg=implode(",",$venueimg);

$updatevenue=mysqli_query($conn,"UPDATE  items set venue_image='$venueimg' where item_id='$i'");  


//////////////////////////////////////////////////////////////////////


                      $barimg = array();

                      for($bp=0;$bp<count($_FILES['barimage']['name']);$bp++)
                    {

                      if(isset($_FILES['barimage'])) 
                      {

                
                      @$filepath3="images/items/bar/";
                  

                       @$imgname18=$_FILES['barimage']['name'][$bp];
                        @$imgsize18=$_FILES['barimage']['size'][$bp];
                        @$file_ext18=strtolower(end(explode('.',$_FILES['barimage']['name'][$bp])));
                        @$expansions18=array("png","jpg","jpeg");  
                        $list18 = getimagesize($_FILES['barimage']['tmp_name']); 

                      

                        @$imgname18= preg_replace('/( *)/', '', $imgname18);
                        
                        @$imgid='';
                        @$message='';

                         @$newimage18='';
                        
                        @$update='';


                        @$chk1=true;
                    if($imgname18!=='')
                    {

                       if(!in_array($file_ext18, $expansions18))
                         { 
                          $chk1=false;
                           $errors18="extension not allowed, please choose a png, jpg or jpeg file.";
                        }
                        if($imgsize18 > 2097152)
                        {
                          $chk1=false;
                        $errors18='File size must be less than 2 MB';
                        }
                        if($list18[0] > 600)
                        {$chk1=false;
                        $errors18='File width must be less than 600 pixels';
                        }
                         if($list18[1] > 600)
                        {$chk1=false;
                        $errors18='File height must be less than 600 pixels';
                        }  


                    }


                    }
                             if($imgname18!=='')
                                    {
                            $newimage18=$filepath3.$i.".".$bp.".".$file_ext18;
                                   }
                                 else
                                {
                                     $newimage18=$_POST['barimage1'][$bp];
                                }
                    
                           if($chk1==true){

                            array_push($barimg,$newimage18);

                           move_uploaded_file($_FILES['barimage']['tmp_name'][$bp],$newimage18); 
                         }
                          

                    }


                    $barimg=implode(",",$barimg);

                    $updatebar=mysqli_query($conn,"UPDATE  items set bar_image='$barimg' where item_id='$i'");   
                    


          
 
//header('location:items.php');

//echo "Data saved";







                  /*$message="Save record";
                  header('location:items.php');*/




  }



}

/*}
}*/


// crop code starts here 

$path = "images/";

$valid_formats = array("jpg", "png", "gif", "bmp");
	if(isset($_POST['crop']))
		{
			$name = $_FILES['photoimg']['name'];
			$size = $_FILES['photoimg']['size'];
			
			if(strlen($name))
				{
					list($txt, $ext) = explode(".", $name);
					if(in_array($ext,$valid_formats) && $size<(1024*1024))
						{
							$actual_image_name = time().substr($txt, 5).".".$ext;
							$tmp = $_FILES['photoimg']['tmp_name'];
							if(move_uploaded_file($tmp, $path.$actual_image_name))
								{  //chmod($path.$actual_image_name,0777);
								//mysql_query("UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
									$image="<h1>Please drag on the image. Width and height should not be less than 400 , 270 resp. </h1><img src='images/".$actual_image_name."' id=\"photo1\" style='max-width:500px' >";
									
								}
							else
								echo "failed";
						}
					else
						echo "Invalid file formats..!";					
				}
			else
				echo "Please select image..!";
		}
// crop code ends here  








		?>
	

	


	
	<body>




<!-- crop script starts here -->




<script type="text/javascript">
//alert(hello);
var id=window.location.href.split('id=');
if(id[1]){
id=id[1];
}
else{
id=0;
}
function getSizes(im,obj)
	{
		var x_axis = obj.x1;
		var x2_axis = obj.x2;
		var y_axis = obj.y1;
		var y2_axis = obj.y2;
		var thumb_width = obj.width;
		var thumb_height = obj.height;
       document.getElementById('wid').value=thumb_width;
       document.getElementById('hei').value=thumb_height;
       
        var image_type=document.getElementById('image_type').value;

 alert(image_type);
      
//alert("id");

//alert(x_axis);alert(y_axis);alert(thumb_width);alert(thumb_height);
		if(thumb_width > 0)
			{
				if(confirm("Do you want to save image..!"))
					{   
//
				    		$.ajax({
							type:"GET",
							url:"item_ajax.php?t=ajax&img="+$("#image_name").val()+"&w="+thumb_width+"&h="+thumb_height+"&x1="+x_axis+"&y1="+y_axis+"&id="+id+"&img_typ="+image_type,
							cache:false,
							success:function(rsponse)
								{
								 $("#cropimage").hide();
								    $("#thumbs").html("");
									$("#thumbs").html("<img src='images/"+rsponse+"' />");
                                                                    
								}
						});
					}
			}
		else
			alert("Please select portion..!");
	}

$(document).ready(function () {

    $('img#photo1').imgAreaSelect({
        aspectRatio: '60:40',
        onSelectEnd: getSizes
         
    });

});


</script>
<!-- crop script ends here -->
















		<div id="app">
			<!-- sidebar -->
			<div class="sidebar app-aside" id="sidebar">
				<div class="sidebar-container perfect-scrollbar">
					<nav>
						<!-- start: SEARCH FORM -->
						<!--<div class="search-form">
							<a class="s-open" href="#">
								<i class="ti-search"></i>
							</a>
							<form class="navbar-form" role="search">
								<a class="s-remove" href="#" target=".navbar-form">
									<i class="ti-close"></i>
								</a>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search...">
									<button class="btn search-button" type="submit">
										<i class="ti-search"></i>
									</button>
								</div>
							</form>
						</div>-->
						<!-- end: SEARCH FORM -->
						<!-- start: MAIN NAVIGATION MENU -->
						<div class="navbar-title">
							<span>Main Navigation</span>
						</div>
						<ul class="main-navigation-menu">
							
						<?php include("nav_index.php"); ?>
	
						<!--	<li >
								<a href="index.php">
									<div class="item-content">
										<div class="item-media">
											<i class="ti-home"></i>
										</div>
										<div class="item-inner">
											<span class="title"> Dashboard </span>
										</div>
									</div>
								</a>
							</li>

							<li>
							<a href="category.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Categories </span>
										</div>

								</div>
							





							</a>
							</li>


							<li class="active">
							<a href="items.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Venues </span>
										</div>

								</div>
							





							</a>
							</li>

<li >
							<a href="djview1.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Artists </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="eventcategory.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Event Category </span>
										</div>

								</div>
							





							</a>
							</li>


<li>
							<a href="eventview.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Events </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="coupon.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Coupons </span>
										</div>

								</div>
							





							</a>
							</li>

							<li>
							<a href="reviewlist.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Reviews </span>
										</div>

								</div>
							





							</a>
							</li>

							<li>
							<a href="offerlist.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Offers </span>
										</div>

								</div>
							





							</a>
							</li>

							<li>
							<a href="users.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Users </span>
										</div>

								</div>
							





							</a>
							</li>

							<li>
							<a href="booking.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Bookings </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="eventticket.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Ticket  </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="typeview.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Ticket Type </span>
										</div>

								</div>
							





							</a>
							</li>

<li >
							<a href="location.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Location </span>
										</div>

								</div>
							





							</a>
							</li>

							<li >
							<a href="tag.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Tag </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="uploadedphoto.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Uploaded Photo </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="banner.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Banner </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="city.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">City </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="dresscode.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Dresscode </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="music.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title">Music </span>
										</div>

								</div>
							





							</a>
							</li> -->


							<!--<li>
							<a href="nightview.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Night Feed </span>
										</div>

								</div>
							





							</a>
							</li>

<li>
							<a href="collection.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Collection </span>
										</div>

								</div>
							





							</a>
							</li>
<li>
							<a href="fb.php">
								<div class="item-content">
									
									<div class="item-media">
							         <i class="ti-user"></i>
							        </div> 
                                    <div class="item-inner">
											<span class="title"> Facebook </span>
										</div>

								</div>
							





							</a>
							</li>-->





							
						</ul>
						
					</nav>
				</div>
			</div>
			<!-- / sidebar -->
			<div class="app-content">
				<!-- start: TOP NAVBAR -->
				<header class="navbar navbar-default navbar-static-top">
					<!-- start: NAVBAR HEADER -->
					<div class="navbar-header">
						<a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
							<i class="ti-align-justify"></i>
						</a>
						<a class="navbar-brand" href="#">
							
						
<h1>ClubGo
</h1>
</a>
						<a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
							<i class="ti-align-justify"></i>
						</a>
						<a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<i class="ti-view-grid"></i>
						</a>
					</div>
					<!-- end: NAVBAR HEADER -->
					<!-- start: NAVBAR COLLAPSE -->
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-right">
							<!-- start: MESSAGES DROPDOWN -->
						<!--	<li class="dropdown">
								<a href class="dropdown-toggle" data-toggle="dropdown">
									<span class="dot-badge partition-red"></span> <i class="ti-comment"></i> <span>MESSAGES</span>
								</a>
								<ul class="dropdown-menu dropdown-light dropdown-messages dropdown-large">
									<li>
										<span class="dropdown-header"> Unread messages</span>
									</li>
									<li>
										<div class="drop-down-wrapper ps-container">
											<ul>
												<li class="unread">
													<a href="javascript:;" class="unread">
														<div class="clearfix">
															<div class="thread-image">
																<img src="./assets/images/avatar-2.jpg" alt="">
															</div>
															<div class="thread-content">
																<span class="author">Nicole Bell</span>
																<span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
																<span class="time"> Just Now</span>
															</div>
														</div>
													</a>
												</li>
												<li>
													<a href="javascript:;" class="unread">
														<div class="clearfix">
															<div class="thread-image">
																<img src="./assets/images/avatar-3.jpg" alt="">
															</div>
															<div class="thread-content">
																<span class="author">Steven Thompson</span>
																<span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
																<span class="time">8 hrs</span>
															</div>
														</div>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<div class="clearfix">
															<div class="thread-image">
																<img src="./assets/images/avatar-5.jpg" alt="">
															</div>
															<div class="thread-content">
																<span class="author">Kenneth Ross</span>
																<span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
																<span class="time">14 hrs</span>
															</div>
														</div>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="view-all">
										<a href="#">
											See All
										</a>
									</li>
								</ul>
							</li> -->
							<!-- end: MESSAGES DROPDOWN -->
							<!-- start: ACTIVITIES DROPDOWN -->
						<!--	<li class="dropdown">
								<a href class="dropdown-toggle" data-toggle="dropdown">
									<i class="ti-check-box"></i> <span>ACTIVITIES</span>
								</a>
								<ul class="dropdown-menu dropdown-light dropdown-messages dropdown-large">
									<li>
										<span class="dropdown-header"> You have new notifications</span>
									</li>
									<li>
										<div class="drop-down-wrapper ps-container">
											<div class="list-group no-margin">
												<a class="media list-group-item" href="">
													<img class="img-circle" alt="..." src="assets/images/avatar-1.jpg">
													<span class="media-body block no-margin"> Use awesome animate.css <small class="block text-grey">10 minutes ago</small> </span>
												</a>
												<a class="media list-group-item" href="">
													<span class="media-body block no-margin"> 1.0 initial released <small class="block text-grey">1 hour ago</small> </span>
												</a>
											</div>
										</div>
									</li>
									<li class="view-all">
										<a href="#">
											See All
										</a>
									</li>
								</ul>
							</li> -->
							<!-- end: ACTIVITIES DROPDOWN -->
							<!-- start: LANGUAGE SWITCHER -->
						<!--	<li class="dropdown">
								<a href class="dropdown-toggle" data-toggle="dropdown">
									<i class="ti-world"></i> English
								</a>
								<ul role="menu" class="dropdown-menu dropdown-light fadeInUpShort">
									<li>
										<a href="#" class="menu-toggler">
											Deutsch
										</a>
									</li>
									<li>
										<a href="#" class="menu-toggler">
											English
										</a>
									</li>
									<li>
										<a href="#" class="menu-toggler">
											Italiano
										</a>
									</li>
								</ul>
							</li> -->
							<!-- start: LANGUAGE SWITCHER -->
							<!-- start: USER OPTIONS DROPDOWN -->
							<li class="dropdown current-user">
								<a href class="dropdown-toggle" data-toggle="dropdown">
									<img src="assets/images/unnamed.jpg" alt="Peter"> <span class="username"><?php echo $snam; ?><i class="ti-angle-down"></i></i></span>
								</a>
								<ul class="dropdown-menu dropdown-dark">
								<!--	<li>
										<a href="pages_user_profile.html">
											My Profile
										</a>
									</li>
									<li>
										<a href="pages_calendar.html">
											My Calendar
										</a>
									</li>
									<li>
										<a hef="pages_messages.html">
											My Messages (3)
										</a>
									</li>
									<li>
										<a href="login_lockscreen.html">
											Lock Screen
										</a>
									</li> -->
									<li>
										<a href="logout.php">
											Log Out
										</a>
									</li>
								</ul>
							</li>
							<!-- end: USER OPTIONS DROPDOWN -->
						</ul>
						<!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
						<div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
							<div class="arrow-left"></div>
							<div class="arrow-right"></div>
						</div>
						<!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
					</div>
				<!--	<a class="dropdown-off-sidebar" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
						&nbsp;
					</a> -->
					<!-- end: NAVBAR COLLAPSE -->
				</header>
				<!-- end: TOP NAVBAR -->
				<div class="main-content" >
					<div class="wrap-content container" id="container">
						<!-- start: DASHBOARD TITLE -->
						<section id="page-title" class="padding-top-15 padding-bottom-15">
							<div class="row">
								<div class="col-sm-7">
									<h1 class="mainTitle">Venues</h1>
									<span class="mainDescription">overview &amp; stats </span>
								</div>
								<div class="col-sm-5">
									<!-- start: MINI STATS WITH SPARKLINE -->
									<ul class="mini-stats pull-right">
									<!--	<li>
											<div class="sparkline-1">
												<span ></span>
											</div>
											<div class="values">
												<strong class="text-dark">18304</strong>
												<p class="text-small no-margin">
													Sales
												</p>
											</div>
										</li>
										<li>
											<div class="sparkline-2">
												<span ></span>
											</div>
											<div class="values">
												<strong class="text-dark">&#36;3,833</strong>
												<p class="text-small no-margin">
													Earnings
												</p>
											</div>
										</li>
										<li>
											<div class="sparkline-3">
												<span ></span>
											</div>
											<div class="values">
												<strong class="text-dark">&#36;848</strong>
												<p class="text-small no-margin">
													Referrals
												</p>
											</div>
										</li> -->
									</ul>
									<!-- end: MINI STATS WITH SPARKLINE -->
								</div>
							</div>
						</section>
						<!-- end: DASHBOARD TITLE -->
						<!-- start: FEATURED BOX LINKS -->
						<div class="container-fluid container-fullw bg-white">
							<div class="row">

                             <div class="panel-body">
                             <div class="col-lg-6 col-md-12">
											<div class="panel panel-white">
												<div class="panel-heading">
													<h5 class="panel-title">Venue form</h5>
												</div>
												<div class="panel-body">
													<!-- <p class="text-small margin-bottom-20">
														Use Bootstrap's predefined grid classes to align labels and groups of form controls in a horizontal layout by adding <code>.form-horizontal</code> to the form. Doing so changes <code>.form-group</code>s to behave as grid rows, so no need for <code>.row</code>.
													</p> -->
													<form role="form" action="<?php $_PHP_SELF ?>" class="form-horizontal" method="post" enctype="multipart/form-data">

														
                                                       <?php
                                                         if(isset($action) and $action=='add'){


                                                       ?>

   <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Introduction</h5>
       </div> 

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputEmail3">
                                Priority of Venue for Venue page
                              </label>
                              <div class="col-sm-10">
                              <input type="text"  id="ep" name="prior" maxlength="3" class="form-control" >
                                
                              </div>
                            </div> 
                            
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputEmail3">
                                Priority of Venue for Home page
                              </label>
                              <div class="col-sm-10">
                              <input type="text"  id="hep" name="hprior" maxlength="3" class="form-control" >
                                
                              </div>
                            </div>

                                        <div class="form-group">
                      <label class="col-sm-2 control-label" for="inputEmail3">
                                Published
                              </label>
                              <div class="col-sm-10">
                                 <select class="select3" name="status" id="status" >
                                <option value="">-Select-</option>
                                <?php
                                                              $arr=array("Published","Unpublished");
                                                              for($i=0;$i<count($arr);$i++){
                                                                echo "<option>".$arr[$i]."</option>";
                                                              }
                                ?>

                              
                                                               

                                 </select>
                              </div>
                            </div>    

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                                Add Featured
                              </label>
                              <div class="col-sm-10">
                               <input type="checkbox" name="featured" value="1" id="t20">
                              </div>
                            </div>                       

														<div class="form-group">
											<label class="col-sm-2 control-label" for="inputEmail3">
															Venue Title
															</label>
															<div class="col-sm-10">
																<input type="text" name="ititle"  id="t1" class="form-control" >
															</div>
														</div>
														<div class="form-group">
											<label class="col-sm-2 control-label" for="inputEmail3">
														Venue	Category
															</label>
															<div class="col-sm-10">
																<select  name="mobid" id="t2">
															<option>select category</option>
															<?php
															
															 $add=mysqli_query($conn,"select * from category");
                                                           
									                           while($fet=mysqli_fetch_array($add)){
									                           	if( $fet['cate_id']==$fetch['cate_id']){

																 echo "<option selected value=".$fet['cate_id'].">". $fet['cate_title']."</option>";
									                           	}
									                           	else{
									                           		echo "<option value=".$fet['cate_id'].">". $fet['cate_title']."</option>";
									                           	
									                           	}
																}
																?>
																</option>
															</select>
																</div>
														</div>

                            <!-- <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Description Title
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t3" name="desctitle" class="form-control" required>
                              </div>
                            </div>


														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Description
															</label>
															<div class="col-sm-10">
														
																<textarea cols="25" rows="10" id="t4" name="idesc" class="form-control" ></textarea>
															</div>
														</div> -->

                              

														<!-- <div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Phone No.
															</label>
															<div class="col-sm-10">
																<input type="text"  id="uph" name="iphone" class="form-control" maxlength="10" >
															</div>
														</div> -->

														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Location
															</label>
															<div class="col-sm-10">
																<!-- <input type="text"  id="t6" name="iloc" class="form-control" > -->
                                <select name="iloc">
                                  <option value="">--Select--</option>
                                  <?php

                                  $loc=mysqli_query($conn,"SELECT * FROM location");
                                  while($res=mysqli_fetch_assoc($loc))
                                  {

                                    echo "<option value='".$res['loc_id']."'>".$res['loc_title']."</option>";
                                  }



                                  ?>
                                </select>

															</div>
														</div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Address
                              </label>
                              <div class="col-sm-10">
                                <textarea  id="t19" name="address" class="form-control" ></textarea>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Item Coordinates
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t19" name="coordi" placeholder="eg: 23.432,77.6554 (seprated by commas)" class="form-control" >
                              </div>
                            </div>


   <!--   <div class="form-group">                       <label class="col-sm-2 control-label" for="inputPassword3">                       Address                       </label>                       <div class="col-sm-10">                         <textarea  id="t19" name="address" class="form-control" ></textarea>                       </div>                     </div> 
                     <div class="form-group">                       <label class="col-sm-2 control-label" for="inputPassword3">                       Item Coordinates                       </label>                       <div class="col-sm-10">                         <input type="text"  id="t19" name="coordi" placeholder="eg: 23.432,77.6554 (seprated by commas)" class="form-control" >                       </div>                     </div> -->
                     <div class="form-group">                       <label class="col-sm-2 control-label" for="inputPassword3">                       Nearest Metro Station                       </label>                       <div class="col-sm-10">                         <select name="metro">                         <option value="">--Select--.</option>                         <?php                               /*$metro=array("Dilshad Garden","Jhil Mil","Mansarovar Park","Shahdara","Welcome","Seelampur","Shastri Park","Kashmere Gate","Tis Hazari","Pul Bangash","Pratap Nagar");*/                         /*$metro= array (
  0 => 'Dilshad Garden',
  1 => ' Jhilmil',
  2 => ' Mansarovar Park',
  3 => ' Shahdara',
  4 => ' Welcome',
  5 => ' Seelampur',
  6 => ' Shastri Park',
  7 => ' Kashmere Gate',
  8 => ' Tis Hazari',
  9 => ' Pul Bangash',
  10 => ' Pratap Nagar',
  11 => ' Shastri Nagar',
  12 => ' Inder Lok',
  13 => ' Kanhiya Nagar',
  14 => ' Keshav Puram',
  15 => ' Netaji Subhash Place',
  16 => ' Kohat Enclave',
  17 => ' Pitam Pura',
  18 => ' Rohini East',
  19 => ' Rohini West',
  20 => ' Rithala',
  21 => 'Jahangirpuri',
  22 => ' Adarsh Nagar',
  23 => ' Azad Pur',
  24 => ' Model Town',
  25 => ' GTB Nagar',
  26 => ' Vishwa Vidyalaya',
  27 => ' Vidhan Sabha',
  28 => ' Civil Lines',
  29 => ' Kashmere Gate',
  30 => ' Delhi Main',
  31 => ' Chawri Bazar',
  32 => ' New Delhi',
  33 => ' Rajiv Chowk',
  34 => ' Patel Chowk',
  35 => ' Central Sectt',
  36 => ' Noida City Centre',
  37 => ' Golf course',
  38 => ' Botanical Garden',
  39 => ' Noida Sector 18',
  40 => ' Noida Sector 16',
  41 => ' Noida Sector 15',
  42 => ' New Ashok Nagar',
  43 => ' Mayur Vihar Ext. Mayur Vihar –I',
  44 => ' Akshardham',
  45 => ' Yamuna Bank',
  46 => ' Indraprastha',
  47 => ' Pragati Maidan',
  48 => ' Mandi House',
  49 => ' Barakhamba Road',
  50 => ' Rajiv Chowk',
  51 => ' R.K. Ashram Marg',
  52 => ' Jhandewalan',
  53 => ' Karol Bagh',
  54 => ' Rajendra Place',
  55 => ' Patel Nagar',
  56 => ' Shadipur',
  57 => ' Kirti Nagar',
  58 => ' Moti Nagar',
  59 => ' Ramesh Nagar',
  60 => ' Rajouri Garden',
  61 => ' Tagore Garden',
  62 => ' Subhash Nagar',
  63 => ' Tilak Nagar',
  64 => ' Janakpuri East',
  65 => ' Janakpuri West',
  66 => ' Uttam Nagar East',
  67 => ' Uttam Nagar West',
  68 => ' Nawada',
  69 => ' Dwarka Morh',
  70 => ' Dwarka',
  71 => ' Dwarka Sec-14',
  72 => ' Dwarka Sec-13',
  73 => ' Dwarka Sec-12',
  74 => ' Dwarka Sec-11',
  75 => ' Dwarka Sec-10',
  76 => ' Dwarka Sec-9',
  77 => ' Laxmi Nagar',
  78 => ' Nirman Vihar',
  79 => ' Preet Vihar',
  80 => ' Karkarduma',
  81 => ' Anand Vihar',
);*/

$metro=array (
  0 => 'Dilshad Garden',
  1 => 'Jhil Mil',
  2 => 'Welcome',
  3 => 'Seelampur',
  4 => 'Shastri Park',
  5 => 'Kashmere Gate',
  6 => 'Tis Hazari',
  7 => 'Pul Bangash',
  8 => 'Pratap Nagar',
  9 => 'Shastri Nagar',
  10 => 'Inder Lok',
  11 => 'Kanhaiya Nagar',
  12 => 'Keshav Puram',
  13 => 'Netaji Subhash Place',
  14 => 'Kohat Enclave',
  15 => 'Pitam Pura',
  16 => 'Rohini East',
  17 => 'Rohini West',
  18 => 'Rithala',
  19 => 'Jahangirpuri',
  20 => 'Adarsh Nagar',
  21 => 'Azadpur',
  22 => 'Model Town',
  23 => 'G.T.B. Nagar',
  24 => 'Vishwavidyalaya',
  25 => 'Rajiv Chowk',
  26 => 'Vidhan Sabha',
  27 => 'New Delhi',
  28 => 'Civil Lines',
  29 => 'Kashmere Gate',
  30 => 'Chandni Chowk',
  31 => 'Chawri Bazar',
  32 => 'Patel Chowk',
  33 => 'Central Secretariat',
  34 => 'Udyog Bhawan',
  35 => 'Race Course',
  36 => 'Jorbagh',
  37 => 'INA',
  38 => 'AIIMS',
  39 => 'Green Park',
  40 => 'Hauz Khas',
  41 => 'Malviya Nagar',
  42 => 'Saket',
  43 => 'Qutab Minar',
  44 => 'Chhattarpur',
  45 => 'Sultanpur',
  46 => 'Ghitorni',
  47 => 'Arjan Garh',
  48 => 'Guru Dronacharya',
  49 => 'Sikandarpur',
  50 => 'MG Road',
  51 => 'IFFCO Chowk',
  52 => 'Huda City Centre',
  53 => 'Dwarka Sec-21',
  54 => 'Dwarka Sec-08',
  55 => 'Dwarka Sec-09',
  56 => 'Dwarka Sec-10',
  57 => 'Dwarka Sec-11',
  58 => 'Dwarka Sec-12',
  59 => 'Dwarka Sec-13',
  60 => 'Dwarka Sec-14',
  61 => 'Dwarka',
  62 => 'Dwarka Mor',
  63 => 'Nawada',
  64 => 'Uttam Nagar West',
  65 => 'Uttam Nagar East',
  66 => 'Janak Puri West',
  67 => 'Janak Puri East',
  68 => 'Tilak Nagar',
  69 => 'Subhash Nagar',
  70 => 'Tagore Garden',
  71 => 'Rajouri Garden',
  72 => 'Ramesh Nagar',
  73 => 'Moti Nagar',
  74 => 'Kirti Nagar',
  75 => 'Shadipur',
  76 => 'Patel Nagar',
  77 => 'Rajendra Place',
  78 => 'Karol Bagh',
  79 => 'Jhandewalan',
  80 => 'R K Ashram Marg',
  81 => 'Rajiv Chowk',
  82 => 'Barakhamba',
  83 => 'Mandi House',
  84 => 'Pragati Maidan',
  85 => 'Indraprastha',
  86 => 'Yamuna Bank',
  87 => 'Akshardham',
  88 => 'Mayur Vihar Phase-1',
  89 => 'Mayur Vihar Extention',
  90 => 'New Ashok Nagar',
  91 => 'Noida Sector-15',
  92 => 'Noida Sector-16',
  93 => 'Noida Sector-18',
  94 => 'Botanical Garden',
  95 => 'Golf Course',
  96 => 'Noida City Center',
  97 => 'New Delhi-Airport Express',
  98 => 'Shivaji Stadium',
  99 => 'Dhaula Kuan',
  100 => 'Delhi Aero City',
  101 => 'IGI Airport',
  102 => 'Dwarka Sec-21-Airport',
  103 => 'Inder Lok',
  104 => 'Ashok Park Main',
  105 => 'Punjabi Bagh',
  106 => 'Shivaji Park',
  107 => 'Madi Pur',
  108 => 'Paschim Vihar (East)',
  109 => 'Paschim Vihar (West)',
  110 => 'Peera Garhi',
  111 => 'Udyog Nagar',
  112 => 'Surajmal Stadium',
  113 => 'Nangloi',
  114 => 'Nangloi Rly Station',
  115 => 'Rajdhani Park',
  116 => 'Mundka',
  117 => 'DLF Sikanderpur',
  118 => 'DLF Phase 2',
  119 => 'Vodaphone Belvedere Towers',
  120 => 'Indus Bank Cyber City',
  121 => 'Micromax Moulsari',
  122 => 'DLF Phase 3',
  123 => 'ITO',
  124 => 'Mandi House',
  125 => 'Janpath',
  126 => 'Central Secretariat',
  127 => 'Khan Market',
  128 => 'Jawaharlal Nehru Stadium',
  129 => 'Jangpura',
  130 => 'Lajpat Nagar',
  131 => 'Moolchand',
  132 => 'Kailash Colony',
  133 => 'Nehru Place',
  134 => 'Kalkaji Mandir',
  135 => 'Govind Puri',
  136 => 'Okhla',
  137 => 'Jasola',
  138 => 'Sarita Vihar',
  139 => 'Mohan Estate',
  140 => 'Tughlakabad',
  141 => 'Badarpur',
  142 => 'Sarai',
  143 => 'N.H.P.C. Chowk',
  144 => 'Mewala Maharajpur',
  145 => 'Sector 28 Faridabad',
  146 => 'Badkal Mor',
  147 => 'Old Faridabad',
  148 => 'Neelam Chowk Ajronda',
  149 => 'Bata Chowk',
  150 => 'Escorts Mujesar',
  151 => 'Yamuna Bank',
  152 => 'Laxmi Nagar',
  153 => 'Nirman Vihar',
  154 => 'Preet Vihar',
  155 => 'Karkar Duma',
  156 => 'Anand Vihar',
  157 => 'Kaushambi',
  158 => 'Vaishali',
  159 => 'Ashok Park Main',
  160 => 'Satguru Ram Singh Marg',
  161 => 'Kirti Nagar',
);
for($i=0;$i<count($metro);$i++)
{
  echo "<option value='".$metro[$i]."'>".$metro[$i]."</option>";
}                                                  ?>                           </select>
                       </div>                     </div>



														<!-- <div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Offers
															</label>
															<div class="col-sm-10">
																<input type="text"  id="t7" name="ioffer" class="form-control" >
															</div>
														</div> -->

                               <div id="frmoff">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Offers</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="ioffer[]" />&nbsp;&nbsp;&nbsp;&nbsp;
<!-- <a href="javascript:void(0);" class="remove_button_off1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>



</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Offers:     <a href="javascript:void(0);" class="add_button_off" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Video
                              </label>
                              <div class="col-sm-10">
                                <input type="file"  id="t21" name="image9" class="form-control" >
                                <strong style="color:red"><?php if(isset($errors9) and strlen($errors9)>0){echo $errors9;}?></strong>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Pricing
                              </label>
                              <div class="col-sm-10">
                              
                                    <select  name="irating" id="t17">
                              <option value="">--select pricing--</option>
                              <option value="0">Free</option>
                              
                              <option value="2">2</option>
                              <option value="3">3</option>
                              
                          
                              </select>
                              </div>
                            </div> 
                              


</div><hr>

													<!-- 	<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Reviews
															</label>
															<div class="col-sm-10">
																<input type="text"  id="t8" name="ireviews" class="form-control" >
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Rating
															</label>
															<div class="col-sm-10">
																
																		<select  name="irating" id="t9" >
															<option>--select Rating--</option>
															<option>1</option>
															<option>2</option>
															<option>3</option>
															<option>4</option>
															<option>5</option>
															</select>
															</div>
														</div> -->


   <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Image Section</h5>
       </div> 

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Featured Image (Image Size must be 320*320)
                              </label>
                              <div class="col-sm-10">
                                <input type="file"  id="t14" name="image5" class="form-control" >
                                 <strong style="color:red"><?php if(isset($errors5) and strlen($errors5)>0){echo $errors5;}?></strong>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Cover Image (Image Size must be 600*400)
                              </label>
                              <div class="col-sm-10">
                                <input type="file"  id="t14" name="image4" class="form-control" >
                                 <strong style="color:red"><?php if(isset($errors4) and strlen($errors4)>0){echo $errors4;}?></strong>
                              </div>
                            </div>



														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
														Slideshow	Image 1 (Image Size must be 1024*768)
															</label>
															<div class="col-sm-10">
																<input type="file"  id="t10" name="image1" class="form-control" >
                                 <strong style="color:red"><?php if(isset($errors1) and strlen($errors1)>0){echo $errors1;}?></strong>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
														Slideshow	Image 2 (Image Size must be 1024*768)
															</label>
															<div class="col-sm-10">
																<input type="file"  id="t11" name="image2" class="form-control" >
                                 <strong style="color:red"><?php if(isset($errors2) and strlen($errors2)>0){echo $errors2;}?></strong>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
														Slideshow	Image 3 (Image Size must be 1024*768)
															</label>
															<div class="col-sm-10">
																<input type="file"  id="t12" name="image3" class="form-control" >
                                <strong style="color:red"><?php if(isset($errors3) and strlen($errors3)>0){echo $errors3;}?></strong>

															</div>
														</div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                            Slideshow Image 4 (Image Size must be 1024*768)
                              </label>
                              <div class="col-sm-10">
                                <input type="file"  id="t13" name="image6" class="form-control" >
                                <strong style="color:red"><?php if(isset($errors6) and strlen($errors6)>0){echo $errors6;}?></strong>
                              </div>
                            </div>

														
</div>

<!-- 
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Full Image
                              </label>
                              <div class="col-sm-10">
                                <input type="file"  id="t14" name="image5" class="form-control" required>
                              </div>
                            </div> -->


                               <!-- <div class="form-group">
                      <label class="col-sm-2 control-label" for="inputEmail3">
                                Opening-hour 
                              </label>
                              <div class="col-sm-10">
                                 <select class="select3" name="happ" id="t15" >
                                <option value="">-Select-</option>
                                <?php
                                                              $arr=array("Yes","No");
                                                              for($i=0;$i<count($arr);$i++){
                                                                echo "<option>".$arr[$i]."</option>";
                                                              }
                                ?>

                               
                                                               

                                 </select>
                              </div>
                            </div> -->


														
                         <!--      <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Featured
                              </label>
                              <div class="col-sm-10">
                              
                                    <select  name="featured" id="t16">
                              <option>--select--</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                              
                              </select>
                              </div>
                            </div> -->

                              

<!--                              <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Opening Time
                              </label>
                              <div class="col-sm-10">
                                
                               <select name="open[]" id="t18">
                               <option>-time-</option>
<?php
                                    for($id=1;$id<=12;$id++){
                                 
                                 echo "<option>".$id."</option>";
                                   }
                              
?>
                              </select>
                              <select name="open[]" >
                                 <option>-min-</option>
                                 <option>00</option>
                                 <option>15</option>
                                 <option>30</option>
                                 <option>45</option>
</select>
                               
                              <select name="open[]">
                                <option>-AM/PM-</option>
                               <option>AM</option>
                                 <option>PM</option>
                              </select>
                             </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Closing Time
                              </label>
                              <div class="col-sm-10">
                               
                                 <select name="close[]" >
                                   <option>-time-</option>
<?php
                                    for($id=1;$id<=12;$id++){
                                 
                                 echo "<option>".$id."</option>";
                                   }
                              
?>
                              </select>

                             <select name="close[]" >
                                 <option>-min-</option>
                                 <option>00</option>
                                 <option>15</option>
                                 <option>30</option>
                                 <option>45</option>
</select>
                               
                              <select name="close[]" >
                               <option>-AM/PM-</option>
                               <option>AM</option>
                                 <option>PM</option>
                              </select>                            

                            </div>
                            </div>

<div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Item Coordinates
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t19" name="coordi" placeholder="eg: 23.432,77.6554 (seprated by commas)" class="form-control" >
                              </div>
                            </div> -->



 <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Food Images</h5>
       </div> 

 <div id="frmmenu">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Food Image (Image Size must be less than 600*600)</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="menuimage[]" id="mi" style="display:inline-block;"/>
<!-- <a  href="javascript:void(0);" class="remove_button_menu1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>




</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                               <strong style="color:red"><?php if(isset($errors15) and strlen($errors15)>0){echo $errors15;}?></strong>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Food Image:     <a href="javascript:void(0);" class="add_button_menu" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>

</div><hr>

<!-- <div id="frmfull">

<div width="500px">

<div>
<label>Full Image</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="fullimage[]" id="fi"  />
</div></br>


</br>
<a href="javascript:void(0);" class="remove_button_full1" title="Remove field"><img src="images/remove-icon.png"/></a>

</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div>
                                
                            Add One More Full Image:     <a href="javascript:void(0);" class="add_button_full" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br> -->

     <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Bar Photos</h5>
       </div> 

  <div id="frmbar">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Bar Photo (Image Size must be less than 600*600)</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="barimage[]" id="vi" style="display:inline-block;"/>
<!-- <a href="javascript:void(0);" class="remove_button_venue1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>




</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                               <strong style="color:red"><?php if(isset($errors18) and strlen($errors18)>0){echo $errors18;}?></strong>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Bar Photo:     <a href="javascript:void(0);" class="add_button_bar" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>

  </div><hr>

 <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Venue Photos</h5>
       </div> 

  <div id="frmvenue">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Venue Photo (Image Size must be less than 600*600)</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="venueimage[]" id="vi" style="display:inline-block;"/>
<!-- <a href="javascript:void(0);" class="remove_button_venue1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>




</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                               <strong style="color:red"><?php if(isset($errors17) and strlen($errors17)>0){echo $errors17;}?></strong>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Venue Photo:     <a href="javascript:void(0);" class="add_button_venue" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>

  </div><hr>




  <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Opening hours</h5>
       </div> 


  <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputPassword3">
                              Opening hours
                              </label>
                            <div class="col-sm-10">
                          
                                                  <?php
                      $a=array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
                    
                      function hoursRange( $upper = 43200, $lower = 1800, $step = 1800, $format = '' ) 

{
    $times = array();

    if ( empty( $format ) ) {
        $format = 'g:i a';
    }

    foreach ( range( $lower, $upper, $step ) as $increment ) {
        $increment = gmdate( 'H:i', $increment );

        list( $hour, $minutes ) = explode( ':', $increment );

        $date = new DateTime( $hour . ':' . $minutes );

        $times[] = $date->format( $format );
    }

    return $times;
}

$every_30_minutes = hoursRange( 43200, 1800, 60 * 30, 'h:i' );



                    for($i=0;$i<count($a);$i++)
                    {
                    echo "<input type='text' name='$a[$i][]' value='$a[$i]' readonly >";
                  /*  @$c=split('@',$fetch['happy_hours']);
                     
                    @$d=explode('*', $c[$i]);

                    @$e=split('-', $d[1]);*/

                    echo"<select name='$a[$i][]'>";

                    echo"<option>n/a</option>";
                    for($c=0;$c<count($every_30_minutes);$c++){
                    
                    echo "<option>".$every_30_minutes[$c]."</option>";
      
                      }

      echo"</select>";
        echo"<select name='$a[$i][]'>";
        echo"<option>am/pm</option>";
      echo"<option>am</option>";
      echo"<option>pm</option>";
                              echo"</select>";
                      echo"<select name='$a[$i][]'>";
                      echo"<option>n/a</option>";

                      for($f=0;$f<count($every_30_minutes);$f++){
                      
                      
                      echo "<option>".$every_30_minutes[$f]."</option>";
                      
                      }
                        echo"</select>";
                          echo"<select name='$a[$i][]'>";
                          echo"<option>am/pm</option>";
                        echo"<option>am</option>";
                        echo"<option>pm</option>";
                        echo"</select>&nbsp;&nbsp;";
                       // echo "Closed&nbsp;";
                        echo "<select name='$a[$i][]' required>";
                        echo "<option value=''>Select</option>";
                        echo "<option value='o'>Open</option>";
                        echo "<option value='closed'>Closed</option></select>";
                        //echo "<input type='checkbox' value='closed' name='$a[$i][]'>";
                        echo"</br>";
                          echo"</br>";
                                    }
                                    ?>
                            </div>

</div>

</div><hr>

  <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Known For</h5>
       </div> 

<div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Known For
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t5" name="known" class="form-control" >
                              </div>
                            </div>

</div><hr>


  <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Cuisines</h5>
       </div> 


   <div id="frmcuis">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Cuisines</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="cuisines[]" />&nbsp;&nbsp;&nbsp;&nbsp;
<!-- <a href="javascript:void(0);" class="remove_button_cuis1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>



</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Cuisines:     <a href="javascript:void(0);" class="add_button_cuis" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>

  </div><hr>

    <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Facilities</h5>
       </div> 


  <div id="frmhigh">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Facilities</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="highlights[]" />&nbsp;&nbsp;&nbsp;&nbsp;
<!-- <a href="javascript:void(0);" class="remove_button_high1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>




</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Facilities:     <a href="javascript:void(0);" class="add_button_high" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>

  </div><hr>

    <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Cost</h5>
       </div> 

  <div id="frmcost">

<div width="500px" style="padding:20px 0px 0px 20px;">

<div>
<label>Cost</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="cost[]" />&nbsp;&nbsp;&nbsp;&nbsp;
<!-- <a href="javascript:void(0);" class="remove_button_cost1" title="Remove field"><img src="images/remove-icon.png"/></a>
 -->
</div>




</div>
</div></br>

                            <div class="form-group">
                            
                              
                              <div class="space1">
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                            Add Another Cost:     <a href="javascript:void(0);" class="add_button_cost" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

  </div></br>

</div><hr>

                            <!--   <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Menu Image2
                              </label>
                              <div class="col-sm-10">
                                <input type="file"  id="t24" name="image8" class="form-control" required>
                              </div>
                            </div> -->

  <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Busy Nights</h5>
       </div> 

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3" >
                             Busy Nights
                              </label>
                              <div class="col-sm-10">
                                <?php 



                                  $arrb=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
              
                        for($ib=0; $ib<count($arrb); $ib++)
                        { 
                            ?>
                        <input name="busynights[]" type="checkbox" value="<?php echo $arrb[$ib] ?>"  class="checkbox" >
                                                           <?php 

                                                           echo $arrb[$ib];


                                                           }?>
                                        

                         
                       
                  
                                                  </div>
                            </div>


</div>

                             




                     


         														<div class="form-group margin-bottom-0">
															<div class="col-sm-offset-2 col-sm-10">
																
																<input type="submit" name="sen" class="btn btn-o btn-primary"  value="Save">
																
																<input type="submit" name="can" class="btn btn-o btn-primary"  onclick="formvalidation()" value="Cancel">
															      <span style="color:red"><?php if(@$errors){ echo $errors;}?></span> 
															</div>
															  <?php
                                                                 /*if(isset($message) and strlen($message)>0 ) 
                                                                 {
                                                                 	echo $message;
                                                                 }*/
															  ?>
															<div>

															</div>
														</div>

														<?php }
                                                         
                             @$i=$_GET['id'];

														if(isset($action) and $action=='edit'){



                                                       
                                                           @$query=mysqli_query($conn,"SELECT * from items where item_id='$i'");
                                                             @$fetch=mysqli_fetch_assoc($query);
                                                             
                                                             
                                                             
                                                             ?>

 <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Introduction</h5>
       </div> 
                          
                       <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputEmail3">
                                Priority of Venue for Venue Page
                              </label>
                              <div class="col-sm-10">
                               <input type="text" name="prior" id="ep" maxlength="3" class="form-control" value="<?php echo $fetch['item_prior']?>" >
                                 
                                 
                              </div>
                            </div>  

                             <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputEmail3">
                                Priority of Venue for Home Page
                              </label>
                              <div class="col-sm-10">
                               <input type="text" name="hprior" id="hep" maxlength="3" class="form-control" value="<?php echo $fetch['home_prior']?>" >
                                 
                                 
                              </div>
                            </div>  


                            <div class="form-group">
                      <label class="col-sm-2 control-label" for="inputEmail3">
                                Published 
                              </label>
                              <div class="col-sm-10">
                                 <select class="select3" name="status" id="status" value="<?php echo $fetch['item_published'] ?>" >
                                  <option value="">-Select-</option>
                                <?php
                                                              $arr=array("Published","Unpublished");
                                                              @$d=count($arr);
                                                             // echo "hello";
                                                              for($i=0;$i<count($arr);$i++){
                                                                if($fetch['item_published']==$arr[$i]){
                                                                  echo  "<option selected>".$arr[$i]."</option>";
                                                                }else{
                                                                  echo "<option>".$arr[$i]."</option>";
                                                                }
                                                                
                                                              }
                                ?>
                                                               

                                 </select>
                                 
                              </div>
                            </div>  

                              <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                                Add Featured
                              </label>
                              <div class="col-sm-10">
                              <?php if($fetch['item_feat']=="1") 
                              
                                {
                                  echo "<input type='checkbox' name='featured' value='1' checked>";
                                }
                                else
                                {
                                  echo "<input type='checkbox' name='featured' value='1'>";
                                }

                                ?>
                               
                              </div>
                            </div>                              				


														<div class="form-group">
											<label class="col-sm-2 control-label" for="inputEmail3">
														Venue	Title
															</label>
															<div class="col-sm-10">
																<input type="text" name="ititle"  id="t1" class="form-control" value="<?php echo $fetch['item_title']?>" >
															</div>
														</div>
														<div class="form-group">
											<label class="col-sm-2 control-label" for="inputEmail3">
																Category
															</label>
															<div class="col-sm-10">
																<select  name="mobid" id="t2">
															<option>select category</option>
															<?php
															
															 $add=mysqli_query($conn,"select * from category");
                                                           
									                           while($fet=mysqli_fetch_array($add)){
									                           	if( $fet['cate_id']==$fetch['cate_id']){

																 echo "<option selected value=".$fet['cate_id'].">". $fet['cate_title']."</option>";
									                           	}
									                           	else{
									                           		echo "<option value=".$fet['cate_id'].">". $fet['cate_title']."</option>";
									                           	
									                           	}
																}
																?>
																</option>
															</select>
																</div>
														</div>

                                <!-- <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Description Title
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t3" name="desctitle" class="form-control" value="<?php echo $fetch['desc_title']?>"  >
                              </div>
                            </div>

														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Description
															</label>
															<div class="col-sm-10">
																
																<textarea cols="25" id="t3" rows="10" class="form-control" name="idesc" ><?php echo $fetch['item_desc']?></textarea>
															</div>
														</div> -->


                               

														<!-- <div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Phone No.
															</label>
															<div class="col-sm-10">
																<input type="text"  id="uph" name="iphone" class="form-control" value="<?php echo $fetch['item_phone']?>" maxlength="10" >
															</div>
														</div> -->

														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Location
															</label>
															<div class="col-sm-10">
																<!-- <input type="text"  id="t5" name="iloc" class="form-control" value="<?php echo $fetch['item_location']?>" > -->
															<select name="iloc">
                                  <option value="">--Select--</option>
                                  <?php

                                  $loc=mysqli_query($conn,"SELECT * FROM location");
                                  while($res=mysqli_fetch_assoc($loc))
                                  {
                                    if($fetch['item_location']==$res['loc_id'])
                                    {
                                       echo "<option selected value='".$res['loc_id']."'>".$res['loc_title']."</option>";
                                    }
                                    else
                                    {
                                      echo "<option value='".$res['loc_id']."'>".$res['loc_title']."</option>";
                                    }
                                  }



                                  ?>
                                </select>

                              </div>
														</div>


                             <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Address
                              </label>
                              <div class="col-sm-10">
                                <textarea  id="t19" name="address" class="form-control" ><?php echo $fetch['item_address']?></textarea>
                              </div>
                            </div>



                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Item Coordinates
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t19" name="coordi" class="form-control" value="<?php echo $fetch['item_lat'].",".$fetch['item_lng']?>" >
                              </div>
                            </div>


                 <!--     <div class="form-group">                       <label class="col-sm-2 control-label" for="inputPassword3">                       Address                       </label>                       <div class="col-sm-10">                         <textarea  id="t19" name="address" class="form-control" ><?php echo $fetch['item_address']?></textarea>                       </div>                     </div>


                     <div class="form-group">                       <label class="col-sm-2 control-label" for="inputPassword3">                       Item Coordinates                       </label>                       <div class="col-sm-10">                         <input type="text"  id="t19" name="coordi" class="form-control" value="<?php echo $fetch['item_lat'].",".$fetch['item_lng']?>" >                       </div>                     </div> -->
                              <div class="form-group">                       <label class="col-sm-2 control-label" for="inputPassword3">                       Nearest Metro Station                       </label>                       <div class="col-sm-10">                         <select name="metro">                         <option value="">--Select--.</option>                         <?php                               /*$metro=array("Dilshad Garden","Jhil Mil","Mansarovar Park","Shahdara","Welcome","Seelampur","Shastri Park","Kashmere Gate","Tis Hazari","Pul Bangash","Pratap Nagar");*/                         /*$metro= array (
  0 => 'Dilshad Garden',
  1 => ' Jhilmil',
  2 => ' Mansarovar Park',
  3 => ' Shahdara',
  4 => ' Welcome',
  5 => ' Seelampur',
  6 => ' Shastri Park',
  7 => ' Kashmere Gate',
  8 => ' Tis Hazari',
  9 => ' Pul Bangash',
  10 => ' Pratap Nagar',
  11 => ' Shastri Nagar',
  12 => ' Inder Lok',
  13 => ' Kanhiya Nagar',
  14 => ' Keshav Puram',
  15 => ' Netaji Subhash Place',
  16 => ' Kohat Enclave',
  17 => ' Pitam Pura',
  18 => ' Rohini East',
  19 => ' Rohini West',
  20 => ' Rithala',
  21 => 'Jahangirpuri',
  22 => ' Adarsh Nagar',
  23 => ' Azad Pur',
  24 => ' Model Town',
  25 => ' GTB Nagar',
  26 => ' Vishwa Vidyalaya',
  27 => ' Vidhan Sabha',
  28 => ' Civil Lines',
  29 => ' Kashmere Gate',
  30 => ' Delhi Main',
  31 => ' Chawri Bazar',
  32 => ' New Delhi',
  33 => ' Rajiv Chowk',
  34 => ' Patel Chowk',
  35 => ' Central Sectt',
  36 => ' Noida City Centre',
  37 => ' Golf course',
  38 => ' Botanical Garden',
  39 => ' Noida Sector 18',
  40 => ' Noida Sector 16',
  41 => ' Noida Sector 15',
  42 => ' New Ashok Nagar',
  43 => ' Mayur Vihar Ext. Mayur Vihar –I',
  44 => ' Akshardham',
  45 => ' Yamuna Bank',
  46 => ' Indraprastha',
  47 => ' Pragati Maidan',
  48 => ' Mandi House',
  49 => ' Barakhamba Road',
  50 => ' Rajiv Chowk',
  51 => ' R.K. Ashram Marg',
  52 => ' Jhandewalan',
  53 => ' Karol Bagh',
  54 => ' Rajendra Place',
  55 => ' Patel Nagar',
  56 => ' Shadipur',
  57 => ' Kirti Nagar',
  58 => ' Moti Nagar',
  59 => ' Ramesh Nagar',
  60 => ' Rajouri Garden',
  61 => ' Tagore Garden',
  62 => ' Subhash Nagar',
  63 => ' Tilak Nagar',
  64 => ' Janakpuri East',
  65 => ' Janakpuri West',
  66 => ' Uttam Nagar East',
  67 => ' Uttam Nagar West',
  68 => ' Nawada',
  69 => ' Dwarka Morh',
  70 => ' Dwarka',
  71 => ' Dwarka Sec-14',
  72 => ' Dwarka Sec-13',
  73 => ' Dwarka Sec-12',
  74 => ' Dwarka Sec-11',
  75 => ' Dwarka Sec-10',
  76 => ' Dwarka Sec-9',
  77 => ' Laxmi Nagar',
  78 => ' Nirman Vihar',
  79 => ' Preet Vihar',
  80 => ' Karkarduma',
  81 => ' Anand Vihar',
);*/
$metro=array (
  0 => 'Dilshad Garden',
  1 => 'Jhil Mil',
  2 => 'Welcome',
  3 => 'Seelampur',
  4 => 'Shastri Park',
  5 => 'Kashmere Gate',
  6 => 'Tis Hazari',
  7 => 'Pul Bangash',
  8 => 'Pratap Nagar',
  9 => 'Shastri Nagar',
  10 => 'Inder Lok',
  11 => 'Kanhaiya Nagar',
  12 => 'Keshav Puram',
  13 => 'Netaji Subhash Place',
  14 => 'Kohat Enclave',
  15 => 'Pitam Pura',
  16 => 'Rohini East',
  17 => 'Rohini West',
  18 => 'Rithala',
  19 => 'Jahangirpuri',
  20 => 'Adarsh Nagar',
  21 => 'Azadpur',
  22 => 'Model Town',
  23 => 'G.T.B. Nagar',
  24 => 'Vishwavidyalaya',
  25 => 'Rajiv Chowk',
  26 => 'Vidhan Sabha',
  27 => 'New Delhi',
  28 => 'Civil Lines',
  29 => 'Kashmere Gate',
  30 => 'Chandni Chowk',
  31 => 'Chawri Bazar',
  32 => 'Patel Chowk',
  33 => 'Central Secretariat',
  34 => 'Udyog Bhawan',
  35 => 'Race Course',
  36 => 'Jorbagh',
  37 => 'INA',
  38 => 'AIIMS',
  39 => 'Green Park',
  40 => 'Hauz Khas',
  41 => 'Malviya Nagar',
  42 => 'Saket',
  43 => 'Qutab Minar',
  44 => 'Chhattarpur',
  45 => 'Sultanpur',
  46 => 'Ghitorni',
  47 => 'Arjan Garh',
  48 => 'Guru Dronacharya',
  49 => 'Sikandarpur',
  50 => 'MG Road',
  51 => 'IFFCO Chowk',
  52 => 'Huda City Centre',
  53 => 'Dwarka Sec-21',
  54 => 'Dwarka Sec-08',
  55 => 'Dwarka Sec-09',
  56 => 'Dwarka Sec-10',
  57 => 'Dwarka Sec-11',
  58 => 'Dwarka Sec-12',
  59 => 'Dwarka Sec-13',
  60 => 'Dwarka Sec-14',
  61 => 'Dwarka',
  62 => 'Dwarka Mor',
  63 => 'Nawada',
  64 => 'Uttam Nagar West',
  65 => 'Uttam Nagar East',
  66 => 'Janak Puri West',
  67 => 'Janak Puri East',
  68 => 'Tilak Nagar',
  69 => 'Subhash Nagar',
  70 => 'Tagore Garden',
  71 => 'Rajouri Garden',
  72 => 'Ramesh Nagar',
  73 => 'Moti Nagar',
  74 => 'Kirti Nagar',
  75 => 'Shadipur',
  76 => 'Patel Nagar',
  77 => 'Rajendra Place',
  78 => 'Karol Bagh',
  79 => 'Jhandewalan',
  80 => 'R K Ashram Marg',
  81 => 'Rajiv Chowk',
  82 => 'Barakhamba',
  83 => 'Mandi House',
  84 => 'Pragati Maidan',
  85 => 'Indraprastha',
  86 => 'Yamuna Bank',
  87 => 'Akshardham',
  88 => 'Mayur Vihar Phase-1',
  89 => 'Mayur Vihar Extention',
  90 => 'New Ashok Nagar',
  91 => 'Noida Sector-15',
  92 => 'Noida Sector-16',
  93 => 'Noida Sector-18',
  94 => 'Botanical Garden',
  95 => 'Golf Course',
  96 => 'Noida City Center',
  97 => 'New Delhi-Airport Express',
  98 => 'Shivaji Stadium',
  99 => 'Dhaula Kuan',
  100 => 'Delhi Aero City',
  101 => 'IGI Airport',
  102 => 'Dwarka Sec-21-Airport',
  103 => 'Inder Lok',
  104 => 'Ashok Park Main',
  105 => 'Punjabi Bagh',
  106 => 'Shivaji Park',
  107 => 'Madi Pur',
  108 => 'Paschim Vihar (East)',
  109 => 'Paschim Vihar (West)',
  110 => 'Peera Garhi',
  111 => 'Udyog Nagar',
  112 => 'Surajmal Stadium',
  113 => 'Nangloi',
  114 => 'Nangloi Rly Station',
  115 => 'Rajdhani Park',
  116 => 'Mundka',
  117 => 'DLF Sikanderpur',
  118 => 'DLF Phase 2',
  119 => 'Vodaphone Belvedere Towers',
  120 => 'Indus Bank Cyber City',
  121 => 'Micromax Moulsari',
  122 => 'DLF Phase 3',
  123 => 'ITO',
  124 => 'Mandi House',
  125 => 'Janpath',
  126 => 'Central Secretariat',
  127 => 'Khan Market',
  128 => 'Jawaharlal Nehru Stadium',
  129 => 'Jangpura',
  130 => 'Lajpat Nagar',
  131 => 'Moolchand',
  132 => 'Kailash Colony',
  133 => 'Nehru Place',
  134 => 'Kalkaji Mandir',
  135 => 'Govind Puri',
  136 => 'Okhla',
  137 => 'Jasola',
  138 => 'Sarita Vihar',
  139 => 'Mohan Estate',
  140 => 'Tughlakabad',
  141 => 'Badarpur',
  142 => 'Sarai',
  143 => 'N.H.P.C. Chowk',
  144 => 'Mewala Maharajpur',
  145 => 'Sector 28 Faridabad',
  146 => 'Badkal Mor',
  147 => 'Old Faridabad',
  148 => 'Neelam Chowk Ajronda',
  149 => 'Bata Chowk',
  150 => 'Escorts Mujesar',
  151 => 'Yamuna Bank',
  152 => 'Laxmi Nagar',
  153 => 'Nirman Vihar',
  154 => 'Preet Vihar',
  155 => 'Karkar Duma',
  156 => 'Anand Vihar',
  157 => 'Kaushambi',
  158 => 'Vaishali',
  159 => 'Ashok Park Main',
  160 => 'Satguru Ram Singh Marg',
  161 => 'Kirti Nagar',
);
for($i=0;$i<count($metro);$i++)
{
  if($fetch['metro_station']==$metro[$i])
  {
    echo "<option selected value='".$metro[$i]."'>".$metro[$i]."</option>";
  }
  else
  {
    echo "<option value='".$metro[$i]."'>".$metro[$i]."</option>";
  }
  
}                                                  ?>                           </select>
                       </div>                     </div>														<!-- <div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Offers
															</label>
															<div class="col-sm-10">
																<input type="text"  id="t6" name="ioffer" class="form-control" value="<?php echo $fetch['item_offer']?>" >
															</div>
														</div> -->

                            <?php

                            $oi=explode(",",$fetch['item_offer']);
                          
                            
                            for($ol=0;$ol<count($oi);$ol++)
                            {
                              
                          
echo '<div id="frmoff_'.$ol.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Offers
                              </label>
                              <div class="field_wrapper_off1">
                              <div>
                                  
                                  
                                                         
                                <input type="text" id="t5" name="ioffer[]" value="<?php echo $oi[$ol] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_off" style="position:absolute; margin-left:380px; margin-top:-40px" title="Remove field"><img  src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>
  <div id="frmoff"></div>
  <br>

            <div class="form-group">
                            
                              
                              <div class="space1"> 
                              <label >
                                Click Here to add Another Offers
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_off" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>


                             <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Video
                              </label>
                              <div class="col-sm-10">
                              <video width="100" height="80" autoplay><source src="<?php echo $fetch['item_video'] ?>" ></video>
                                <input type="file"  id="t22" name="image9" class="form-control" >
                              <input type="hidden" name="image99" value="<?php echo $fetch['item_video']?>">
                              <strong style="color:red"><?php if(isset($errors9) and strlen($errors9)>0){echo $errors9;}?></strong>
                              </div>
                            </div>


                              <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Pricing
                              </label>
                              <div class="col-sm-10">
                                
                                <select  name="irating" id="t17">
                              <option value="">--select pricing--</option>
                              <?php

                            /* $pr=array("0","2","3");                          
                                            
                               for($g=0;$g<count($pr);$g++)
                               {
                                              if($pr[$g]==$fetch['item_rating'])
                                          {
                                            echo "<option selected value='".$pr[$g]."'' >".$pr[$g]."</option>";
                                          }
                                          else
                                          {
                                          echo "<option value='".$pr[$g]."''>".$pr[$g]."</option>";
                                          }
                                } */

                                  $pr=array("0"=>"Free", "2"=>"2", "3"=>"3");                          
                                            
                               foreach($pr as $key => $value)
                               {
                                              if($key==$fetch['item_rating'])
                                          {
                                            echo "<option selected value='".$key."'' >".$value."</option>";
                                          }
                                          else
                                          {
                                          echo "<option value='".$key."''>".$value."</option>";
                                          }
                                }                                   
                                
                              
                              
                                
                                ?>
                                </option>
                              </select>
                              </div>
                            </div> 

</div><hr>

													<!-- 	<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Reviews
															</label>
															<div class="col-sm-10">
																<input type="text"  id="t7" name="ireviews" class="form-control" value="<?php echo $fetch['item_reviews']?>" >
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															 Rating
															</label>
															<div class="col-sm-10">
															
																<select  name="irating" id="t8">
															<option>select rating</option>
															<?php
															
															 $add1=mysqli_query($conn,"select * from items where item_id='$i'");
                                                           
									                           $fet1=mysqli_fetch_array($add1);
									                           for($f=1;$f<=5;$f++){
		if($f==$fet1['item_rating'])
{
	echo "<option selected>".$f."</option>";
}
else{
echo "<option>".$f."</option>";
}
}
																
																?>
																</option>
															</select>
															</div>
														</div> -->

 <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Image Section</h5>
       </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Featured Image (Image Size must be 320*320)
                              </label>
                              <div class="col-sm-10">
                              <image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['feat_image']?>">
                                <input type="file"  id="t13" name="image5"  class="form-control" >
                              <input type="hidden" name="image55" value="<?php echo $fetch['feat_image']?>">
                               <strong style="color:red"><?php if(isset($errors5) and strlen($errors5)>0){echo $errors5;}?></strong>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Cover Image (Image Size must be 600*400)
                              </label>
                              <div class="col-sm-10">
                              <image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['cover_img']?>">
                                <input type="file"  id="t13" name="image4"  class="form-control" >
                              <input type="hidden" name="image44" value="<?php echo $fetch['cover_img']?>">
                               <strong style="color:red"><?php if(isset($errors4) and strlen($errors4)>0){echo $errors4;}?></strong>
                              </div>
                            </div>

														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Image 1 (Image Size must be 1024*768)
															</label>
															<div class="col-sm-10">
															<image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['item_img1']?>">
																<input type="file"  id="t9" name="image1"  class="form-control" >
															<input type="hidden" name="image11" value="<?php echo $fetch['item_img1']?>">
                               <strong style="color:red"><?php if(isset($errors1) and strlen($errors1)>0){echo $errors1;}?></strong>
                              </div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Image 2 (Image Size must be 1024*768)
															</label>
															<div class="col-sm-10">
															<image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['item_img2']?>">
																<input type="file"  id="t10" name="image2"  class="form-control"  >
															<input type="hidden" name="image22" value="<?php echo $fetch['item_img2']?>">
                               <strong style="color:red"><?php if(isset($errors2) and strlen($errors2)>0){echo $errors2;}?></strong>
                              </div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label" for="inputPassword3">
															Image 3 (Image Size must be 1024*768)
															</label>
															<div class="col-sm-10">
															<image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['item_img3']?>">
																<input type="file"  id="t11" name="image3"  class="form-control"  >
															<input type="hidden" name="image33" value="<?php echo $fetch['item_img3']?>">
                               <strong style="color:red"><?php if(isset($errors3) and strlen($errors3)>0){echo $errors3;}?></strong>
                              </div>
														</div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Image 4 (Image Size must be 1024*768)
                              </label>
                              <div class="col-sm-10">
                              <image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['item_img4']?>">
                                <input type="file"  id="t12" name="image6"  class="form-control" >
                              <input type="hidden" name="image66" value="<?php echo $fetch['item_img4']?>">
                               <strong style="color:red"><?php if(isset($errors6) and strlen($errors6)>0){echo $errors6;}?></strong>
                              </div>
                            </div>

														

</div><hr>
<!-- 
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Full Image
                              </label>
                              <div class="col-sm-10">
                              <image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['full_image']?>">
                                <input type="file"  id="t14" name="image5"  class="form-control" >
                              <input type="hidden" name="image55" value="<?php echo $fetch['full_image']?>">
                              </div>
                            </div> -->




                             <!--      <div class="form-group">
                      <label class="col-sm-2 control-label" for="inputEmail3">
                                Happy hour 
                              </label>
                              <div class="col-sm-10">
                                 <select class="select3" name="happ" id="t15" >
                                  <option value="">-Select-</option>
                                <?php
                                                              $arr=array("Yes","No");
                                                              for($i=0;$i<count($arr);$i++){
                                                                if($fetch['happy']==$arr[$i]){
                                                                  echo  "<option selected>".$arr[$i]."</option>";
                                                                }else{
                                                                  echo "<option>".$arr[$i]."</option>";
                                                                }
                                                                
                                                              }
                                ?>
                                                               

                                 </select>
                              </div>
                            </div> -->




                                                   <!--   <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Featured
                              </label>
                              <div class="col-sm-10">
                                
                                    <select  name="featured" id="t16">
                              <option>--select--</option>
                              <?php if($fetch['item_featured']=="Yes")
                              {?>

                              <option value="Yes" selected>Yes</option>
                              <option value="No" >No</option>
                              <?php }else{ ?>
                              <option value="Yes" >Yes</option>
                              <option value="No" selected>No</option>
                              <?php }?>
                              
                              </select>
                              </div>
                            </div> -->

                                      


                               <!--                    
                                   <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Opening Time
                              </label>
                              <div class="col-sm-10">
                             
<?php
//echo $fetch['opening_time'];
$splitam=split(" ",$fetch['opening_time']);
//echo $splitam[0];
$splittime=split(":",$splitam[0]);
//echo $splittime[0];
$minarr = array("-min-",00,15,30,45);
$ampmarr= array("-AM/PM-","AM","PM");
echo "<select name='open[]'>";
$splitam=split(" ",$fetch['opening_time']);
$splittime=split(":",$splitam[0]);
for($idd=1;$idd<=12;$idd++){

if($splittime[0]==$idd)
{
echo "<option selected>".$idd."</option>";
}
else{
echo "<option>".$idd."</option>";
}
}
echo "</select>";

echo "<select name='open[]'>";
for($iddd=0;$iddd<count($minarr);$iddd++){
if($minarr[$iddd]==$splittime[1]){
echo "<option selected>".$minarr[$iddd]."</option>";
}
else{
echo "<option>".$minarr[$iddd]."</option>";
}


}

echo "</select>";

echo "<select name='open[]'>";
for($idddd=0;$idddd<count($ampmarr);$idddd++){
if($minarr[$idddd]==$splitam[1]){
echo "<option selected>".$ampmarr[$idddd]."</option>";
}
else{
echo "<option>".$ampmarr[$idddd]."</option>";
}


}

echo "</select>";




?>













                              </div>
                            </div> -->

           <!--                  <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Closing Time
                              </label>
                              <div class="col-sm-10">
                                

<?php
//echo $fetch['opening_time'];
$splitamc=split(" ",$fetch['closing_time']);
//echo $splitamc[1];
$splittimec=split(":",$splitamc[0]);
//echo $splittimec[1];
$minarrc = array("-min-",00,15,30,45);
$ampmarrc= array("-AM/PM-","AM","PM");
echo "<select name='close[]'>";

for($am=1;$am<=12;$am++){

if($splittimec[0]==$am)
{
echo "<option selected>".$am."</option>";
}
else{
echo "<option>".$am."</option>";
}
}
echo "</select>";

echo "<select name='close[]'>";
for($amc=0;$amc<count($minarrc);$amc++){
if($minarrc[$amc]==$splittimec[1]){
echo "<option selected>".$minarrc[$amc]."</option>";
}
else{
echo "<option>".$minarrc[$amc]."</option>";
}


}

echo "</select>";

echo "<select name='close[]'>";
for($amd=0;$amd<count($ampmarrc);$amd++){
if($ampmarrc[$amd]==$splitamc[1]){
echo "<option selected>".$ampmarrc[$amd]."</option>";
}
else{
echo "<option>".$ampmarrc[$amd]."</option>";
}


}

echo "</select>";




?>


                              </div>
                            </div> -->





													


                        <!--     <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Menu Image1
                              </label>
                              <div class="col-sm-10">
                              <image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['item_menu_image1']?>">
                              <input type="file"  id="t20" name="image7"  class="form-control" >
                              <input type="hidden" name="image77" value="<?php echo $fetch['item_menu_image1']?>">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                              Menu Image2
                              </label>
                              <div class="col-sm-10">
                              <image  id="t3" class="h" width="60px" height="60px" src="<?php echo $fetch['item_menu_image2']?>">
                                <input type="file"  id="t21" name="image8" class="form-control" >
                              <input type="hidden" name="image88" value="<?php echo $fetch['item_menu_image2']?>">
                              </div>
                            </div> -->

 <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Food Images</h5>
       </div>

<?php

                            $mi=explode(",",$fetch['menu_image']);
                          
                            
                            for($ml=0;$ml<count($mi);$ml++)
                            {
                              
                          
echo '<div id="frmmenu_'.$ml.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Food Image (Image Size must be less than 600*600)
                              </label>
                              <div class="field_wrapper_menu1">
                              <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <img height="50px" width="50px" src="<?php echo $mi[$ml] ?>">
                                <input type="file" name="menuimage[]" style="display:inline-block;">                             
                                <input type="hidden" id="t5" name="menuimage1[]" value="<?php echo $mi[$ml] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_menu" style="position:absolute; margin-left:480px; margin-top:-50px" title="Remove field"><img src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>
<div id="frmmenu"></div>
<br>
            <div class="form-group">
                            
                              
                              <div class="space1">
                               <strong style="color:red"><?php if(isset($errors15) and strlen($errors15)>0){echo $errors15;}?></strong>
                              <label >
                                Click Here to add Another Food Image
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_menu"  title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                            

</div><hr>


<?php

                            /*$fi=explode(",",$fetch['full_image']);
                          
                            
                            for($fl=0;$fl<count($fi);$fl++)
                            {
                              
                          
echo '<div id="frmfull_'.$fl.'">'; 
*/
                        ?>


     <!--                    <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Full Image
                              </label>
                              <div class="field_wrapper_full1">
                              <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <img height="50px" width="50px" src="<?php echo $fi[$fl] ?>">
                                <input type="file" name="fullimage[]">                             
                                <input type="hidden" id="t5" name="fullimage1[]" value="<?php echo $fi[$fl] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_full" title="Remove field"><img src="images/remove-icon.png"/></a>
</div>
 -->

                            <?php //} ?>

           <!--  <div class="form-group">
                            
                              
                              <div>
                              <label >
                                Click Here to add one more Full Image
                              </label>
                                 
                                    <a href="javascript:void(0);" class="add_button_full" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                            <div id="frmfull"></div>
 -->

   <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Bar Photos</h5>
       </div>

<?php

                            $bi=explode(",",$fetch['bar_image']);
                          
                            
                            for($bl=0;$bl<count($bi);$bl++)
                            {
                              
                          
echo '<div id="frmbar_'.$bl.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Bar Photo (Image Size must be less than 600*600)
                              </label>
                              <div class="field_wrapper_venue1">
                              <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <img height="50px" width="50px" src="<?php echo $bi[$bl] ?>">
                                <input type="file" name="barimage[]" style="display:inline-block;">                             
                                <input type="hidden" id="t5" name="barimage1[]" value="<?php echo $bi[$bl] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_bar" style="position:absolute; margin-left:480px; margin-top:-50px" title="Remove field"><img src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>

 <div id="frmbar"></div>
 <br>

            <div class="form-group">
                            
                              
                              <div class="space1">
                               <strong style="color:red"><?php if(isset($errors18) and strlen($errors18)>0){echo $errors18;}?></strong>
                              <label >
                                Click Here to add Another Bar photo
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_bar" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                           

</div><hr>

  <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Venue Photos</h5>
       </div>

<?php

                            $vi=explode(",",$fetch['venue_image']);
                          
                            
                            for($vl=0;$vl<count($vi);$vl++)
                            {
                              
                          
echo '<div id="frmvenue_'.$vl.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Venue Photo (Image Size must be less than 600*600)
                              </label>
                              <div class="field_wrapper_venue1">
                              <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <img height="50px" width="50px" src="<?php echo $vi[$vl] ?>">
                                <input type="file" name="venueimage[]" style="display:inline-block;">                             
                                <input type="hidden" id="t5" name="venueimage1[]" value="<?php echo $vi[$vl] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_venue" style="position:absolute; margin-left:480px; margin-top:-50px" title="Remove field"><img src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>

 <div id="frmvenue"></div>
 <br>

            <div class="form-group">
                            
                              
                              <div class="space1">
                               <strong style="color:red"><?php if(isset($errors17) and strlen($errors17)>0){echo $errors17;}?></strong>
                              <label >
                                Click Here to add Another Venue photo
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_venue" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                           

</div><hr>




 <div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Opening Hours</h5>
       </div>



                            <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputPassword3">
                              Opening hours
                              </label>
                            <div class="col-sm-10">
                            <?php

                            function hoursRange( $upper = 43200, $lower = 1800, $step = 1800, $format = '' ) 

                            {
                                $times = array();

                                if ( empty( $format ) ) {
                                    $format = 'g:i a';
                                }

                                foreach ( range( $lower, $upper, $step ) as $increment ) {
                                    $increment = gmdate( 'H:i', $increment );

                                    list( $hour, $minutes ) = explode( ':', $increment );

                                    $date = new DateTime( $hour . ':' . $minutes );

                                    $times[] = $date->format( $format );
                                }

                                return $times;
                            }

                            $every_30_minutes = hoursRange( 43200, 1800, 60 * 30, 'h:i' );

                            $a=array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
                            $arr=array("am/pm","am","pm");
for($i=0;$i<count($a);$i++){
echo "<input type='text' name='$a[$i][]' value='$a[$i]' readonly >";
@$c=split('@',$fetch['happy_hours']);
 
@$d=explode('*', $c[$i]);

@$e=split('-', $d[1]);
@$clo=split('#', $d[1]);
//echo strlen($e[0]);
if($e[0]!=="n/aam/pm"){
if(strlen($e[0])==7){
@$p=substr($e[0], 5);       
//@$diff=($e[0]-$p);
@$diff=substr($e[0],0, 5);

//echo $p;
//echo $diff;
}

/* else{
  $p=substr($e[0], 2);
  $diff=($e[0]-$p);
  } */
}
else{$p="";
   $diff="n/a";
}

@$z=split('#', $e[1]);
//echo $z[0];
if($z[0]!=="n/aam/pm"){
if(strlen($z[0])==7){
@$pi=substr($z[0], 5);
//@$dif=($z[0]-$pi);
@$dif=substr($z[0],0, 5);
//echo $pi;
}
 /*else{
  $pi=substr($z[0], 2);
  $dif=($z[0]-$pi);
  } */
}
else{$pi="";
$dif="n/a";
}

echo"<select name='$a[$i][]'>";

echo"<option>n/a</option>";
for($c=0;$c<count($every_30_minutes);$c++){
  if($every_30_minutes[$c]==$diff)
{
  echo "<option selected value='".$every_30_minutes[$c]."'>".$every_30_minutes[$c]."</option>";
}

else{
echo "<option value='".$every_30_minutes[$c]."'>".$every_30_minutes[$c]."</option>";
}
}

echo"</select>";
  echo"<select name='$a[$i][]'>";
/*  echo"<option></option>";
echo"<option>am</option>";
echo"<option>pm</option>";*/
for($r=0;$r<count($arr);$r++){
  if($arr[$r]==$p)
{
  echo "<option selected>".$arr[$r]."</option>";
}

else{
echo "<option>".$arr[$r]."</option>";
}
}
  echo"</select>";
echo"<select name='$a[$i][]'>";
echo"<option>n/a</option>";

for($f=0;$f<count($every_30_minutes);$f++){
    if($every_30_minutes[$f]==$dif)
{
  echo "<option selected>".$every_30_minutes[$f]."</option>";
}
else{
echo "<option>".$every_30_minutes[$f]."</option>";
}
}
echo"</select>";
  echo"<select name='$a[$i][]'>";
  /*
  echo"<option></option>";
echo"<option>am</option>";
echo"<option>pm</option>";*/

for($s=0;$s<count($arr);$s++)
{
  if($arr[$s]==$pi)
{
  echo "<option selected>".$arr[$s]."</option>";
}

else{
echo "<option>".$arr[$s]."</option>";
}
}
  echo"</select>&nbsp;";
  /*echo "Closed&nbsp;";
  if($clo[1]!=="closed")
  {
    echo "<input type='checkbox' value='closed' name='$a[$i][]'>";
  }
  else
  {
    echo "<input type='checkbox' checked value='closed' name='$a[$i][]'>";
  }*/

/*echo "<select>";
   $arrt=array(" ","closed");
                                                              for($t=0;$t<count($arrt);$t++)
                                                              {
                                                                if($clo[1]==$arrt[$t]){
                                                                  echo  "<option selected>".$arrt[$t]."</option>";
                                                                }else
                                                                {
                                                                  echo "<option>".$arrt[$t]."</option>";
                                                                }
                                                             
                                                                
                                                              }

echo "</select>";*/
echo "<select name='$a[$i][]'>";
 $pr=array("o"=>"Open", "closed"=>"Closed"); 
//echo $clo[1];                       
                                            
                                               foreach($pr as $key => $value)
                                               {
                                                              if($key==$clo[1])
                                                          {
                                                            echo "<option selected value='".$key."'' >".$value."</option>";
                                                          }
                                                          else
                                                          {
                                                          echo "<option value='".$key."''>".$value."</option>";
                                                          }

                                               }   

echo "</select>";
echo"</br>";
echo"</br>";
}
?>  
                                                    </div></div>


</div><hr>

<div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Known For</h5>
       </div>


 <div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3">
                               Known For
                              </label>
                              <div class="col-sm-10">
                                <input type="text"  id="t3" name="known" class="form-control" value="<?php echo $fetch['known_for']?>"  >
                              </div>
                            </div>

</div><hr>

<div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Cuisines</h5>
       </div>


<?php

                            $cui=explode(",",$fetch['cuisines']);
                          
                            
                            for($cul=0;$cul<count($cui);$cul++)
                            {
                              
                          
echo '<div id="frmcuis_'.$cul.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Cuisines
                              </label>
                              <div class="field_wrapper_cuis1">
                              <div>
                                  
                                  
                                                         
                                <input type="text" id="t5" name="cuisines[]" value="<?php echo $cui[$cul] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_cuis" style="position:absolute; margin-left:380px; margin-top:-40px" title="Remove field"><img  src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>
  <div id="frmcuis"></div>
  <br>

            <div class="form-group">
                            
                              
                              <div class="space1"> 
                              <label >
                                Click Here to add Another Cuisines
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_cuis" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                          
</div><hr>

<div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Facilities</h5>
       </div>

                            <?php

                            $hi=explode(",",$fetch['highlights']);
                          
                            
                            for($hl=0;$hl<count($hi);$hl++)
                            {
                              
                          
echo '<div id="frmhigh_'.$hl.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Facilities
                              </label>
                              <div class="field_wrapper_high1">
                              <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  
                                                         
                                <input type="text" id="t5" name="highlights[]" value="<?php echo $hi[$hl] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_high" style="position:absolute; margin-left:380px; margin-top:-40px" title="Remove field"><img src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>
 <div id="frmhigh"></div>
 <br>
            <div class="form-group">
                            
                              
                              <div class="space1">
                              <label >
                                Click Here to add Another Facilities
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_high" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                           

</div><hr>

<div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Cost</h5>
       </div>


                            <?php

                            $coi=explode(",",$fetch['cost']);
                          
                            
                            for($col=0;$col<count($coi);$col++)
                            {
                              
                          
echo '<div id="frmcost_'.$col.'">'; 

                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail3">
                             Cost
                              </label>
                              <div class="field_wrapper_cost1">
                              <div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  
                                                         
                                <input type="text" id="t5" name="cost[]" value="<?php echo $coi[$col] ?>" />
                                    
                                </div>
                            </div>
                            </div>

                            <a href="javascript:void(0);" class="remove_button_cost" style="position:absolute; margin-left:380px; margin-top:-40px" title="Remove field"><img src="images/remove-icon.png"/></a>
</div>


                            <?php } ?>
<div id="frmcost"></div>
<br>
            <div class="form-group">
                            
                              
                              <div class="space1">
                              <label >
                                Click Here to Another Cost
                              </label>
                                  <!-- <input type="text" name="field_name[]" id="t2"  required/> -->
                                    <a href="javascript:void(0);" class="add_button_cost" title="Add field"><img src="images/add-icon.png"/></a>
                                </div>
                            

                            </div>

                            

</div><hr>

<div id="intro">
    
       <div class="panel-heading">
         <h5 class="panel-title">Busy Nights</h5>
       </div>

<div class="form-group">
                              <label class="col-sm-2 control-label" for="inputPassword3" >
                             Busy Nights
                              </label>
                              <div class="col-sm-10">
                                <?php 

                        $busy = explode(",",$fetch['busy_nights']);

                                  $arrb=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
              
                        for($ib=0; $ib<count($arrb); $ib++)
                        { 
                            ?>
                       <!--  <input name="busynights[]" type="checkbox" value="<?php echo $arrb[$ib] ?>"  class="checkbox" > -->
                                                           <?php 

                                                           //echo $arrb[$ib];


                          if(in_array($arrb[$ib], $busy))
                            { ?>
                        <input name="busynights[]" type="checkbox" checked="checked" value="<?php echo $arrb[$ib]; ?>" id="t12"  class="checkbox" >
                                                           <?php echo $arrb[$ib]; ?>
                                        

                            <?php }
                        else{
                        ?>
                                            <input name="busynights[]" type="checkbox" value="<?php echo $arrb[$ib]; ?>" id="t12"  class="checkbox" >
                                       <?php echo $arrb[$ib];
                                        }








                         }?>
                                        

                         
                       
                  
                                                  </div>
                            </div>


</div>
                             




														<div class="form-group margin-bottom-0">
															<div class="col-sm-offset-2 col-sm-10">
																
																<input type="submit" name="send" class="btn btn-o btn-primary"  value="Save">
																
																<input type="submit" name="can" class="btn btn-o btn-primary"  onclick="formvalidation()" value="Cancel">
															      <span style="color:red"><?php if(@$errors){ echo $errors;}?></span> 
															</div> 
															  <?php
                                                                /* if(isset($message) and strlen($message)>0 ) 
                                                                 {
                                                                 	echo $message;
                                                                 }*/
															  ?>
															<div>

															</div>
														</div>



															<?php }?>
													</form>
												</div>
											</div>
										</div>












								
								</div>
							
							</div>
						</div>

						
									</div>
								</div>
							</div>
						</div>
						<!-- end: FOURTH SECTION -->
					</div>
				</div>
			</div>


<!-- crop form starts -->



<!-- <div style="margin:0 auto; width:600px">
<label>width</label>
<input type='text' id='wid' readonly>
<label>Height</label>
<input type='text' id='hei' readonly>
<?php echo @$image; ?>
<div id="thumbs" style="padding:5px; width:600px"></div>
<div style="width:600px">

 <form id="cropimage" method="post" enctype="multipart/form-data">
 Select Image to Crop<select name="image_type" id="image_type" required>
<option value="">--Select--</option>
<option value="item_img1">Image 1</option>
<option value="item_img2">Image 2</option>
<option value="item_img3">Image 3</option>
<option value="item_img4">Image 4</option>
<option value="cover_img">Cover Image</option>
<option value="full_image">Full Image</option>
<option value="item_menu_image1">Menu Image1</option>
<option value="item_menu_image2">Menu Image2</option>
</select></br> 
	Upload to crop your image <input type="file" name="photoimg" id="photoimg" />
	<input type="hidden" name="image_name" id="image_name" value="<?php echo($actual_image_name)?>" />
	<input type="submit" name="crop" value="Submit" />
</form> 

</div>
</div> -->
<!--crop form ends-->














			<!-- start: FOOTER -->
			<footer>
				<div class="footer-inner">
					<div class="pull-left">
						&copy; <span class="current-year"></span><span class="text-bold text-uppercase">ClubGo</span>. <span>All rights reserved</span>
					</div>
					<div class="pull-right">
						<span class="go-top"><i class="ti-angle-up"></i></span>
					</div>
				</div>
			</footer>
			<!-- end: FOOTER -->
			<!-- start: OFF-SIDEBAR -->
			<div id="off-sidebar" class="sidebar">
				<div class="sidebar-wrapper">
					<ul class="nav nav-tabs nav-justified">
						<li class="active">
							<a href="#off-users" aria-controls="off-users" role="tab" data-toggle="tab">
								<i class="ti-comments"></i>
							</a>
						</li>
						<li>
							<a href="#off-favorites" aria-controls="off-favorites" role="tab" data-toggle="tab">
								<i class="ti-heart"></i>
							</a>
						</li>
						<li>
							<a href="#off-settings" aria-controls="off-settings" role="tab" data-toggle="tab">
								<i class="ti-settings"></i>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="off-users">
							<div id="users" toggleable active-class="chat-open">
								<div class="users-list">
									<div class="sidebar-content perfect-scrollbar">
										<h5 class="sidebar-title">On-line</h5>
										<ul class="media-list">
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<i class="fa fa-circle status-online"></i>
													<img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Nicole Bell</h4>
														<span> Content Designer </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<div class="user-label">
														<span class="label label-success">3</span>
													</div>
													<i class="fa fa-circle status-online"></i>
													<img alt="..." src="assets/images/avatar-3.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Steven Thompson</h4>
														<span> Visual Designer </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<i class="fa fa-circle status-online"></i>
													<img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Ella Patterson</h4>
														<span> Web Editor </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<i class="fa fa-circle status-online"></i>
													<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Kenneth Ross</h4>
														<span> Senior Designer </span>
													</div>
												</a>
											</li>
										</ul>
										<h5 class="sidebar-title">Off-line</h5>
										<ul class="media-list">
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Nicole Bell</h4>
														<span> Content Designer </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<div class="user-label">
														<span class="label label-success">3</span>
													</div>
													<img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Steven Thompson</h4>
														<span> Visual Designer </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<img alt="..." src="assets/images/avatar-8.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Ella Patterson</h4>
														<span> Web Editor </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<img alt="..." src="assets/images/avatar-9.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Kenneth Ross</h4>
														<span> Senior Designer </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Ella Patterson</h4>
														<span> Web Editor </span>
													</div>
												</a>
											</li>
											<li class="media">
												<a data-toggle-class="chat-open" data-toggle-target="#users" href="#">
													<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
													<div class="media-body">
														<h4 class="media-heading">Kenneth Ross</h4>
														<span> Senior Designer </span>
													</div>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="user-chat">
									<div class="chat-content">
										<div class="sidebar-content perfect-scrollbar">
											<a class="sidebar-back pull-left" href="#" data-toggle-class="chat-open" data-toggle-target="#users"><i class="ti-angle-left"></i> <span>Back</span></a>
											<ol class="discussion">
												<li class="messages-date">
													Sunday, Feb 9, 12:58
												</li>
												<li class="self">
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															Hi, Nicole
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															How are you?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="other">
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															Hi, i am good
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="self">
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															Glad to see you ;)
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="messages-date">
													Sunday, Feb 9, 13:10
												</li>
												<li class="other">
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															What do you think about my new Dashboard?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="messages-date">
													Sunday, Feb 9, 15:28
												</li>
												<li class="other">
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															Alo...
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															Are you there?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="self">
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															Hi, i am here
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															Your Dashboard is great
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="messages-date">
													Friday, Feb 7, 23:39
												</li>
												<li class="other">
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															How does the binding and digesting work in AngularJS?, Peter?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="self">
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															oh that's your question?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															little reduntant, no?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															literally we get the question daily
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="other">
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															I know. I, however, am not a nerd, and want to know
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="self">
													<div class="message">
														<div class="message-name">
															Peter Clark
														</div>
														<div class="message-text">
															for this type of question, wouldn't it be better to try Google?
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-1.jpg" alt="">
														</div>
													</div>
												</li>
												<li class="other">
													<div class="message">
														<div class="message-name">
															Nicole Bell
														</div>
														<div class="message-text">
															Lucky for us :)
														</div>
														<div class="message-avatar">
															<img src="assets/images/avatar-2.jpg" alt="">
														</div>
													</div>
												</li>
											</ol>
										</div>
									</div>
									<div class="message-bar">
										<div class="message-inner">
											<a class="link icon-only" href="#"><i class="fa fa-camera"></i></a>
											<div class="message-area">
												<textarea placeholder="Message"></textarea>
											</div>
											<a class="link" href="#">
												Send
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="off-favorites">
							<div class="users-list">
								<div class="sidebar-content perfect-scrollbar">
									<h5 class="sidebar-title">Favorites</h5>
									<ul class="media-list">
										<li class="media">
											<a href="#">
												<img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
												<div class="media-body">
													<h4 class="media-heading">Nicole Bell</h4>
													<span> Content Designer </span>
												</div>
											</a>
										</li>
										<li class="media">
											<a href="#">
												<div class="user-label">
													<span class="label label-success">3</span>
												</div>
												<img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
												<div class="media-body">
													<h4 class="media-heading">Steven Thompson</h4>
													<span> Visual Designer </span>
												</div>
											</a>
										</li>
										<li class="media">
											<a href="#">
												<img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
												<div class="media-body">
													<h4 class="media-heading">Ella Patterson</h4>
													<span> Web Editor </span>
												</div>
											</a>
										</li>
										<li class="media">
											<a href="#">
												<img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
												<div class="media-body">
													<h4 class="media-heading">Kenneth Ross</h4>
													<span> Senior Designer </span>
												</div>
											</a>
										</li>
										<li class="media">
											<a href="#">
												<img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
												<div class="media-body">
													<h4 class="media-heading">Ella Patterson</h4>
													<span> Web Editor </span>
												</div>
											</a>
										</li>
										<li class="media">
											<a href="#">
												<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
												<div class="media-body">
													<h4 class="media-heading">Kenneth Ross</h4>
													<span> Senior Designer </span>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="off-settings">
							<div class="sidebar-content perfect-scrollbar">
								<h5 class="sidebar-title">General Settings</h5>
								<ul class="media-list">
									<li class="media">
										<div class="padding-10">
											<div class="display-table-cell">
												<input type="checkbox" class="js-switch" checked />
											</div>
											<span class="display-table-cell vertical-align-middle padding-left-10">Enable Notifications</span>
										</div>
									</li>
									<li class="media">
										<div class="padding-10">
											<div class="display-table-cell">
												<input type="checkbox" class="js-switch" />
											</div>
											<span class="display-table-cell vertical-align-middle padding-left-10">Show your E-mail</span>
										</div>
									</li>
									<li class="media">
										<div class="padding-10">
											<div class="display-table-cell">
												<input type="checkbox" class="js-switch" checked />
											</div>
											<span class="display-table-cell vertical-align-middle padding-left-10">Show Offline Users</span>
										</div>
									</li>
									<li class="media">
										<div class="padding-10">
											<div class="display-table-cell">
												<input type="checkbox" class="js-switch" checked />
											</div>
											<span class="display-table-cell vertical-align-middle padding-left-10">E-mail Alerts</span>
										</div>
									</li>
									<li class="media">
										<div class="padding-10">
											<div class="display-table-cell">
												<input type="checkbox" class="js-switch" />
											</div>
											<span class="display-table-cell vertical-align-middle padding-left-10">SMS Alerts</span>
										</div>
									</li>
								</ul>
								<div class="save-options">
									<button class="btn btn-success">
										<i class="icon-settings"></i><span>Save Changes</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: OFF-SIDEBAR -->
			<!-- start: SETTINGS -->
			<div class="settings panel panel-default hidden-xs hidden-sm" id="settings">
				<!-- <button ct-toggle="toggle" data-toggle-class="active" data-toggle-target="#settings" class="btn btn-default">
					<i class="fa fa-spin fa-gear"></i>
				</button> -->
				<div class="panel-heading">
					Style Selector
				</div>
				<div class="panel-body">
					<!-- start: FIXED HEADER -->
					<div class="setting-box clearfix">
						<span class="setting-title pull-left"> Fixed header</span>
						<span class="setting-switch pull-right">
							<input type="checkbox" class="js-switch" id="fixed-header" />
						</span>
					</div>
					<!-- end: FIXED HEADER -->
					<!-- start: FIXED SIDEBAR -->
					<div class="setting-box clearfix">
						<span class="setting-title pull-left">Fixed sidebar</span>
						<span class="setting-switch pull-right">
							<input type="checkbox" class="js-switch" id="fixed-sidebar" />
						</span>
					</div>
					<!-- end: FIXED SIDEBAR -->
					<!-- start: CLOSED SIDEBAR -->
					<div class="setting-box clearfix">
						<span class="setting-title pull-left">Closed sidebar</span>
						<span class="setting-switch pull-right">
							<input type="checkbox" class="js-switch" id="closed-sidebar" />
						</span>
					</div>
					<!-- end: CLOSED SIDEBAR -->
					<!-- start: FIXED FOOTER -->
					<div class="setting-box clearfix">
						<span class="setting-title pull-left">Fixed footer</span>
						<span class="setting-switch pull-right">
							<input type="checkbox" class="js-switch" id="fixed-footer" />
						</span>
					</div>
					<!-- end: FIXED FOOTER -->
					<!-- start: THEME SWITCHER -->
					<div class="colors-row setting-box">
						<div class="color-theme theme-1">
							<div class="color-layout">
								<label>
									<input type="radio" name="setting-theme" value="theme-1">
									<span class="ti-check"></span>
									<span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
									<span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
								</label>
							</div>
						</div>
						<div class="color-theme theme-2">
							<div class="color-layout">
								<label>
									<input type="radio" name="setting-theme" value="theme-2">
									<span class="ti-check"></span>
									<span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
									<span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
								</label>
							</div>
						</div>
					</div>
					<div class="colors-row setting-box">
						<div class="color-theme theme-3">
							<div class="color-layout">
								<label>
									<input type="radio" name="setting-theme" value="theme-3">
									<span class="ti-check"></span>
									<span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
									<span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
								</label>
							</div>
						</div>
						<div class="color-theme theme-4">
							<div class="color-layout">
								<label>
									<input type="radio" name="setting-theme" value="theme-4">
									<span class="ti-check"></span>
									<span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
									<span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
								</label>
							</div>
						</div>
					</div>
					<div class="colors-row setting-box">
						<div class="color-theme theme-5">
							<div class="color-layout">
								<label>
									<input type="radio" name="setting-theme" value="theme-5">
									<span class="ti-check"></span>
									<span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
									<span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
								</label>
							</div>
						</div>
						<div class="color-theme theme-6">
							<div class="color-layout">
								<label>
									<input type="radio" name="setting-theme" value="theme-6">
									<span class="ti-check"></span>
									<span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
									<span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
								</label>
							</div>
						</div>
					</div>
					<!-- end: THEME SWITCHER -->
				</div>
			</div>
			<!-- end: SETTINGS -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->

<!--<script src="assets/js/jquery.imgareaselect.js"></script>-->
		<!-- <script src="vendor/jquery/jquery.min.js"></script> -->
		<!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
		<script src="vendor/modernizr/modernizr.js"></script>
		<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="vendor/switchery/switchery.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

		<script src="vendor/Chart.js/Chart.min.js"></script>
		<script src="vendor/jquery.sparkline/jquery.sparkline.min.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CLIP-TWO JAVASCRIPTS -->
		<script src="assets/js/main.js"></script>
		<!-- start: JavaScript Event Handlers for this page -->
		 <script src="assets/js/index.js"></script>
<script type="text/javascript" src="jquery.check.js"></script>
<!--  -->

		<script>


			jQuery(document).ready(function() {
				Main.init();
				Index.init();
			});
		</script>
		<!-- end: JavaScript Event Handlers for this page -->
		<!-- end: CLIP-TWO JAVASCRIPTS -->
	</body>
</html>
<?php ob_end_flush();
?>