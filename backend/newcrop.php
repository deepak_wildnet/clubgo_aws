<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
  <!--<![endif]-->
  <!-- start: HEAD -->
  <head>
       <title>ClubGo - Responsive Admin Template</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <!-- end: GOOGLE FONTS -->
    <!-- start: MAIN CSS -->

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/themify-icons/themify-icons.min.css">
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
    <link href="vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
    <!-- end: MAIN CSS -->
    <!-- start: CLIP-TWO CSS -->
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/themes/theme-1.css" id="skin_color" />
<!--<link rel="stylesheet" href="assets/css/style1.css" />-->
<!--<link rel="stylesheet" href="assets/css/jquery.Jcrop.min1.css" type="text/css" />-->
<link rel="stylesheet" type="text/css" href="assets/css/imgareaselect-default.css" />




<script type="text/javascript" src="assets/js/jquery.min.js"></script>

<!-- crop script starts here -->

<script type="text/javascript">

// var id=window.location.href.split('id=');
// if(id[1]){
// id=id[1];
// }
// else{
// id=0;
// }
function getSizes(im,obj)
  {


    var x_axis = obj.x1;
    var x2_axis = obj.x2;
    var y_axis = obj.y1;
    var y2_axis = obj.y2; 
    var thumb_width = obj.width;
  

  var image_type=document.getElementById('image_type').value;
    

    if(image_type=='feature_image')
    {
      if(thumb_width<320){


      alert("Image width shouldn't be less than 320 px.");
      return false;
    }
    if(thumb_height<320){


      alert("Image height shouldn't be less than 320 px.");
      return false;
    }
    }

     if(image_type=='cover_image')
    {
      if(thumb_width<600){


      alert("Image width shouldn't be less than 600 px.");
      return false;
    }
    if(thumb_height<400){


      alert("Image height shouldn't be less than 400 px.");
      return false;
    }
    }

     if(image_type=='ss_img1' || image_type=='ss_img2' || image_type=='ss_img3' || image_type=='ss_img4' )
    {
      if(thumb_width<1024){


      alert("Image width shouldn't be less than 1024 px.");
      return false;
    }
    if(thumb_height<768){


      alert("Image height shouldn't be less than 768 px.");
      return false;
    }
    }
    
    var thumb_height = obj.height;
       document.getElementById('wid').value=thumb_width;
       document.getElementById('hei').value=thumb_height;

       
 
//alert(x_axis);alert(y_axis);alert(thumb_width);alert(thumb_height);
    if(thumb_width > 0)
      {
        if(confirm("Do you want to save image?"))
          {   
//                                              
                $.ajax({
              type:"GET",
              url:"event_ajax.php?t=ajax&img="+$("#image_name").val()+"&w="+thumb_width+"&h="+thumb_height+"&x1="+x_axis+"&y1="+y_axis+"&id="+id+"&img_typ="+image_type,
              cache:false,
              success:function(rsponse)
                {
                 $("#cropimage").hide();
                    $("#thumbs").html("");
                  $("#thumbs").html("<img src='images/event/"+rsponse+"' />");
                                                                    
                }
            });
          }
      }
    else
      alert("Please select portion.");
  }

$(document).ready(function () {

  var  ab=$('#image_type :selected').val();



if(ab=='feature_image'){



    $('img#photo1').imgAreaSelect({

      
        aspectRatio: '1:1',
        
        onSelectEnd: getSizes
         
    });
}
 if(ab=='cover_image'){



    $('img#photo1').imgAreaSelect({

      
        aspectRatio: '3:2',
        
        onSelectEnd: getSizes
         
    });
}



 if(ab=='ss_img1' || ab=='ss_img2' || ab=='ss_img3' || ab=='ss_img4' ){



    $('img#photo1').imgAreaSelect({

      
        aspectRatio: '4:3',
        
        onSelectEnd: getSizes
         
    });
}


});
</script>
</head>
<body>
<?php


error_reporting(0);

ob_start();

include("config.php");
// crop code starts here 

$path = "images/event/";

$valid_formats = array("jpg", "png", "gif", "bmp");
  if(isset($_POST['crop']))
    {

       @$image_type=$_POST['image_type'];
      
      $name = $_FILES['photoimg']['name'];
      $size = $_FILES['photoimg']['size'];
      $size1 = getimagesize($_FILES['photoimg']['tmp_name']);



      if($size1[0] < 2400){

      

         
      
      
      if(strlen($name))
        {
          list($txt, $ext) = explode(".", $name);
          if(in_array($ext,$valid_formats) && $size<(1024*1024))
            {
              $actual_image_name = time().substr($txt, 5).".".$ext;
              $tmp = $_FILES['photoimg']['tmp_name'];
              if(move_uploaded_file($tmp, $path.$actual_image_name))
                {  //chmod($path.$actual_image_name,0777);
                //mysql_query("UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");

if($image_type=='feature_image'){

                  
                  $image="<h3>Please drag on the image. Width and height must be 320 , 320 resp. Maximum Image width shouldn't exceed 2400 px.</h3><img src='images/event/".$actual_image_name."' id=\"photo1\" style='max-width:2400px' >";

                     }

                    if($image_type=='cover_image'){

                  
                  $image="<h3>Please drag on the image. Width and height must be 600 , 400 resp. Maximum Image width shouldn't exceed 2400 px. </h3><img src='images/event/".$actual_image_name."' id=\"photo1\" onclick='abc()' style='max-width:2400px' >";

                     }
                      if($image_type=='ss_img1' || $image_type=='ss_img2' || $image_type=='ss_img3' || $image_type=='ss_img4' ){

                  
                  $image="<h3>Please drag on the image. Width and height must be 1024 , 768 resp. Maximum Image width shouldn't exceed 2400 px. </h3><img src='images/event/".$actual_image_name."' id=\"photo1\" style='max-width:2400px' >";

                     }
                  
                }
              else
                echo "failed";
            }
          else
            echo "Invalid file formats..!";         
        }
      else
        echo "Please select image..!";

    }else{


          @$Err="Maximum Image width shouldn't exceed 2400 px.";

    }
  }
// crop code ends here  





?>

<!-- crop form starts -->


<div style="margin:0 auto; width:600px">
<label>width</label>
<input type='text' id='wid' readonly>
<label>Height</label>
<input type='text' id='hei' readonly>
<?php echo @$image; ?>
<div id="thumbs" style="padding:5px; width:600px"></div>
<div style="width:600px">

<form id="cropimage" method="post" enctype="multipart/form-data">
Select Image to Crop<select name="image_type" id="image_type" required>
<option value="">--Select--</option>
<option value="feature_image" <?php if(isset($_POST['image_type']) && $_POST['image_type'] == "feature_image") echo 'selected="Featured Image"';?>>Featured Image</option>
<option value="cover_image" <?php if(isset($_POST['image_type']) && $_POST['image_type'] == "cover_image") echo 'selected="Cover Image"';?>>Cover Image</option>
<option value="ss_img1" <?php if(isset($_POST['image_type']) && $_POST['image_type'] == "ss_img1") echo 'selected="Slideshow Image1"';?>>Slideshow Image1</option>
<option value="ss_img2" <?php if(isset($_POST['image_type']) && $_POST['image_type'] == "ss_img2") echo 'selected="Slideshow Image2"';?>>Slideshow Image2</option>
<option value="ss_img3" <?php if(isset($_POST['image_type']) && $_POST['image_type'] == "ss_img3") echo 'selected="Slideshow Image3"';?>>Slideshow Image3</option>
<option value="ss_img4" <?php if(isset($_POST['image_type']) && $_POST['image_type'] == "ss_img4") echo 'selected="Slideshow Image4"';?>>Slideshow Image4</option>
</select></br>
  Upload to crop your image <input type="file" name="photoimg" id="photoimg" />
  <input type="hidden" name="image_name" id="image_name" value="<?php echo($actual_image_name)?>" />
  <input type="submit" name="crop" value="Submit" /><p style="color:red;"><?php echo $Err; ?></p>
</form>

</div>
</div>
<!--crop form ends-->


<!-- <script src="vendor/jquery.sparkline/jquery.sparkline.min.js"></script>-->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CLIP-TWO JAVASCRIPTS -->
     

      <!-- <script src="assets/js/main.js"></script>
    start: JavaScript Event Handlers for this page -->

<!-- 
     <script src="assets/js/index.js"></script>
<script type="text/javascript" src="jquery.check.js"></script>  -->

<!-- crop script ends here -->

 </body>
</html>
<?php ob_end_flush();
?>