<?php
error_reporting(0);
ob_start();
session_start();
$a = $_SESSION['name'];

if (!$a) {
    header("location:login.php");
}
$expireAfter = 30;
if (isset($_SESSION['last_action'])) {

    $secondsInactive = time() - $_SESSION['last_action'];
    $expireAfterSeconds = $expireAfter * 60;

    if ($secondsInactive >= $expireAfterSeconds) {
        session_unset();
        session_destroy();
    }
}

$_SESSION['last_action'] = time();
?>

<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>ClubGo - Responsive Admin Template</title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="vendor/themify-icons/themify-icons.min.css">
        <link href="vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/plugins.css">
        <link rel="stylesheet" href="assets/css/themes/theme-1.css" id="skin_color" />
        <!-- end: CLIP-TWO CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <link href="vendor/DataTables/css/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        
        <script type="text/javascript">
                function UpdateRecord(id) {
                    var x;
                    if (confirm("Want to change status ?") == true) {

                        var ID = id;
                        //alert(id);
                        var status = $('#active').attr('id');
                        $.ajax({
                            type: 'POST',
                            url: 'interestinsert.php?action=status&id=' + ID,
                            data: {res: status},
                            success: function(data) {
                                $(".info_1").text(" ");
                                if (data == '1') {
                                    $("#active_" + ID).val("Active");
                                    $(".info").text("Record Activated Successfully!!!").fadeIn('slow').delay(1000).hide(1);

                                } else {
                                    $("#active_" + ID).val("In-active");
                                    $(".info").text("Record Deactivated Successfully!!!").fadeIn('slow').delay(1000).hide(1);

                                }
                            },
                            error: function(err) {
                                console.log(err);
                            }
                        });
                    }
                }

            </script>
            
        
        <script type="text/javascript">
            function go() {
                self.location.href = "interestinsert.php?action=add";
            }

        </script>
        <?php
        include("config.php");
        ?>
    </head>
    <!-- end: HEAD -->
    <body>
        <div id="app">
            <!-- sidebar -->
            <div class="sidebar app-aside" id="sidebar">
                <div class="sidebar-container perfect-scrollbar">
                    <nav>                        
                        <!-- start: MAIN NAVIGATION MENU -->
                        <div class="navbar-title">
                            <span>Main Navigation</span>
                        </div>
                        <ul class="main-navigation-menu">

                            <?php include("nav_index.php"); ?>                          

                        </ul>                       
                    </nav>
                </div>
            </div>
            <!-- / sidebar -->
            <div class="app-content">
                <!-- start: TOP NAVBAR -->
                <header class="navbar navbar-default navbar-static-top">
                    <!-- start: NAVBAR HEADER -->
                    <div class="navbar-header">
                        <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="navbar-brand" href="#">
                            <h1>ClubGo</h1>
                            <!-- <img src="assets/images/logo.png" alt="Clip-Two"/> -->
                        </a>
                        <a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ti-view-grid"></i>
                        </a>
                    </div>
                    <!-- end: NAVBAR HEADER -->
                    <!-- start: NAVBAR COLLAPSE -->
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-right">                            
                            <!-- start: LANGUAGE SWITCHER -->
                            <!-- start: USER OPTIONS DROPDOWN -->
                            <li class="dropdown current-user">
                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="assets/images/unnamed.jpg" alt="Peter"> <span class="username"><?php echo $a; ?> <i class="ti-angle-down"></i></i></span>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">                                    
                                    <li>
                                        <a href="logout.php">
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER OPTIONS DROPDOWN -->
                        </ul>
                        <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
                        <div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <div class="arrow-left"></div>
                            <div class="arrow-right"></div>
                        </div>
                        <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
                    </div>
                    
                </header>
                <!-- end: TOP NAVBAR -->
                <div class="main-content" >
                    <div class="wrap-content container" id="container">                        
                        <!-- start: DYNAMIC TABLE -->
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">
                                <div class="col-sm-7">
                                    <h1 class="mainTitle">Customer Interest List</h1>
                                    <!-- <span class="mainDescription">overview &amp; stats </span> -->
                                </div>
                                <div class="col-md-12">
                                        <!-- <h5 class="over-title margin-bottom-15">Basic <span class="text-bold">Data Table</span></h5>
                                        <p>
                                                DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table.
                                        </p> -->
                                    <input type="button" class="btn btn-green add-row fa fa-plus" name="send" value="Add New" onclick="go()" style="float:right; margin-right:60px;"></br></br> 


                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1" width="50%">
                                        <thead>
                                            <tr>
                                                <th>Interest</th>
                                                <th>Action</th>                      
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            @$act = $_GET['action'];
                                            @$id = $_GET['id'];

                                            if ($act and $act == 'del') {
                                                $dele = mysqli_query($conn, "DELETE from interest where id='$id'");
                                            }
                                            
                                            @$query = mysqli_query($conn, "SELECT id,title,interest_date,status from interest");

                                            while (@$fetch = mysqli_fetch_array($query)) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $fetch['title'] ?></td>              
                                                    <td>
                                                        <a href="interestinsert.php?action=edit&id=<?php echo $fetch['id'] ?>">Edit</a> | <a href="interest.php?action=del&id=<?php echo $fetch['id'] ?>" onclick="return confirm('Are you sure want to delete it?');" >Delete</a></td> 
                                                </tr>
                                                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end: DYNAMIC TABLE -->
                        <!-- start: DYNAMIC TABLE -->

                    </div>
                </div>
            </div>


            <!-- start: FOOTER -->
            <footer>
                <div class="footer-inner">
                    <div class="pull-left">
                        &copy; <span class="current-year"></span><span class="text-bold"> ClubGo</span>. <span>All rights reserved</span>
                    </div>
                    <div class="pull-right">
                        <span class="go-top"><i class="ti-angle-up"></i></span>
                    </div>
                </div>
            </footer>
            <!-- end: FOOTER -->
            <!-- start: OFF-SIDEBAR -->
            <div id="off-sidebar" class="sidebar">
                <div class="sidebar-wrapper">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#off-users" aria-controls="off-users" role="tab" data-toggle="tab">
                                <i class="ti-comments"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#off-favorites" aria-controls="off-favorites" role="tab" data-toggle="tab">
                                <i class="ti-heart"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#off-settings" aria-controls="off-settings" role="tab" data-toggle="tab">
                                <i class="ti-settings"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="off-users">                            
                        </div>
                        <div role="tabpanel" class="tab-pane" id="off-favorites">                            
                        </div>
                        <div role="tabpanel" class="tab-pane" id="off-settings">                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: OFF-SIDEBAR -->
            <!-- start: SETTINGS -->
            <div class="settings panel panel-default hidden-xs hidden-sm" id="settings">
                <!-- <button ct-toggle="toggle" data-toggle-class="active" data-toggle-target="#settings" class="btn btn-default">
                        <i class="fa fa-spin fa-gear"></i>
                </button>
                <div class="panel-heading">
                        Style Selector
                </div> -->
                <div class="panel-body">
                    <!-- start: FIXED HEADER -->
                    <div class="setting-box clearfix">
                        <span class="setting-title pull-left"> Fixed header</span>
                        <span class="setting-switch pull-right">
                            <input type="checkbox" class="js-switch" id="fixed-header" />
                        </span>
                    </div>
                    <!-- end: FIXED HEADER -->
                    <!-- start: FIXED SIDEBAR -->
                    <div class="setting-box clearfix">
                        <span class="setting-title pull-left">Fixed sidebar</span>
                        <span class="setting-switch pull-right">
                            <input type="checkbox" class="js-switch" id="fixed-sidebar" />
                        </span>
                    </div>
                    <!-- end: FIXED SIDEBAR -->
                    <!-- start: CLOSED SIDEBAR -->
                    <div class="setting-box clearfix">
                        <span class="setting-title pull-left">Closed sidebar</span>
                        <span class="setting-switch pull-right">
                            <input type="checkbox" class="js-switch" id="closed-sidebar" />
                        </span>
                    </div>
                    <!-- end: CLOSED SIDEBAR -->
                    <!-- start: FIXED FOOTER -->
                    <div class="setting-box clearfix">
                        <span class="setting-title pull-left">Fixed footer</span>
                        <span class="setting-switch pull-right">
                            <input type="checkbox" class="js-switch" id="fixed-footer" />
                        </span>
                    </div>
                    <!-- end: FIXED FOOTER -->
                    <!-- start: THEME SWITCHER -->
                    <div class="colors-row setting-box">
                        <div class="color-theme theme-1">
                            <div class="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" value="theme-1">
                                    <span class="ti-check"></span>
                                    <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                                    <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                                </label>
                            </div>
                        </div>
                        <div class="color-theme theme-2">
                            <div class="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" value="theme-2">
                                    <span class="ti-check"></span>
                                    <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                                    <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="colors-row setting-box">
                        <div class="color-theme theme-3">
                            <div class="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" value="theme-3">
                                    <span class="ti-check"></span>
                                    <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                                    <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                                </label>
                            </div>
                        </div>
                        <div class="color-theme theme-4">
                            <div class="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" value="theme-4">
                                    <span class="ti-check"></span>
                                    <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                                    <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="colors-row setting-box">
                        <div class="color-theme theme-5">
                            <div class="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" value="theme-5">
                                    <span class="ti-check"></span>
                                    <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                                    <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                                </label>
                            </div>
                        </div>
                        <div class="color-theme theme-6">
                            <div class="color-layout">
                                <label>
                                    <input type="radio" name="setting-theme" value="theme-6">
                                    <span class="ti-check"></span>
                                    <span class="split header"> <span class="color th-header"></span> <span class="color th-collapse"></span> </span>
                                    <span class="split"> <span class="color th-sidebar"><i class="element"></i></span> <span class="color th-body"></span> </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- end: THEME SWITCHER -->
                </div>
            </div>
            <!-- end: SETTINGS -->
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/modernizr/modernizr.js"></script>
        <script src="vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="vendor/switchery/switchery.min.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/DataTables/jquery.dataTables.min.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CLIP-TWO JAVASCRIPTS -->
        <script src="assets/js/main.js"></script>
        <!-- start: JavaScript Event Handlers for this page -->
        <script src="assets/js/table-data.js"></script>
        <script>
            jQuery(document).ready(function() {
                Main.init();
                TableData.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
        <!-- end: CLIP-TWO JAVASCRIPTS -->
    </body>
</html>
<?php ob_end_flush();
?>
