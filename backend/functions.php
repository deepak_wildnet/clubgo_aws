<?php
error_reporting(1);

Class Crud{

	private $conn;
	public function __construct($conn) {
        global $conn;
        $this->conn = $conn;
    }
	//Insert Event Price
	public function eventPrice($data){
		$sql = $this->insertToSqlQuery($data);

		$insert = mysqli_query($this->conn,"INSERT into `event_price` (".$sql['keys'].") values(".$sql['values'].")");
		
		if(!$insert){
			die("Error :".mysqli_errno());
		}
	}
	/**
	  * Update Event Price
	  *
	 */
	public function eventPriceUpdate($data,$id){
		if($id){

			$sql = $this->updateToSqlQuery($data);
			
			$update = mysqli_query($this->conn,"UPDATE `event_price` set $sql where id=".$id);

		}else{
			return ;
		}
	}
	/**
	 * Get all the event price using event id (e_id)
	 */
	public function getEventPrices($id = null){

		if($id){
			$query = mysqli_query($this->conn,"SELECT * FROM `event_price` where e_id =".$id);
			
			$prices = array();
			
			while($row = mysqli_fetch_object($query)){
			
				$ttype = explode(',',$row->ttype);
				
				$ticket_type_title = "";
				foreach ($ttype as $k => $v) {

					$query1 = mysqli_query($this->conn,"SELECT * FROM `ticket_type` where type_id =".$v);
					$ttype_object = mysqli_fetch_object($query1);
					$ticket_type_title .= $ttype_object->type_title.",";
				}

				$ticket_type_title = rtrim($ticket_type_title,',');
				$row->ttype = $ticket_type_title;
				array_push($prices,$row);
			}

			return (object) $prices;
		}
	}
	/**
	 * Get Event Price Information using event price id (id)
	 * @return Object
	 */
	public function getEventPricesById($id){

		if($id){
			$query = mysqli_query($this->conn,"SELECT * FROM `event_price` where id =".$id);
			
			$row = mysqli_fetch_object($query);
			return (object) $row;
		}
	}
	/**
	  * This function is used to count new review (status = 0)
	  * @return count
 	  */
	public function countNewReview(){
		
		$query = mysqli_query($this->conn,"SELECT count(*) as count FROM `review` where status=0");
		$row = mysqli_fetch_object($query);
		return $row->count;
	}

	/**
	  * This function is used for convert array into keys and values
	  * @return Array
	  */
	public function insertToSqlQuery($data){

		$keys = "";
		$values = "";
		foreach ($data as $key => $value) {
			$keys .= $key.',';
			$values .= "'$value'".",";
		}
		$keys = rtrim($keys,',');
		$values = rtrim($values,',');

		$sql = array('keys'=>$keys,'values'=>$values);
		return $sql;

	}
	/**
	 * This function is used for convert array into like update query
	 */
	public function updateToSqlQuery($data){
		
		$fields = "";
		foreach ($data as $key => $value) {
			$fields .= "`$key`='$value'".',';
		}
		$fields = rtrim($fields,',');
		return $fields;
		
	}
	/**
	 * Get Single Row
	 * @First Argument for table name
	 * @Second Argument for key
	 * @Third argument for value
	 */
	function get_row_using_key_and_value($tablename,$key,$value){

		if($key && $value){
			$query = mysqli_query($this->conn,"SELECT * FROM $tablename where $key =".$value);
			$row = mysqli_fetch_object($query);
			return (object) $row;
		}

	}
}
?>